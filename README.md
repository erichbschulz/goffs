## Overall architecture:

This React client served off a node server making CORS requests to an ExpressJS backend using file storage.

Two key repositories:

* [Client](git@bitbucket.org:erichbschulz/goffs.git)
* [Server](git@bitbucket.org:erichbschulz/goffserver.git)


## Data Dictionary

Many to many relationship between users and sessions.
Each session may have multiple screens.


Core entities have
- id|title|type fields

Relationship entities exist for many to many relationships.
Optional fields in (brackets)
Many to many relationship between sessions and users

Database directory: /var/goff/

    /sessions
    - One file per session
    - Holds a level of trust between users and device.
    - The server controls the session but shares with the client
      - users|deviceName
    /users
    - one file per user id|title|(email)|passhash
    /forum
      - id|title|description|start|end|updatedId
      - updatedId is held in redis instance
    /forumData
      /forumX
        /item
          - [forumId]|id|title|description|timeAllocated|timeElapsed|status
          status = [pending|complete|active]
        /motion
          - [forumId]|id|title|description|moved|seconded|motionId|status
          status = [passed|defeated|withdrawn|tabled]
        /comment
          - [forumId]|id|title|description|targetId|targetType|authors|status
          status = [active|hidden]
        /member
          - [forumId]|id|status|proxyTo|votingFor|sessions|
          status = [present|absent]
        /vote
          - [forumId]|targetType|targetId|memberId|viaMemberId
          - id = userId:targetType:targetId
        /stream (open to all forum members) - TBC
          - [forumId]|id|title|description|focusItem|focusMotion|convenors
        /update
          - type|id|action|forumId|record
          - type = 'UPSERT'/'PATCH'
          - id = integer sequence
        /log
      /forumY
    /logs

# Screen entity

Holds configuration of current screen. The sagas track server session changes and may update screen.

  showMenu: false,
  gridLock: false,
  showComments: true,
  currentForumId: undefined,
  currentItemId: undefined,
  updateId: undefined,
  users: {userId: {id, localStatus}} - list synchronised with session users


# Initial download:
    cd [[[Root]]]
    mkdir goff
    cd goff
    git clone git@bitbucket.org:erichbschulz/goffserver.git server
    git clone git@bitbucket.org:erichbschulz/goff.git client


#intalling node:

    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt-get install -y nodejs

# creating app

    npx create-react-app app1
    cd app1
    npm install
    npm start

#Check installation:

    lsb_release -a | grep Release
    node -v
    npm -v

# installing and running

From with in the container you can then (from within the container):

    cd /user/src/app/client
    npm install
    npm start

Then the server

    cd /user/src/app/server
    npm install
    HTTPSPORT=3003 PORT=3004 supervisor index.js | tee log

# Docker notes

In summary, create an image, then start (run) the container.
(Restart or attach as needed. Execute bash shells in running containers)

Create an image named `goff` with:

    docker build -t goff:1.0 .

Start a container `localgoff` based on the image with:

    BASE=~/goff
    docker run -it --name localgoff \
      -p 4000-4020:3000-3020        \
      -v $BASE:/user/src/app        \
      goff:1.0 bash

Restart the image with:

    docker restart localgoff
    docker exec -it localgoff bash

or, if container is running visible with `docker ps`:

    docker attach localgoff

If lost volumes on restart need to commit container and create new image:

    docker ps

    then

    N=1
    docker commit 493c8 goff:1.$N
    BASE=~/goff
    docker run -it --name localgoff$N \
      -p 4000-4020:3000-3020          \
      -v $BASE:/user/src/app          \
      goff:1.$N bash

# Erich's config

    Digital Ocean droplet. Ubuntu/Node/NPM: 16.04 / 8.94 / 5.6.0
    Erich's laptop: 16.04 / v8.9.4 / 5.6.0
                    16.04 / v8.10.0 / 5.6.0

- IP: 128.199.82.175
- ssh alias: do
  - serve build with: `serve -s build&`
- server instance:
  - path: /root/server
  - start with `supervisor index.js`


Start client with:

    HTTPS=true cd ~/app2 && npm start

    Your cert will expire on 2018-06-12. To obtain a new or tweaked
    version of this certificate in the future, simply run certbot again
    with the "certonly" option. To non-interactively renew *all* of
    your certificates, run "certbot renew"

To update keys in webpack-dev-server

    cd <to app directory>
    SOURCE=/etc/letsencrypt/live/wiseenough.com
    cd node_modules/webpack-dev-server/ssl/
    mv server.pem server.pem1
    cat $SOURCE/privkey.pem $SOURCE/fullchain.pem > server.pem

### Other stuff

See [React Readme](ReactReadme.md) for standard instructions.

To fix vim glitches on autorecompile:

    set backupcopy=yes
    au BufWritePost Shared.js :silent !cp /home/erich/goff/server/Shared.js /home/erich/goff/client/src/Shared.js

Installing redis:
    apt-get install make gcc
    curl -sSL http://download.redis.io/releases/redis-stable.tar.gz -o /tmp/redis.tar.gz
    mkdir -p /tmp/redis
    tar -xzf /tmp/redis.tar.gz -C /tmp/redis --strip-components=1
    make -C /tmp/redis
    make -C /tmp/redis install
    echo -n | /tmp/redis/utils/install_server.sh
    rm -rf /tmp/redis*
    # See: http://redis.io/topics/faq
    sysctl vm.overcommit_memory=1
    # Bind Redis to localhost. Comment out to make available externally.
    sed -i -e 's/# bind 127.0.0.1/bind 127.0.0.1/g' /etc/redis/6379.conf
    service redis_6379 restart

to restart after a shutdown:

    docker restart localgoff1
    docker exec -it localgoff1 bash


#
    cd /user/src/app/client/
    npm start

# restart redis
    rm /var/run/redis_6379.pid
    service redis_6379 restart
    redis-cli PING

# run server
# npm install supervisor -g
    cd /user/src/app/server/
    supervisor index.js
