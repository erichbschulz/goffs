FROM ubuntu:16.04

RUN apt-get update \
      && apt-get install -y curl

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - \
      && apt-get install -y nodejs

RUN mkdir -p /usr/src/app

## SSL:
# sudo add-apt-repository ppa:certbot/certbot
# sudo apt-get update
# sudo apt-get install python-certbot-apache


RUN npm install supervisor -g
WORKDIR /usr/src/app
EXPOSE 3000 3001 3002

# CMD ["npm","start"]
CMD ["bash"]
