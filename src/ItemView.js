import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Mood from './MoodView.js'
import Motion from './MotionView.js'
import Comments from './CommentView.js'
import FieldEdit from './FieldEditView.js'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { actionCreators  } from './Store.js'
import U from './Utils.js'
import { availableActionButtons } from './Meta.js'
import ActionButtons from './ActionButtonView.js'
import Typography from 'material-ui/Typography'
import Card, { CardContent } from 'material-ui/Card';
import {scaleColor} from './Theme.js';
//import Tooltip from 'material-ui/Tooltip'
import { LoveIndicatorIcon, Form } from './Widgets.js'
import ListAltIcon from 'material-ui-icons/FormatListNumbered'
import FirstPageIcon from 'material-ui-icons/FirstPage'
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft'
import ChevronRightIcon from 'material-ui-icons/ChevronRight'
import LastPageIcon from 'material-ui-icons/LastPage'
import LockIcon from 'material-ui-icons/Lock'
import OpenInNewIcon from 'material-ui-icons/OpenInNew'
import TrackChangesIcon from 'material-ui-icons/TrackChanges'

class ItemRaw extends Component {

  constructor(props) {
    super(props)
    this.baseline = {
      mode: 'normal',
    }
    const {dispatch} = props
    // create bound versions of functions so can
    // pass them down to our child later.
    this.actionCreators = bindActionCreators(actionCreators, dispatch)
    const {permissions} = props.session
    const buttons = this.makeActionButtons(permissions)
    this.state = Object.assign({
      buttons,
      navButtons: [],
      }, this.baseline)
    this.statusMeta = U.metaByField.item.status
    this.titleMeta = U.metaByField.item.title
    this.navToItem(props)
  }


  noItem = null

  makeActionButtons = permissions => ({
    item: availableActionButtons('globalItem', permissions),
    time: availableActionButtons('time', permissions),
    love: availableActionButtons('love', permissions),
  })

  findActive = items => {
    for (let key in items) {
      if (items[key].status === 'active') {
        return key // found an active item!
      }
    }
    return this.noItem
  }

  findItem = (currentItemId, items, mode) =>
    U.findRelativeInHash(currentItemId, items, mode, 'order')

  handleNav = (button) => {
    const {screen, items, history} = this.props
    const {currentForumId, currentItemId} = screen
    const url = itemId => U.itemUrl(currentForumId, itemId)
    const go = itemId => U.navigateTo(url(itemId), itemId, history)
    switch (button.key) {
      case 'forum':
        U.navigateTo(U.forumUrl(currentForumId), currentForumId, history)
        break
      case 'eject':
        U.navigateTo(url(currentItemId), currentItemId)
        break
      case 'track':
        go('active')
        break
      case 'lock':
        go(currentItemId)
        break
      default: // handle first/previous/next/last
        go(this.findItem(currentItemId, items, button.key))
    }
  }

  makeNavButtons= () => [
    {key: 'forum',
     title: 'Goto the forum agenda',
     icon: <ListAltIcon />},
    {key: 'first',
     title: 'Goto the first item',
     icon: <FirstPageIcon />},
    {key: 'previous',
     title: 'Goto the previous item',
     icon: <ChevronLeftIcon />},
    {key: 'next',
     title: 'Goto the next item',
     icon: <ChevronRightIcon />},
    {key: 'last',
     title: 'Goto the last item',
     icon: <LastPageIcon />},
    {key: 'lock',
     title: 'Stay on this item',
     icon: <LockIcon />},
    {key: 'eject',
     title: 'Open this item in own window',
     icon: <OpenInNewIcon />},
    {key: 'track',
     title: 'Update to stay on active item',
     icon: <TrackChangesIcon />},
  ]

  navToItem = (props) => {
    const {screen, items, match = {}, dispatch} = props
    const {params = {}} = match
    const {forumId, itemId} = params
    const {currentForumId, currentItemId} = screen
    if (forumId) {
      if (forumId !== currentForumId) {
        // we have a direct route but not currently on the correct forum
        dispatch({type: "CURRENTFORUM_SET", p: forumId})
      }
      else if (itemId) {
        if (itemId === 'active') { // find active item if not on one
          if ((items[currentItemId] || {}).status !== 'active') {
            const newItemId = this.findActive(items)
            if (currentItemId !== newItemId) {
              this.setScreenToItem(newItemId)
            }
          }
        }
        else {
          if (itemId !== currentItemId) {
            this.setScreenToItem(itemId)
          }
        }
      }
    }
  }

  isDynamic = (props) => {
    const {match = {}} = props
    const {params = {}} = match
    const {itemId} = params
    const item = this.currentItem()
    return itemId === 'active' && item.status === 'active'
  }

  componentDidUpdate(prevProps, prevState) {
    const {screen} = this.props
    const {currentItemId} = screen
    const {permissions} = this.props.session
    if (U.newPerms(prevProps, permissions)) {
      const buttons = this.makeActionButtons(permissions)
      this.setState({buttons})
    }
    // make button on new item
    if (prevProps.screen.currentItemId !== currentItemId) {
       this.setState({navButtons: this.makeNavButtons()})
    }
    this.navToItem(this.props)
  }

  setScreenToItem = itemId =>
    this.props.dispatch({type: "screen/setcurrentitem", p: {itemId}})

  editHandler = (meta, item, newValue) => {
    const {id: field} = meta
    const {forumId, id, type} = item
    const record = {id, type, forumId, [field]: newValue}
    const updates = [{type: 'update', verb: 'PATCH', record}]
    this.sendUpdates(updates)
  }

  // send an array of updates to the bulk route and handle outcome
  sendUpdates = (records) => {
    this.setState({mode: 'waiting'})
    U.sendBulk(records, this.props.dispatch)
    .catch(error => alert("This cannot be good. I dont know what to do."))
    .then(result => this.setState({mode: 'normal'}))
  }


  resetModeToNormal = () => this.setState(this.baseline)

  handleActionButton = (button, items) => {
    const {screen} = this.props
    const forumId = screen.currentForumId
    const {key} = button
    switch (key) {
    case 'addMotion': {
      const item = this.currentItem()
      const motion = U.buildObj({type: 'motion'}, {forumId, itemId: item.id})
      this.setState({mode: 'edit', motion})
    }
    break
    default:
      this.vote(button, items)
    }
  }

  vote(vote, items) {
    // FIXME should be general function
    const {screen, members, dispatch} = this.props
    const activeVoters = U.currentVoters(screen.users, members)
    items.forEach(item => {
      const payload = {
        forumId: screen.currentForumId,
        targetType: 'item',
        targetId: item.id,
        voters: activeVoters,
        action: {[vote.atribute]: vote.value}
      }
      dispatch({type: 'VOTE_PATCH', p: payload})
    })
  }

  // return an array
  availableUsers(props) {
    const {session, members} = props
    const {permissions} = session
    const result =  Object.values(
      permissions['update motion mover to any member']
      ? members
      : session.users)
    result.unshift({id: '--', title: '--', type: 'member'})
    return result
  }

  editForm() {
    const {dispatch} = this.props
    const users = this.availableUsers(this.props)
    const fields = [{
      name: 'title',
      label: "Motion title",
      placeholder: "Enter brief motion title here",
      type: "text",
      value: ''
    },{
      name: 'movedBy',
      label: "Moved by",
      type: "array",
      value: [(users[1] || users[0]).id], // 0 = '--'
      values: users
    },{
      name: 'secondedBy',
      label: "Seconded by",
      type: "array",
      disabled: users.length < 2, // ie another user other than '--'
      value: [users[0].id], // 0 = '--'
      values: users
    }]
    const buttons = [{text: "Submit"}]
    const onSubmit = (values, button) => {
      const motion = Object.assign({}, this.state.motion, values)
      ;['movedBy', 'secondedBy'].forEach(field => {
        // get userId (noting that the -- option means we wont find anyone
        if (motion[field] === '--') {
          delete motion[field]
        }

//        motion[field] = [
//          (users.find(user => user.title === values[field]) || {}).id
//          ]
      })
      dispatch({type: 'FORUM_UPSERT', p: motion})
      this.resetModeToNormal()
    }
    return <Form
      fields={fields}
      buttons={buttons}
      onCancel={this.resetModeToNormal}
      onSubmit={onSubmit}
      />
  }

  currentItem() {
    const {screen, items, item} = this.props
    const {currentItemId} = screen
    return item || items[currentItemId] || false
  }

  render() {
    const {screen, members, forums, motions, session} = this.props
    const availableMovers = () => this.availableUsers(this.props)
    const {permissions} = session
    const {mode, buttons, navButtons} = this.state
    const {participants = 0, currentForumId} = screen
    const forum = forums[currentForumId] || {}
    const item = this.currentItem()
    // if in dynamic mode prevent editing as may re-route
    const isDynamic = this.isDynamic(this.props)
    console.log('isDynamic', isDynamic)
    const nav = <ActionButtons
            menu={navButtons}
            handler={this.handleNav}
            hide={button =>
              (isDynamic && button.key === 'track')
              || (!isDynamic &&
                  (button.key === 'eject' || button.key === 'lock'))}
            items={[]}
          />
    if (item) {
      const { title, status, details, motion = [],
        mood, moodTally, moodSummary } = item
      // denominator)
      const scale = scaleColor(moodSummary.love / participants) || ""
      return mode === 'edit'
      ? this.editForm()
      : <div>
      {forum.title} {nav}
        <div
          style={{
            display: "flex",
            alignItems: "center", }}>
          <Typography variant={'title'} >
            <FieldEdit
              readOnly={isDynamic || !U.has(permissions, 'update item')}
              meta={this.titleMeta}
              onChangeHandler={newValue =>
              this.editHandler({id:'title'}, item, newValue)}
              value={title} />
          </Typography>
          <FieldEdit
            meta={this.statusMeta}
            readOnly={isDynamic || !U.has(permissions, 'update item')}
            onChangeHandler={newValue =>
              this.editHandler({id:'status'}, item, newValue)}
            value={status} />
          <ActionButtons
            menu={buttons.item}
            handler={this.handleActionButton}
            items={[item]}
          />
          <ActionButtons
            menu={buttons.time}
            handler={this.handleActionButton}
            items={[item]}
          />
          <LoveIndicatorIcon scale={scale} />
          <ActionButtons
            menu={buttons.love}
            handler={this.handleActionButton}
            items={[item]}
          />
        </div>
        <Typography component={'p'} >
          {details}
        </Typography>
        <Comments isDynamic={isDynamic} parentObj={item} />
        <div>
          <Mood
            members={members}
            mood={mood}
            tally={moodTally}
            participants={participants}
            summary={moodSummary} />
        </div>
        {motion && motion.length
          ? motion.map(motionId =>
          <Card key={motionId} >
            <CardContent>
              <Motion
              motion={motions[motionId]}
              participants={participants}
              availableMovers={availableMovers}
              isDynamic={isDynamic}
              members={members} />
            </CardContent>
          </Card>
            )
          : <Typography variant='subheading'>No motions</Typography>
          }
      </div>
    }
    else {
      return <div>
        {nav}
        <Typography variant={'title'} > No item  </Typography>
      </div>
    }
  }
}

ItemRaw.propTypes = {
  item: PropTypes.object,
}

// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
    // props here become props in the component
  return {
    forums: state.forums,
    items: state.items,
    motions: state.motions,
    members: state.members,
    screen: state.screen,
    session: state.session,
    users: state.fullUsers,
  }
}

const Item = connect(mapStateToProps)(ItemRaw)

export default Item
