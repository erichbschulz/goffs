import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Typography from 'material-ui/Typography'
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import PersonIcon from 'material-ui-icons/Person';
import PersonOutlineIcon from 'material-ui-icons/PersonOutline';
import GroupIcon from 'material-ui-icons/Group';

class Member extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: false,
    }
  }
  onClick = () => {
    this.setState({expanded: !this.state.expanded})
  }
  render() {
    const {memberId, members, mode} = this.props
    const current = memberId ? memberId.toString() : '' // make a safe string for comparison
    const {expanded} = this.state
    const member = members[memberId]
    if (!member) {
      console.warn('undefined member', memberId)
      return null
    }
    const {votingFor = [], title, present} = member
    let label = title
    let proxies = []
    if (mode === 'proxy') {
      proxies = votingFor.filter(memId => memId.toString() !== current)
      // list out the proxies
      if (expanded) {
        label += proxies.length ? ` + (${proxies.map(memId => members[memId].title).join(', ')})` : ''
      }
      else if (proxies.length) {
        // has a proxy that is not self
        label += ' +' + proxies.length
      }
    }
    const avatar = (proxies.length)
      ? <Avatar><GroupIcon /></Avatar>
      : present ? <Avatar><PersonIcon /></Avatar>
      : <Avatar><PersonOutlineIcon /></Avatar>
    return <Chip style={{margin: 2}}
      key={memberId} avatar={avatar} label={label}
      onClick={this.onClick}
       />
  }
}

Member.propTypes = {
  memberId: PropTypes.string.isRequired,
  members: PropTypes.object.isRequired,
  mode: PropTypes.oneOf(['proxy', 'plain'])
}

class Members extends Component {
  render() {
    const {memberList =[], members, mode, heading} = this.props
    return <div>
        <Typography variant="caption">
          {heading}
        </Typography>
        {memberList
          .filter(id => id)
          .map(id =>
              <Member key={id} memberId={id} members={members} mode={mode} />)}
      </div>
  }
}
Members.propTypes = {
  memberList: PropTypes.array.isRequired,
  members: PropTypes.object.isRequired,
  mode: PropTypes.oneOf(['proxy', 'plain']),
  heading: PropTypes.string
}
Members.defaultProps = {
  mode: 'proxy',
}

export default Members
export { Member }
