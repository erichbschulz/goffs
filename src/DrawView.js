import React from 'react'
import { Link} from 'react-router-dom'
import { bindActionCreators } from 'redux'
import Drawer from 'material-ui/Drawer'
import Divider from 'material-ui/Divider'
import {MenuList, MenuItem} from 'material-ui/Menu'
import List, {
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  ListSubheader,
} from 'material-ui/List'
import Tooltip from 'material-ui/Tooltip'
import Switch from 'material-ui/Switch'
import HelpIcon from 'material-ui-icons/Help'
import HomeIcon from 'material-ui-icons/Home'
import LockOpenIcon from 'material-ui-icons/LockOpen'
import PeopleIcon from 'material-ui-icons/People'
import ExitToAppIcon from 'material-ui-icons/ExitToApp'
import PersonAddIcon from 'material-ui-icons/PersonAdd'
import FileUploadIcon from 'material-ui-icons/FileUpload'
//import XIcon from 'material-ui-icons/X'
import ForumIcon from 'material-ui-icons/Forum'
//import LayersIcon from 'material-ui-icons/Layers'
import { connect } from 'react-redux'
import { actionCreators  } from './Store.js'
import U from './Utils.js'
//import Utils from './Utils.js'
import './DrawView.css'
import ClickOutHandler from 'react-onclickout'

// not this function is shared with both menu and draw classes
const mapStateToProps = state => {
  return {
    screen: state.screen,
  }
}

class MenuRaw  extends React.Component {

  constructor(props) {
    super(props)
    const {dispatch} = props
    this.actionCreators = bindActionCreators(actionCreators, dispatch)
  }

  hideMenu = () => {
    this.props.dispatch({type: 'MENU_HIDE'})
  }

  reset = (event) => {
    this.props.dispatch({type: 'SESSION_RESET'})
  }

  leaveForum = (event) => {
    const {dispatch, history} = this.props
    dispatch({type: 'CURRENTFORUM_SET', p: null})
    U.navigateTo('/forums', 'forums', history)
  }


  render() {
    const {
      //gridLock,
      showComments,
      users,
    } = this.props.screen
    const userCount = Object.keys(users).length
    const {
      toggleComments,
      //toggleGridlock
    } = this.actionCreators
    return (
      <div
        onClick={this.hideMenu}
        className="goffrightdraw">
        <MenuList>
          <Link to="/">
          <MenuItem>
            <ListItemIcon><HomeIcon /></ListItemIcon>
            <ListItemText primary="Home" />
          </MenuItem>
          </Link>
          <Link to="/auth">
          <MenuItem>
            <ListItemIcon><PersonAddIcon /></ListItemIcon>
              <ListItemText
              primary={userCount ? "Add user" : "Log in"} />
          </MenuItem>
          </Link>
          <Tooltip id="rptt" placement="bottom-end" title={userCount === 0
              ? "Log in first to change your password or, if you forgot your it then please ask an admin."
              : userCount === 1
              ? "Change your password"
              : "Only possible when a single user is logged in"
            }>
    <div>
          <Link to="/changepassword">
          <MenuItem disabled={userCount !== 1}>
              <ListItemIcon><LockOpenIcon /></ListItemIcon>
              <ListItemText
              primary="Change password" />
          </MenuItem>
            </Link>
          </div>
          </Tooltip>
            <Link to="/users">
          <MenuItem>
              <ListItemIcon><PeopleIcon /></ListItemIcon>
              <ListItemText primary="Manage users" />
          </MenuItem>
            </Link>
            <Link to="/bulkImport">
          <MenuItem>
              <ListItemIcon><FileUploadIcon /></ListItemIcon>
              <ListItemText primary="Import" />
          </MenuItem>
            </Link>
        </MenuList>
          <ListItem onClick={this.reset} button>
            <ListItemIcon><ExitToAppIcon /></ListItemIcon>
            <ListItemText primary="Log out" />
          </ListItem>
          {/*
          <ListItem onClick={this.leaveForum} button>
            <ListItemIcon><ExitToAppIcon /></ListItemIcon>
            <ListItemText primary="Swap forum" />
          </ListItem>
          */}
        <Link to="/help">
        <MenuItem>
            <ListItemIcon><HelpIcon /></ListItemIcon>
            <ListItemText primary="Help" />
        </MenuItem>
        </Link>
        <Divider />
        <List subheader={<ListSubheader>Settings</ListSubheader>}>
        {/*
          <ListItem onClick={toggleGridlock} button>
            <ListItemIcon>
              <LayersIcon />
            </ListItemIcon>
            <ListItemText primary="Lock Grid" />
            <ListItemSecondaryAction>
              <Switch onChange={toggleGridlock} checked={gridLock} />
            </ListItemSecondaryAction>
          </ListItem>
          */}
          <ListItem onClick={toggleComments} button>
            <ListItemIcon>
              <ForumIcon />
            </ListItemIcon>
            <ListItemText primary="Comments" />
            <ListItemSecondaryAction>
              <Switch onChange={toggleComments} checked={showComments}
              />
            </ListItemSecondaryAction>
          </ListItem>
        </List>
      </div>
    )
  }
}
const Menu = connect(mapStateToProps)(MenuRaw)

class DrawRaw extends React.Component {

  constructor(props) {
    super(props)
    const {dispatch} = props
    this.actionCreators = bindActionCreators(actionCreators, dispatch)
  }

  onClickOut = () => {
    this.props.dispatch({type: 'MENU_HIDE'})
  }

  render() {
    return (
      <Drawer open={this.props.screen.showMenu} >
        <ClickOutHandler onClickOut={this.onClickOut}>
          <Menu />
        </ClickOutHandler>
      </Drawer>
    )
  }
}

const Draw = connect(mapStateToProps)(DrawRaw)

export default Draw
