import React, { Component } from 'react'
import { connect } from 'react-redux'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton'
import MenuIcon from 'material-ui-icons/Menu'
import Icon from 'material-ui/Icon';
// import Checkbox from 'material-ui/Checkbox'

class AppBarRaw extends Component {

  userBadge = (user, members, screen) => {
    const {present, id} = user
    const {localStatus} = (screen.users || {})[id] || {}
    const member = members[id] || {}
    const {title} = user
    const {dispatch} = this.props
    const {
      votingFor = [],
      } = member
    const tip = () =>
      `${title} is recored as ${
        present ? 'here' : 'absent'
      } and has a local status of ${localStatus}`
    return <Badge
      key={id}
      color={localStatus === "active" ? "secondary" : "default"}
      badgeContent={votingFor.length}
      >
      <Button color="inherit"
      onClick={e => dispatch({type: 'USERSTATUS_TOGGLE_NOW', p: user.id})}
      title={tip()}
      >
        {title}
        {localStatus === 'active'
          ? <Icon color="action">add_circle</Icon>
          : <Icon color="disabled">remove_circle_outline</Icon>
        }
      </Button>
    </Badge>
  }

  render() {
    const {dispatch} = this.props
    const {forums, session, members, screen} = this.props
    const forum = forums[screen.currentForum] || {}
    return (
      <AppBar position="fixed">
      <Toolbar>
        <IconButton
          onClick={e => dispatch({type: 'MENU_SHOW'})}
         color="inherit" aria-label="Menu">
          <MenuIcon />
        </IconButton>
        <Typography variant="title" color="inherit" >
          {forum.title || "Welcome! Have a great meeting!"}
        </Typography>
        {Object.values(session.users)
          .map(user => this.userBadge(user, members, screen))}
      </Toolbar>
    </AppBar>
    )
  }
}

const mapStateToProps = state => {
  // props here become props in the component
  return {
    screen: state.screen,
    forums: state.forums,
    members: state.members,
    session: state.session,
  }
}

const AppBar1 = connect(mapStateToProps)(AppBarRaw)
export default AppBar1
