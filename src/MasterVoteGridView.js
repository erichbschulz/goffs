import React, { Component } from 'react'
// import U from './Utils.js'
import PropTypes from 'prop-types'
import {pieColors} from './Theme.js'
import Tooltip from 'material-ui/Tooltip'
import Typography from 'material-ui/Typography'
import DoneIcon from 'material-ui-icons/Done'
import ClearIcon from 'material-ui-icons/Clear'
import U from './Utils.js'
import { Link} from 'react-router-dom'
import Table, {
  TableBody,
  TableCell,
//  TableFooter,
  TableHead,
//  TablePagination,
  TableRow,
//  TableSortLabel,
} from 'material-ui/Table'

class MasterVoteGrid extends Component {

  renderMood = (mood = {}, userId) => {
    const iconStyle = {width: '11px', height: '11px'}
    const color = support => support > 0
      ? pieColors.green
      : support < 0
      ? pieColors.red
      : pieColors.grey
    const icon = support => support > 0
      ? <DoneIcon style={iconStyle}/>
      : support < 0
      ? <ClearIcon style={iconStyle}/>
      : support

    return <TableCell
      padding={'none'}
      key={userId}
      style={{
        background: color(mood.support),
        textAlign: 'center',
        fontSize: 6}}>
        {icon(mood.support)}
      </TableCell>
  }

  render() {
    // sort items
    // make hash of motionId - rowId
    // make hash of memberId - colId
    // headder row + totals

    const {items, motions, members} = this.props
    const sortedItems = Object.values(items).sort((a, b) => a.order > b.order)
    const memberIndex = {}
    const memberList = []
    let i = 0
    for (const userId in members) {
      memberIndex[userId] = i
      memberList[i] = userId
      i++
    }

    return <div>
    <Table>
    <TableHead>
      <TableRow><TableCell padding={'none'}>
       <Typography variant="body1" align="right">
         Motion</Typography>
       </TableCell>
      {memberList.map(userId =>
          <TableCell padding={'none'} key={userId}>
            <Tooltip title={members[userId].title}>
              <Typography variant="body1" align="right">
                {members[userId].title[0]}
              </Typography>
            </Tooltip>
          </TableCell>
          )}
      </TableRow>
    </TableHead>
    <TableBody>
      {sortedItems.map(item => [
        <TableRow key={item.id}>
          <TableCell colSpan={i+1}>
            <a href={U.itemUrl(item.forumId, item.id)}>
              <Typography variant="body2">
                {item.title}
              </Typography>
            </a>
          </TableCell>
          <TableCell>
            <Typography variant="body1" align="right">
              {item.status}
            </Typography>
          </TableCell>
        </TableRow>,
        item.motion.map(motionId => {
          const motion = motions[motionId] || {}
          const {title, mood = {}, status} = motion
          return <TableRow key={motionId}>
            <TableCell>
            <Typography variant="body1" align="right">
              <Link to={U.motionUrl(item.forumId, item.id, motionId)}>
              {title}
              </Link>
            </Typography>
            </TableCell>
            {memberList.map(userId => this.renderMood(mood[userId], userId))}
            <TableCell>
            <Typography variant="body1" align="right">
              {status}
            </Typography>
            </TableCell>
          </TableRow>
          }),
      ])
      }
      </TableBody>
      </Table>
      </div>
//      <Debug heading='Debug items' val={sortedItems}/>
//      <Debug heading='Debug motions' val={motions}/>
//      <Debug heading='Debug members' val={members}/>
  }
}

MasterVoteGrid.propTypes = {
  items: PropTypes.object.isRequired,
  motions: PropTypes.object.isRequired,
  members: PropTypes.object.isRequired,
}

export default MasterVoteGrid
