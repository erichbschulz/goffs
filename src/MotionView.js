import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { actionCreators  } from './Store'
import U from './Utils.js'
import { availableActionButtons } from './Meta'
import Comments from './CommentView.js'
import Mood from './MoodView.js'
import { MoodPie  } from './Widgets'
import ActionButtons from './ActionButtonView.js'
import Members from './MemberView.js'
import Typography from 'material-ui/Typography'
import Paper from 'material-ui/Paper'
import './MotionView.css'
import {scaleColor, evenPadding} from './Theme.js';
import { LoveIndicatorIcon } from './Widgets.js'
import FieldEdit from './FieldEditView.js'

class MotionRaw extends Component {

  constructor(props) {
    super(props)
    const {dispatch, availableMovers} = props
    // create bound versions of functions so can
    // pass them down to our child later.
    this.actionCreators = bindActionCreators(actionCreators, dispatch)
    const {permissions} = props.session
    this.supportActionButtons = availableActionButtons('support', permissions)
    this.loveActionButtons = availableActionButtons('love', permissions)
    this.statusMeta = U.metaByField.motion.status
    this.descriptionMeta = U.metaByField.motion.description
    this.titleMeta = U.metaByField.motion.title
    // decorate core metatdata with function to get list of members
    this.movedByMeta =
      Object.assign({values: availableMovers}, U.metaByField.motion.movedBy)
    this.secondedByMeta =
      Object.assign({values: availableMovers}, U.metaByField.motion.secondedBy)
  }

  editHandler = (meta, item, newValue) => {
    const {id: field} = meta
    const {forumId, id, type} = item
    const record = {id, type, forumId, [field]: newValue}
    console.log('record', record)
    const updates = [{type: 'update', verb: 'PATCH', record}]
    this.sendUpdates(updates)
  }

  // send an array of updates to the bulk route and handle outcome
  sendUpdates = (records) => {
    this.setState({mode: 'waiting'})
    U.sendBulk(records, this.props.dispatch)
    .catch(error => alert("This cannot be good. I dont know what to do."))
    .then(result => this.setState({mode: 'normal'}))
  }

  handleActionButton = (button, motions) => {
    const {screen, members, dispatch} = this.props
    const activeVoters = U.currentVoters(screen.users, members)
    motions.forEach(motion => {
      const payload = {
        forumId: screen.currentForumId,
        targetType: 'motion',
        targetId: motion.id,
        voters: activeVoters,
        action: {[button.atribute]: button.value}
      }
      dispatch({type: 'VOTE_PATCH', p: payload})
    })
  }

  render() {
    const {
      motion,
      members,
      participants,
      session,
      isDynamic,
      availableMovers,
      } = this.props
    const {permissions} = session
    // FIXME none of these defaults should really be needed
    console.log('xx - availableMovers', availableMovers())
    const {
      id, title, description = '', status = '??', movedBy = [], secondedBy = [],
      mood = {}, moodTally = {}, moodSummary = {}
      } = motion
    const scale = scaleColor(moodSummary.love / participants) || ""
    return (
      <div>
        <div className="goff-motion-view-motion-container">
          <div className="goff-motion-view-motion">
            <div id={id} style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center", }}>
            <Typography variant={'title'} >
            <div style={evenPadding}>
              Motion to:&nbsp;
            <FieldEdit
              meta={this.titleMeta}
              readOnly={isDynamic || !U.has(permissions, 'update motion')}
              onChangeHandler={newValue =>
                this.editHandler({id:'title'}, motion, newValue)}
              value={title} />
            </div>
            </Typography>
            <FieldEdit
              meta={this.statusMeta}
              readOnly={!U.has(permissions, 'update motion')}
              onChangeHandler={newValue =>
                this.editHandler({id:'status'}, motion, newValue)}
              value={status} />
            <LoveIndicatorIcon scale={scale} />
            <ActionButtons
              menu={this.loveActionButtons}
              handler={this.handleActionButton}
              items={[motion]}
            />
            <MoodPie
              mood={moodTally.support || {}}
              total={participants}
              size={"1.4em"} />
            <ActionButtons
              menu={this.supportActionButtons}
              handler={this.handleActionButton}
              items={[motion]}
            />
          </div>
          <Paper>
            <div style={evenPadding}>
            <FieldEdit
              meta={this.descriptionMeta}
              readOnly={isDynamic || !U.has(permissions, 'update motion')}
              onChangeHandler={newValue =>
                this.editHandler({id:'description'}, motion, newValue)}
              value={description} />
            </div>
          </Paper>
          </div>
          <div className="goff-motion-view-movers">
            <div className="goff-motion-view-moved">
            { U.has(permissions, 'update motion')
              ? <FieldEdit
                readOnly={isDynamic}
                meta={this.movedByMeta}
                onChangeHandler={newValue =>
                  this.editHandler({id:'movedBy'}, motion, newValue)}
                value={movedBy || '--'} />
              : <Members
                  heading='Moved'
                  memberList={movedBy}
                  members={members}
                  mode='plain' /> }
            </div>
            <div className="goff-motion-view-seconded">
            { U.has(permissions, 'update motion')
              ?  <FieldEdit
                readOnly={isDynamic}
                meta={this.secondedByMeta}
                onChangeHandler={newValue =>
                  this.editHandler({id:'secondedBy'}, motion, newValue)}
                value={secondedBy || '--'} />
              : <Members
                heading='Seconded'
                memberList={secondedBy}
                members={members}
                mode='plain' /> }
            </div>
          </div>
        </div>
        <Comments isDynamic={isDynamic} parentObj={motion} />
            <Mood
              participants={participants}
              members={members}
              mood={mood}
              tally={moodTally}
              summary={moodSummary} />
    </div>
    )
  }
}
MotionRaw.propTypes = {
  members: PropTypes.object.isRequired,
  motion: PropTypes.object.isRequired,
  isDynamic: PropTypes.bool.isRequired,
//  mode: PropTypes.oneOf['proxy', 'plain']
}
MotionRaw.defaultProps = {
  motion: {title: 'loading'},
}


// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
    // props here become props in the component
  return {
    forum: state.forum,
    members: state.members,
    screen: state.screen,
    session: state.session,
  }
}
 
const Motion = connect(mapStateToProps)(MotionRaw)

export default Motion
