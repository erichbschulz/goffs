import React, { Component } from 'react'
import { connect } from 'react-redux'
//import Utils from './Utils.js'
//import Typography from 'material-ui/Typography'

// component to control overall initialisation and high-level server
// interaction

class CoreRaw extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentWillMount() {
    this.props.dispatch({type: 'SESSION_GET'})
  }

  render() {
    return <div>
      </div>
  }
}

const mapStateToProps = state => { // props here become props in the component
  return {
    session: state.session,
    screen: state.screen,
    items: state.items,
    motions: state.motions,
    comments: state.comments,
  }
}

const Core = connect(mapStateToProps)(CoreRaw)

export default Core
