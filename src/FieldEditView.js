import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
//import Button from 'material-ui/Button'
import {Markdown, TextInput, SelectMenu} from './Widgets.js'
//import Chip from 'material-ui/Chip'

class FieldEditRaw extends Component {

//  constructor(props) {
//    super(props)
//  }

  // note coverage of disabled is limited (TODO)
  render() {
    const {meta, onChangeHandler, value, readOnly, disabled=false} = this.props
    const {type, values} = meta
    switch (type) {
      case 'string':
      case 'number':
        return readOnly
          ? value
          : <TextInput
            value={value}
            meta={meta}
            onChange={onChangeHandler} />
      case 'markdown':
        return readOnly
          ? <Markdown value={value} />
          : <TextInput
            value={value}
            meta={meta}
            onChange={onChangeHandler} />
      // "array" type could be best split into generic list handler
      // only acts to extract element then rewrap in array on completion
      case 'array': // only exists to support member list choosing
      case 'enum':
        return <SelectMenu
            disabled={disabled || readOnly}
            options={values}
            value={value}
            type={type}
            onChange={onChangeHandler} />
      case 'datetime':
        return readOnly
          ? value
          : <TextInput
            value={value}
            meta={meta}
            onChange={onChangeHandler} />
//            defaultValue={value}
//            InputLabelProps={{shrink: true}}
      default:
        throw Error('bad type: ' + type)
    }
  }
}

const mapStateToProps = state => {
  return { // props here become props in the component
//    session: state.session,
//    screen: state.screen,
  }
}

FieldEditRaw.defaultProps = {
//  selectionMode: 'internal'
}

FieldEditRaw.propTypes = {
  meta: PropTypes.object.isRequired,
  onChangeHandler: PropTypes.func.isRequired,
//  onBlurHandler: PropTypes.func,
  value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.array]).isRequired,
  readOnly: PropTypes.bool.isRequired
}

const FieldEdit = connect(mapStateToProps)(FieldEditRaw)

export default FieldEdit
