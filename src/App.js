import React, { Component } from 'react'
import { Provider, connect } from 'react-redux'
import { BrowserRouter as Router, Switch, Route, withRouter }
  from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import store from './Store.js'
import AppBar from './AppBarView.js'
import Draw from './DrawView.js'
import Core from './CoreView.js'
import Theme from './Theme.js'
import './App.css'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import {availableRoutes} from './Meta.js'
//import {Debug} from './Widgets.js'

class Root extends Component {
  render() {
    return <MuiThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <Body />
          </Router>
        </Provider>
      </MuiThemeProvider>
  }
}

class BodyRaw extends Component {
  // https://reacttraining.com/react-router/web/example/auth-workflow
  render() {
    const {session} = this.props
    const {permissions} = session
    if (!permissions) {
      return <h1>Let's do this!</h1>
    }
    const routes = availableRoutes(permissions, this.props.location)
    return <div className="App">
      <AppBar />
      <Draw />
      <div className="App-body">
        <Switch> {
          routes.map(route => <Route
            exact={true}
            key={route.path}
            path={route.path}
            component={route.component}
            />)
        }
        </Switch>
      </div>
      <Core />
      {/*
          <Debug heading='Debug' val={{session}}/>
      */}
    </div>
  }
}
const Body = withRouter(connect(state => ({ // populate in the component
  session: state.session,
  screen: state.screen,
  }))(BodyRaw))

export default Root
