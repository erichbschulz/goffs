import React from 'react'
import DoneIcon from 'material-ui-icons/Done'
import PlaylistAddIcon from 'material-ui-icons/PlaylistAdd'
import ClearIcon from 'material-ui-icons/Clear'
import ThumbDownIcon from 'material-ui-icons/ThumbDown'
import ThumbUpIcon from 'material-ui-icons/ThumbUp'
import ArrowUpwardIcon from 'material-ui-icons/ArrowUpward'
import ArrowDownwardIcon from 'material-ui-icons/ArrowDownward'
import ExpandLessIcon from 'material-ui-icons/ExpandLess'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import TrackChangesIcon from 'material-ui-icons/TrackChanges'
import GridOnIcon from 'material-ui-icons/GridOn'
import ListAltIcon from 'material-ui-icons/FormatListNumbered'
import OpenInNewIcon from 'material-ui-icons/OpenInNew'
import ForumIcon from 'material-ui-icons/Forum'
import BulkImport from './BulkImportView.js'
import User from './UserView.js'
import Help from './HelpView.js'
import Main from './MainView.js'
import Auth from './AuthView.js'
import Forum from './ForumView.js'
import Forums from './ForumsView.js'
import Item from './ItemView.js'
import U from './Utils.js'
import {Redirect} from 'react-router-dom'

// subsets of basic buttons
const B = {
  support: [
    {key: 'support',
     title: 'Indicate support',
     requires:  'rate forum thing',
     icon: <DoneIcon />, atribute: 'support', value: +1},
    {key: 'oppose',
     title: 'Indicate opposition',
     requires:  'rate forum thing',
     icon: <ClearIcon />, atribute: 'support', value: -1},
    ],
  love: [
    {key: 'up',
     title: 'Indicate love',
     requires:  'rate forum thing',
     icon: <ThumbUpIcon />, atribute: 'love', value: +1},
    {key: 'down',
     title: 'Indicate fear',
     requires:  'rate forum thing',
     icon: <ThumbDownIcon />, atribute: 'love', value: -1},
    ],
  time: [
    {key: 'more',
     title: 'Request more time for this item',
     requires:  'rate forum thing',
     icon: <ArrowUpwardIcon />, atribute: 'timeAllowance', value: 'more'},
    {key: 'less',
     title: 'Request less time for this item',
     requires:  'rate forum thing',
     icon: <ArrowDownwardIcon />, atribute: 'timeAllowance', value: 'less'},
    {key: 'earlier',
     title: 'Move item up agenda',
     requires: 'reorder motion',
     icon: <ExpandLessIcon />},
    {key: 'later',
     title: 'Move item down agenda',
     requires: 'reorder motion',
     icon: <ExpandMoreIcon />},
    ],
  forums: [
    {key: 'addForum',
     title: 'Create a new forum',
     requires: 'create forum',
     icon: <PlaylistAddIcon />},
  ],
  forum: [
    {key: 'addItem',
     title: 'Add an item to the agenda',
     requires: 'propose item',
     icon: <PlaylistAddIcon />},
  ],
  forumNav: [
    {key: 'track',
     title: 'Open page to track current agenda item',
     requires: 'use system',
     icon: <TrackChangesIcon />},
    {key: 'agenda',
     title: 'Goto the forum agenda',
     requires: 'use system',
     icon: <ListAltIcon />},
    {key: 'votes',
     title: 'See the votes at this forum',
     requires: 'use system', // need more specific thang
     icon: <GridOnIcon />},
    {key: 'comments',
     title: 'See discussion',
     requires: 'use system',
     icon: <ForumIcon />},
  ],
  itemNav: [
    {key: 'nav',
     title: 'Open this item',
     requires: 'use system', // need more specific thang
     icon: <OpenInNewIcon />},
  ],
  globalItem: [
    {key: 'addMotion',
     title: 'Add a new motion',
     requires: 'propose motion',
     icon: <PlaylistAddIcon />},
  ],
}

const availableActionButtons = (type, permissions) => {
  const f = item => U.has(permissions, item.requires)
  if (B[type]) return B[type].filter(f)
  else switch (type) {
    case 'item': // agenda items
      return [].concat(B.love, B.time).filter(f)
    case 'motion':
      return [].concat(B.love, B.support).filter(f)
    default:
        throw Error('bad type:' + type)
  }
}

const routes = [
  {path: '/auth', component: Auth},
  {path: '/register', component: Auth},
  {path: '/changepassword', component: Auth, requires: 'use system'},
  {path: '/bulkImport', component: BulkImport, requires: 'bulk import'},
  {path: '/users', component: User, requires: 'administer user'},
  {path: '/help', component: Help, requires: 'see help'},
  {path: '/forums', component: Forums, requires: 'use system'},
  {path: '/grid', component: Main, requires: 'use system'},
  {path: '/', component: Forums, requires: 'use system'},
  {path: '/forum/:forumId/:view?', component: Forum, requires: 'use system'},
  {path: '/item/:forumId/:itemId',
    component: Item, requires: 'use system'},
]

const availableRoutes = (permissions, location) => routes.map(route => {
  const redirect = () => {
    console.log('route', route.path)
    console.log('location', location)
    return <Redirect
    to={{pathname: "/auth", state: {from: location }}}
  />
}
  if (route.requires && !U.has(permissions, route.requires)) {
    // console.log('withholding', route.path, route.requires)
    return Object.assign({}, route, {component: redirect})
  }
  else {
    // console.log('allowing', route.path, route.requires)
    return route
  }
})

export {
  availableActionButtons,
  availableRoutes,
}
