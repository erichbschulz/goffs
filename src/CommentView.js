import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import U from './Utils.js'
import { availableActionButtons } from './Meta.js'
import Members from './MemberView.js'
import ActionButtons from './ActionButtonView.js'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import './CommentView.css'
import {scaleColor} from './Theme';
//import Icon from 'material-ui/Icon';
import theme from './Theme.js';
import { LoveIndicatorIcon } from './Widgets.js'

class CommentsRaw extends Component {

  constructor(props) {
    super(props)
    // create bound versions of functions so can
    // pass them down to our child later.
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    const {permissions} = props.session
    this.loveActionButtons = availableActionButtons('love', permissions)
    this.color = theme.palette.primary.light
    this.state = {
      commenting: false
    }
  }

  handleSubmit = (event) => {
    console.log('this.input.value', this.input.value)
    const {dispatch, parentObj, screen} = this.props
    const comment = U.addId('comment', {
      authors: U.currentUsers(screen.users),
      title: this.input.value,
      parentType: parentObj.type,
      parentId: parentObj.id,
      forumId: screen.currentForumId})
    event.preventDefault();
    dispatch({type: 'FORUM_UPSERT', p: comment})
    this.setState({commenting: false})
  }

  handleCancel = (event) => {
    event.preventDefault();
    this.setState({commenting: false})
  }

  onCommenting = () => {
    this.setState({commenting: true})
  }

  handleActionButton = (button, comments) => {
    const {screen, members, dispatch} = this.props
    const activeVoters = U.currentVoters(screen.users, members)
    comments.forEach(comment => {
      const payload = {
        forumId: screen.currentForumId,
        targetType: 'comment',
        targetId: comment.id,
        voters: activeVoters,
        action: {[button.atribute]: button.value}
      }
        console.log('payload', payload)
      dispatch({type: 'VOTE_PATCH', p: payload})
    })
  }

  renderComment = ({parentObj, comment, members, participants, isDynamic}) => {
    const {
      id, title, details, authors,
      // mood = {}, moodTally = {},
      moodSummary = {}
      } = comment
    const scale = scaleColor((moodSummary.love || 0)/ participants) || ""
    return (
      <div key={id}
        className="goff-comment-view-item"
        >
        <div
          className="goff-comment-view-title">
          <div id={id} style={{display: 'inline-flex'}}>
            <Typography variant={'subheading'} >
              {title}
            </Typography>
          </div>
          <div style={{display: 'inline-flex'}}>
            <LoveIndicatorIcon scale={scale} />
          </div>
          <div style={{display: 'inline-flex'}}>
            <ActionButtons
              menu={this.loveActionButtons}
              handler={this.handleActionButton}
              items={[comment]}
            />
          </div>
          <div style={{display: 'inline-flex'}}>
          <div className="goff-comment-view-author" >
            <Members heading='By' memberList={authors} members={members} mode='plain' />
          </div>
          </div>
        </div>

          <Typography variant='body1' >
            {details}
          </Typography>
          <Comments isDynamic={isDynamic} parentObj={comment} />
        </div>)
  }

  render() {
    const {
      parentObj,
      comments,
      members,
      screen,
      session,
      isDynamic,
      } = this.props
    const {permissions} = session
    const {participants, showComments} = screen
    // abandon if we are not showing comments
    if (!showComments || !U.has(permissions, 'see comment')) return null
    // else carry on
    const {commenting} = this.state
    this.color = theme.palette.primary.light
    const activeComments = Object.values(comments)
      .filter(comment => {
        return comment.parentType === parentObj.type &&
        comment.parentId === parentObj.id})
    return <div>
      {commenting
          ? <div style={{borderColor: this.color}}
            className="goff-comment-view-container" >
              <form onSubmit={this.handleSubmit}>
                <TextField autoFocus multiline fullWidth rows="2"
                  label="Polite and friendly comments and questions:"
                  placeholder="Enter your thoughts here"
                  id="comment"
                  inputRef={(input) => this.input = input}
                  helperText='Please use the thumbs rather than "I (dis)agree" style comments.'
                  type="text" />
                <Button variant="flat" color="primary" size="small"
                onClick={this.handleSubmit}>
                  Submit
                </Button>
                <Button variant="flat" color="secondary" size="small"
                onClick={this.handleCancel}>
                  Cancel
                </Button>
              </form>
            </div>
         : U.has(permissions, 'create comment') && !isDynamic
         ? <Button variant="flat" color="primary" size="small"
               onClick={this.onCommenting}>
              {parentObj.type === 'comment' ? "Reply" : "Comment"}
            </Button>
         : null
        }
        { activeComments.length
          ? <div style={{borderColor: this.color}}
            className="goff-comment-view-container" >
            {activeComments.map(comment =>
                this.renderComment({
                  parentObj, comment, members, participants, isDynamic}))}
            </div>
          : null
        }
    </div>
  }
}

// inputProps={inputProps}


CommentsRaw.propTypes = {
  parentObj: PropTypes.object.isRequired,
  comments: PropTypes.object.isRequired,
  isDynamic: PropTypes.bool.isRequired,
}
CommentsRaw.defaultProps = {
//  mode: 'proxy',
}


// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
    // props here become props in the component
  return {
    members: state.members,
    comments: state.comments,
    screen: state.screen,
    session: state.session,
  }
}
 
const Comments = connect(mapStateToProps)(CommentsRaw)

export default Comments
