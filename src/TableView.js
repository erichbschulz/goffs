// curtesy of
// https://material-ui-next.com/demos/tables/
import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import Paper from 'material-ui/Paper'
import Checkbox from 'material-ui/Checkbox'
import Tooltip from 'material-ui/Tooltip'
//import IconButton from 'material-ui/IconButton'
//import FilterListIcon from 'material-ui-icons/FilterList'
import { lighten } from 'material-ui/styles/colorManipulator'
import ActionButtons from './ActionButtonView'
import FieldEdit from './FieldEditView'
import U from './Utils'
import './TableView.css'

class EnhancedTableHead extends React.Component {

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property)
  }

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount,
      meta,
      showSelectBoxes} = this.props
    return (
      <TableHead>
        <TableRow>
          {showSelectBoxes
           ? <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
            </TableCell>
          : null }
          {meta.map(column => {
            return (
              <TableCell key={column.id}
                numeric={column.type !== 'text'}
                padding={column.padding ? 'default' : 'none'}
                sortDirection={orderBy === column.id ? order : false} >
                <Tooltip
                  title="Sort"
                  placement={column.type === 'numeric'
                    ? 'bottom-end'
                    : 'bottom-start'}
                  enterDelay={300} >
                  <TableSortLabel
                    active={orderBy === column.id}
                    direction={order}
                    onClick={this.createSortHandler(column.id)} >
                    {column.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            )}, this)}
        </TableRow>
      </TableHead>
    )
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
}

const toolbarStyles = theme => ({
  root: { paddingRight: theme.spacing.unit},
  highlight:
    theme.palette.type === 'light'
      ? { color: theme.palette.secondary.dark,
          backgroundColor: lighten(theme.palette.secondary.light, 0.4)}
      : { color: lighten(theme.palette.secondary.light, 0.4),
          backgroundColor: theme.palette.secondary.dark},
  spacer: { flex: '1 1 100%'},
  actions: { color: theme.palette.text.secondary},
  title: { flex: '0 0 auto'},
})

let EnhancedTableToolbar = props => {
  const { classes, heading,
      menu,
      globalMenu,
      buttonHandler,
      selected,
  } = props


  const numSelected = selected.length
  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })} >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography variant="subheading">{numSelected} selected</Typography>
        ) : (
          <Typography variant="subheading">{heading}</Typography>
        )}
      </div>
      <div className={classes.actions}>
        {numSelected > 0
          ? <ActionButtons menu={menu} handler={buttonHandler} items={selected} />
          : <ActionButtons menu={globalMenu} handler={buttonHandler} items={[]} />
        }
      </div>
    </Toolbar>
  )
}

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
}

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar)

const styles = theme => ({
  root: { width: '100%', marginTop: theme.spacing.unit * 0},
  table: { minWidth: 400},
  tableWrapper: { overflowX: 'auto'},
})

class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = props.startingState
    const {order, orderBy} = this.state
    this.state.sortedData = this.sort(props.data, orderBy, order)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // length of data has changed
    if (prevProps.data.length !== this.props.data.length) {
      this.setState({sortedData: this.props.data})
    }
    else {
      const {orderBy, order} = this.state
      const valFunction = this.sortValFunction(orderBy) // based on meta
      const prevSortCol = prevProps.data.map(valFunction)
      const sortCol = this.props.data.map(valFunction)
      const sameOrder = U.arraysEqual(prevSortCol, sortCol)
      // trigger resort
      if (!sameOrder) {
        this.setState({sortedData: this.sort(this.props.data, orderBy, order)})
      }
    }
  }

  handleRequestSort = (event, orderBy) => {
    let order = 'asc'
    if (this.state.orderBy === orderBy && this.state.order === 'asc') {
      order = 'desc'
    }
    const sortedData = this.sort(this.props.data, orderBy, order)
    this.setState({sortedData, order, orderBy})
  }

  // use the meta to return a function (row) => sortVal
  sortValFunction = orderBy => {
    const columnMeta = this.props.meta.find(n => n.id === orderBy) || {}
    // function to either calculate or lookup value
    return columnMeta.sortValue
      || columnMeta.value
      || (row => row[orderBy])
  }

  // sort the data and update column
  sort = (data, orderBy, order) => {
    const val = this.sortValFunction(orderBy) // get function based on meta
    const sort = (a, b) => ((order==='desc') === (val(b) < val(a))) ? -1 : 1
    return data.slice().sort(sort)
  }

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.props.data.map(n => n.id) })
    }
    else {
      this.setState({ selected: [] })
    }
  }

  // update selected
  handleClick = (event, id) => {
    const { selected } = this.state
    const selectedIndex = selected.indexOf(id)
    switch (this.props.selectionMode) {
      case 'internal': {
        let newSelected = []
        if (selectedIndex === -1) { // add item
          newSelected = newSelected.concat(selected, id)
        } else if (selectedIndex === 0) {
          newSelected = newSelected.concat(selected.slice(1))
        } else if (selectedIndex === selected.length - 1) {
          newSelected = newSelected.concat(selected.slice(0, -1))
        } else if (selectedIndex > 0) {
          newSelected = newSelected.concat(
            selected.slice(0, selectedIndex),
            selected.slice(selectedIndex + 1)) }
        this.setState({ selected: newSelected })
        if (newSelected.length === 1) {
          this.props.focusOnItem && this.props.focusOnItem(newSelected[0])
        }
      }
      break
      case 'external': // esentially a special mode for choosing proxies...
        this.props.focusOnItem(id)
      break
      default:
        throw Error('bad selectionMode')
    }
  }

  handleChangePage = (event, page) => {
    this.setState({ page })
  }

  // transform a cell to JSX based on the meta
  cellValue = (columnMeta, row) => {
    let val = columnMeta.value ? columnMeta.value(row) : row[columnMeta.id]
    if (typeof val !== 'string'
        && typeof val !== 'number'
        && !React.isValidElement(val)) {
      console.error('bad value', val)
      val = 'error'
    }
    return columnMeta.editable
    ? <FieldEdit
        meta={columnMeta}
        readOnly={false}
        onChangeHandler={value =>
          this.props.editHandler(columnMeta, row, value)}
        value={val} />
    : columnMeta.type === 'numeric' && isNaN(val) ? '!' : val
  }

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value })
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1

  // calls the supplied action handler after looking up the id in the collection
  handler = (button, ids) => {
    const data = this.props.data
    const items = data.filter(row => ids.find(id => (id === row.id)))
    this.props.buttonHandler(button, items)
  }

  render() {
    const {
      classes, selectionMode,
      heading, data,
      menu,
      globalMenu = [],
      meta
    } = this.props
    const { sortedData,
      order, orderBy, selected,
      rowsPerPage: rpp, page} = this.state
    const showSelectBoxes = selectionMode === 'internal'
    const emptyRows = rpp - Math.min(rpp, data.length - page * rpp)
    const colSpan = meta.length + 2
    const visibleRows = sortedData.slice(page*rpp, (page+1)*rpp)
    var tableClasses = classes.table
    if (true) tableClasses += " goff-table-view-compact"
    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar
          heading={heading}
          menu={menu}
          globalMenu={globalMenu}
          buttonHandler={this.handler}
          selected={selected}
          />
        <div className={classes.tableWrapper}>
          <Table className={tableClasses}>
            <EnhancedTableHead
              meta={meta}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              showSelectBoxes={showSelectBoxes}
            />
            <TableBody>
              {visibleRows.map(sortedRow => {
                // fixme - this seems very inneficient!
                const row = data.find(r => r.id === sortedRow.id)
                if (row) {
                  const isSelected = this.isSelected(row.id)
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, row.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={row.id}
                      selected={isSelected}
                    >
                    { showSelectBoxes
                      ? <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell> : null }
                      {meta.map(columnMeta => { return (
                        <TableCell
                          numeric={columnMeta.type !== 'text'}
                          padding={columnMeta.padding ? 'default' : 'none'}
                          key={columnMeta.id}
                          >
                          {this.cellValue(columnMeta, row)}
                        </TableCell>
                      )}, this)}
                    </TableRow>
                  )
                }
                else {
                  return null
                }
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={colSpan} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  colSpan={colSpan}
                  count={data.length} rowsPerPage={rpp} page={page}
                  backIconButtonProps={{'aria-label': 'Previous Page'}}
                  nextIconButtonProps={{'aria-label': 'Next Page'}}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </Paper>
    )
  }
}

EnhancedTable.defaultProps = {
  selectionMode: 'internal'
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  heading: PropTypes.string,
  meta: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  buttonHandler: PropTypes.func.isRequired,
  editHandler: PropTypes.func,
  focusOnItem: PropTypes.func,
  startingState: PropTypes.object.isRequired,
  selectionMode: PropTypes.string.isRequired,
}

export default withStyles(styles)(EnhancedTable)

