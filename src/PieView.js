import React, { Component } from 'react'
import PieChart from 'react-minimal-pie-chart';

class Pie extends Component {

//  constructor(props) {
//    super(props)
//  }
//
  render() {
    const {size, data} = this.props
    return (
        <PieChart data={data}
          lineWidth={75}
          startAngle={-90}
          style={{ height: size, width: size}}
            />
    )
  }
}

export default Pie
