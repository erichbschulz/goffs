import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { Duration } from 'luxon'
import { connect } from 'react-redux'
import { actionCreators  } from './Store.js'
import U from './Utils.js'
import {padding} from './Theme.js'
import Table from './TableView.js'
import Icon from 'material-ui/Icon';
import Grid from 'material-ui/Grid'
import {scaleColor} from './Theme.js';
import ActionButtons from './ActionButtonView.js'
import MasterVoteGrid from './MasterVoteGridView.js'
import MasterComment from './MasterCommentView.js'
import {availableActionButtons} from './Meta.js'
import humanizeDuration from 'humanize-duration'
import Typography from 'material-ui/Typography'
import Tooltip from 'material-ui/Tooltip'
import { LoveIndicatorIcon, Form } from './Widgets.js'

// TODO - split this into a shell route-aware componenent and the core table

class ForumRaw extends Component {

  constructor(props) {
    super(props)
    const {dispatch, session} = props
    const {permissions} = session
    this.baseline = {
      mode: 'normal',
    }
    const buttons = this.makeActionButtons(permissions, props)
    this.state = Object.assign({
      buttons,
      agendaTable: this.makeAgendaTable(buttons)
      }, this.baseline)
    // create bound versions of functions so can
    // pass them down to our child later.
    this.actionCreators = bindActionCreators(actionCreators, dispatch)
  }

  resetModeToNormal = () => this.setState(this.baseline)

  // this componenent operates in two separate modes
  // split mode - sends updates via upating screen.currentSessionId
  // whole screen - uses routes to navigate
  isFullScreen = (props) => {
    const match = props.match
    return !!(match && match.params)
  }

  makeActionButtons = (permissions, props) => {
    const buttons = {
      forum: availableActionButtons('forum', permissions),
      item: availableActionButtons('item', permissions),
      time: availableActionButtons('time', permissions),
      love: availableActionButtons('love', permissions),
      nav: availableActionButtons('forumNav', permissions),
      itemNav: availableActionButtons('itemNav', permissions)
    }
    return buttons
  }

  handleActionButton = (button, selectedItems) => {
    const {screen, members, dispatch, items, history} = this.props
    const forumId = screen.currentForumId
    const activeVoters = U.currentVoters(screen.users, members)
    const {key, atribute, value} = button
    switch (key) {
    case 'track':
      U.navigateTo(U.itemUrl(forumId, 'active'), forumId, history)
    break
    case 'votes':
      U.navigateTo(U.forumUrl(forumId)+'/votes', forumId, history)
    break
    case 'comments':
      U.navigateTo(U.forumUrl(forumId)+'/comments', forumId, history)
    break
    case 'agenda':
      U.navigateTo(U.forumUrl(forumId), forumId, history)
    break
    case 'nav':
      selectedItems.forEach(item => {
        const itemId = item.id
        U.navigateTo(U.itemUrl(forumId, itemId), itemId, history)
      })
    break
    case 'addItem': {
      const item = U.buildObj({type: 'item'}, {forumId})
      this.setState({mode: 'edit', item})
    }
    break
    case 'later':
    case 'earlier': {
      // find other one
      const updates = U.itemReorderPatches(key, selectedItems[0], items)
      if (updates) {
        this.sendUpdates(updates)
      }
    }
    break
    default:
      // FIXME should be general function
      selectedItems.forEach(item => {
        const payload = {
          forumId,
          targetType: 'item',
          targetId: item.id,
          voters: activeVoters,
          action: {[atribute]: value}
        }
        dispatch({type: 'VOTE_PATCH', p: payload})
        if (selectedItems.length === 1) { // set the focust to a single item
          this.actionCreators.setCurrentItem({itemId: selectedItems[0].id})
        }
      })
    }
  }

  focusOnItem = (itemId) => {
    this.actionCreators.setCurrentItem({itemId})
  }

  // send an array of updates to the bulk route and handle outcome
  sendUpdates = (records) => {
    this.setState({mode: 'waiting'})
    U.sendBulk(records, this.props.dispatch)
    .catch(error => alert("This cannot be good. I dont know what to do."))
    .then(result => this.setState({mode: 'normal'}))
  }

  // uses state as source of buttons
  makeAgendaTable(buttons) {
    const {screen, session} = this.props
    const getParticipants = () => screen.participants
    const {permissions} = session
    return {
      startingState: {
        page: 0,
        rowsPerPage: 10,
        order: 'asc',
        orderBy: 'order',
        selected: [],
      },
      meta: [
        { id: 'open', type: 'text',  label: 'Open',
          value: row => <ActionButtons
                menu={buttons.itemNav}
                handler={this.handleActionButton}
                items={[row]} />
        },
        { id: 'title', type: 'text',  label: 'Item' },
        { id: 'status',
          editable: U.has(permissions, 'update item'),
          label: 'Status' },
        { id: 'order', type: 'text',  label: 'Order' },
        { id: 'motions', label: 'Motions',
          type: 'numeric',
          value: row => (row.motion || []).length
        },
        { id: 'engagement', label: 'Engagement',
          type: 'chart',
          sortValue: row => Object.keys(row.mood || {}).length,
          value: row => {
            const n = Object.keys(row.mood || {}).length
            const scale = scaleColor(n / getParticipants())
            return < div>
            <Tooltip placement="bottom-end"
            title="Darker colour indicates more people have engaged with this item">
            <Icon style={{color: scale}}>group</Icon>
            </Tooltip>
            </div>
          }
        },
        { id: 'love', label: 'Love',
          type: 'chart',
          sortValue: row => row.moodSummary.love,
          value: row => {
            const scale = scaleColor(row.moodSummary.love / getParticipants())
              || ""
            return <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center", }}>
              <LoveIndicatorIcon scale={scale} />
              <ActionButtons
                menu={buttons.love}
                handler={this.handleActionButton}
                items={[row]}
              />
            </div>
          }
        },
        { id: 'timeAllocated', label: 'Allocated',
          editable: U.has(permissions, 'update item'),
          type: 'numeric',
        },
        { id: 'timeElapsed', label: 'Elapsed',
          type: 'text',
          padding: true,
          sortValue: this.elapsed,
          value: row =>
            Duration.fromObject({milliseconds: this.elapsed(row)})
              .toFormat('hh:mm:ss') //+ ' (' + this.elapsed(row) + ')'
        },
        { id: 'remaining', label: 'Remaining',
          type: 'text',
          sortValue: this.remaining,
          padding: true,
          value: row => ' ' +
            Duration.fromObject({milliseconds: this.remaining(row)})
              .toFormat('hh:mm:ss') //+ ' (' + this.remaining(row) + ')'
        },
        { id: 'timeAllowance', label: 'Requested',
          type: 'chart',
          sortValue: row => row.moodSummary.timeAllowance,
          value: row => {
            return <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center", }}>
              <div>
              {row.moodSummary.timeAllowance}
              </div>
            <ActionButtons
              menu={buttons.time}
              handler={this.handleActionButton}
              items={[row]}
            />
          </div>
          },
        },
        // decorate each column definition with column metadata:
      ].map(column => Object.assign(column, U.metaByField.item[column.id])),
    }
  }

  elapsed(item) {
    return item.status === 'active'
      ? U.elapsed(item.history, U.now())
      : item.timeElapsed
  }

  remaining = item => U.remaining(item, U.now())

  editForm() {
    const fields = [{
      name: 'title',
      label: "Agenda item title",
      placeholder: "Enter brief title here",
      type: "text",
      value: '',
    }]
    const buttons = [{text: "Submit"}]
    const onSubmit = (values, button) => {
      const {dispatch, items} = this.props
      const order = Object.keys(items).length + 1
      console.log('items', items)
      const item = Object.assign(values, this.state.item, {order})
      dispatch({type: 'FORUM_UPSERT', p: item})
      this.resetModeToNormal()
    }
    return <Form
      fields={fields}
      buttons={buttons}
      onCancel={this.resetModeToNormal}
      onSubmit={onSubmit}
      />
  }

  // TODO:
  //   if item status X=>active, then set other items active=>complete
  editHandler = (meta, item, newValue) => {
    const {id: field} = meta
    const {forumId, id, type} = item
    const record = {id, type, forumId, [field]: newValue}
    const updates = [{type: 'update', verb: 'PATCH', record}]
      console.log('>>>updates', updates)
    if (field === 'status' && newValue === 'active') {
      Object.values(this.props.items)
        .filter(item => item.status === 'active')
        .forEach(item => updates.push({
          type: 'update',
          verb: 'PATCH',
          record: {id: item.id, type, forumId, status: 'complete'}
        }))
    }
      console.log('>>>updates', updates)
    this.sendUpdates(updates)
  }

  // checks for new permssions and regenerates buttons
  componentDidUpdate(prevProps, prevState) {
    const {screen, session, match = {}, dispatch} = this.props
    const {permissions} = session
    const {params = {}} = match
    const {forumId} = params
    const {currentForumId} = screen
    if (forumId && forumId !== currentForumId) {
      // we have a direct route but not currently on the correct forum
      dispatch({type: "CURRENTFORUM_SET", p: forumId})
    }
    if (U.newPerms(prevProps, permissions)) {
      const buttons = this.makeActionButtons(permissions, this.props)
      this.setState({
        buttons,
        agendaTable: this.makeAgendaTable(buttons)})
    }
  }

  renderMain() {
    const {screen, items, forums} = this.props
    const {mode, buttons, agendaTable} = this.state
    const {participants, currentForumId} = screen
    const forum = forums[currentForumId]
    const itemArray = Object.values(items)
    const now = U.now()
    const remaining = U.totalRemaining(items, now)
    const remainingH = humanizeDuration(remaining, {largest: 2, round: true})
    const heading = 'Agenda'
    return forum ? (
      mode === 'edit'
      ? this.editForm()
      : itemArray.length
      ?  <div>
          <Table
            heading={heading}
            meta={agendaTable.meta}
            data={itemArray}
            menu={buttons.item}
            globalMenu={buttons.forum}
            buttonHandler={this.handleActionButton}
            editHandler={this.editHandler}
            focusOnItem={this.focusOnItem}
            startingState={agendaTable.startingState}
          />
          <Typography variant="body1" gutterBottom align="right">
            { participants ? participants + ' participants. ' : null }
            { remainingH } remaining.
          </Typography>
        </div>
      : <Grid container spacing={0} >
          <Grid item style={padding}>
            <Typography variant={'title'} > No Items! Woohoo!! </Typography>
          </Grid><Grid item style={padding}>
            <ActionButtons menu={buttons.forum}
              handler={this.handleActionButton} items={[]} />
          </Grid>
        </Grid>
      )
      : <div>Loading</div>
  }

  render() {
    const {screen, forums, items, motions, members, comments, match = {}} = this.props
    const {params = {}} = match
    const {currentForumId} = screen
    const forum = forums[currentForumId] || {}
    const {view = 'agenda'} = params
    return <div>
      <Grid container spacing={0} >
        <Grid item style={padding}>
          <Typography variant={'title'} >{forum.title}</Typography>
        </Grid>
        <Grid item style={padding}>
          <ActionButtons menu={this.state.buttons.nav}
           hide={item => item.key === view}
            handler={this.handleActionButton} items={[forum]} />
        </Grid>
      </Grid>
      {(() => {
      switch (view) {
        case 'votes':
          return <MasterVoteGrid
             items={items}
             motions={motions}
             members={members}
            />
        case 'comments':
          return <MasterComment
             items={items}
             motions={motions}
             comments={comments}
            />
        case 'agenda':
          return this.renderMain()
        default:
          return <div>unknown view: {view}</div>
      }
      })()}
    </div>
  }
}

// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
    // props here become props in the component
  return {
    forums: state.forums,
    items: state.items,
    members: state.members,
    comments: state.comments,
    motions: state.motions,
    screen: state.screen,
    session: state.session,
    clock: state.clock,
  }
}
 
const Forum = connect(mapStateToProps)(ForumRaw)

export default Forum
