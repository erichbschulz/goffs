import React, { Component } from 'react'
//import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import U from './Utils.js'
import {padding} from './Theme.js'
import ActionButtons from './ActionButtonView.js'
import {default as T} from 'material-ui/Typography'
//import {default as T} from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import Chip from 'material-ui/Chip'
//import Card, { CardContent } from 'material-ui/Card'
import Input, { InputLabel } from 'material-ui/Input'
import { MenuItem } from 'material-ui/Menu'
import {
  FormLabel,
  FormControl,
  FormGroup,
  FormControlLabel,
  FormHelperText} from 'material-ui/Form'
import Checkbox from 'material-ui/Checkbox'
import Select from 'material-ui/Select'
import Button from 'material-ui/Button'
import { Debug, /*ExpandLite */} from './Widgets'
//import RefreshIcon from 'material-ui-icons/Refresh'
//import SupervisorAccountIcon from 'material-ui-icons/SupervisorAccount'
import FlightLandIcon from 'material-ui-icons/FlightLand'
import FlightTakeoffIcon from 'material-ui-icons/FlightTakeoff'
import PersonAddIcon from 'material-ui-icons/PersonAdd'
import PersonOutlineIcon from 'material-ui-icons/PersonOutline'
import LockOpenIcon from 'material-ui-icons/LockOpen'
import StarIcon from 'material-ui-icons/Star'
//import StarHalfIcon from 'material-ui-icons/StarHalf'
//import StarBorderIcon from 'material-ui-icons/StarBorder'
import Table from './TableView'
import {resetPassword} from './Server.js'

/*
 * Handles users in context of a forum
 */

class UserRaw extends Component {

  constructor(props) {
    super(props)
    this.state = Object.assign({
      mode: 'normal',
      // try to use current or just take the first
      selectedForum: props.screen.currentForumId
        || U.firstKey(props.forums) || "",
      grantRevokeChoice: 'grant',
    }, this.roleStates(this.roleContexts))
    this.userTable = this.makeUserTable()
  }

  // metadata for the two different role contexts
  roleContexts = [{
      userProperty: "roles", // live on the `user` object
      roleList: U.globalRoles,
      statePrefix: "role_",
      type: "user", // global roles live in the "user" object
      }, {
      roleList: U.localRoles, // live on the `member` object
      statePrefix: "localRole_",
      userProperty: "forumRoles",
      type: "member",
    }]

  // accumulate the state properties for each context
  roleStates = contexts => {
    const states = {
      selectedRoles: 0
    }
    contexts.forEach(context =>
      context.roleList.forEach(role =>
        states[context.statePrefix + role] = false))
    return states
  }

  componentDidMount() {
    this.refreshFullUsers()
  }

  refreshFullUsers() {
    const {dispatch} = this.props
    dispatch({type: 'FULLUSERS_GET', p: {forumId: this.state.selectedForum}})
  }

  arrivedAction = {key: 'arrived',
     title: "Record arrival of members",
     color: "secondary",
     icon: <FlightLandIcon />}
  departedAction = {key: 'departed',
     title: "Record departure of members",
     icon: <FlightTakeoffIcon />}
  proxyAddAction = {key: 'proxyAdd',
     title: "Add proxies to these members",
     icon: <PersonAddIcon />}
  proxyRemoveAction = {key: 'proxyRemove',
     title: "Remove proxies from these members",
     icon: <PersonOutlineIcon />}
  resetPasswordAction = {key: 'resetPassword',
     title: "Reset these members' passwords",
     icon: <LockOpenIcon />}
  grantRevokeAction = {key: 'grantRevoke',
     title: "Add or remove roles form users",
     icon: <StarIcon />}

  actions = () => {
    const {mode, selectedForum} = this.state
    if (mode === 'normal') {
      const generalActions = [
        this.grantRevokeAction,
        this.resetPasswordAction,
        ]
      if (selectedForum) {
        return generalActions.concat([
          this.departedAction,
          this.arrivedAction,
          this.proxyAddAction,
          this.proxyRemoveAction,
          // refresh
          // reset pin
          // set roles
        ])
      }
      else {
        return generalActions
      }
    }
    return []
  }

  handleForumChange = (event) => {
    this.setState({selectedForum: event.target.value},
      this.refreshFullUsers
    )
  }

  // generic name -> state handler
  handleInputChange = (event) => {
    const newState = U.onChangeEventToStateUpdate(event)
    const {name, type, checked} = event.target
    // keep track of how many roles we've selected
    if (type === 'checkbox' && /role_/i.test(name)) {
      newState.selectedRoles = this.state.selectedRoles + (checked ? 1 : -1)
    }
    this.setState(newState)
  }

  handleChange = name => (event, checked) => {
    this.setState({ [name]: checked });
  };

  focusOnItem = (user,x) => {
    console.log('user', user)
    const {proxies, mode} = this.state
    if (mode !== 'normal') {
      const index = proxies.indexOf(user)
      if (index === -1) {
        proxies.push(user)
        this.setState({proxies})
      }
      else {
        this.setState({proxies: U.removeArrayItem(proxies, index)})
      }
    }
  }

  resetModeToNormal = () => {
    this.setState({
      mode: 'normal',
      proxies: [],
    })
  }

  ADDASPREFERRED = "Add as preferred"
  ADDASBACKUP = "Add as back-up"
  REPLACEEXISTING = "Replace existing"
  REMOVE = "Remove"

  handleEndOfProxyAllocation = (e) => {
    const action = e.target.textContent // read button text value
    const {recipientList, proxies} = this.state
    const updates = []
    const nProxies = proxies.length
    for (let i=recipientList.length-1; i>=0; i--) {
      const member = this.props.users[recipientList[i]]
      const {id, forumId, proxyTo} =  member
      let newProxies = action === this.REPLACEEXISTING
        ? [] : proxyTo
      for (let j=nProxies-1; j>=0; j--) { // going backwards
        // if sticking at end go in reverse again
        const index = action === this.ADDASBACKUP ? j : nProxies-j-1
        const proxyId = proxies[index]
        if (proxyId !== id) {
          if (action !== this.REPLACEEXISTING) {
            // this covers REMOVE
            newProxies = U.removeArrayItemByValue(newProxies, proxyId)
          }
          if (action === this.ADDASPREFERRED || action === this.REPLACEEXISTING) {
            newProxies.unshift(proxyId) // add at front
          }
          else if (action === this.ADDASBACKUP) {
            newProxies.push(proxyId)
          }
        }
      }
      member.proxyTo = newProxies // this could be bad...
      const record = {id, forumId, type: 'member', proxyTo: newProxies}
      updates.push({type: 'update', verb: 'PATCH', record})
    }
    this.sendUpdates(updates)
  }

  handleResetPassword = (e) => {
    const {selectedForum, selectedUsers} = this.state
    const payload = {
      forumId: selectedForum,
      userIds: selectedUsers.map(user => user.id),
    }
    this.setState({mode: 'waiting'})
    resetPassword(payload)
    .then(result => this.setState({mode: 'normal'}))
  }

  derivePermissionUpdates = (state, roleContexts) => {
    const {selectedForum, selectedUsers, grantRevokeChoice} = state
    const updates = []
    roleContexts.forEach(context => { // context is global or forum specific
      const {roleList, statePrefix, userProperty, type} = context
      selectedUsers.forEach(user => {
        let user_roles = user[userProperty] || []
        let updating = false
        roleList.forEach(role => {
          const roleSelected = state[statePrefix + role]
          if (roleSelected) {
            const hasRole = user_roles.includes(role)
            switch (grantRevokeChoice) {
              case 'grant':
                if (!hasRole) {
                  updating = true
                  user_roles.push(role)
                }
                break
              case 'revoke':
                if (hasRole) {
                  updating = true
                  user_roles = U.removeArrayItemByValue(user_roles, role)
                }
                break
              default:
                throw Error('bad grantRevokeChoice')
          }}
        })
        if (updating) {
          const update = {id: user.id, type, [userProperty]: user_roles}
          if (type === 'member') { // users don't need forumId property
            update.forumId = selectedForum
          }
          updates.push({type: 'update', verb: 'PATCH', record: update})
        }
      })
    })
    return updates
  }

  handleGrantRevoke = (e) => {
    const {state, roleContexts} = this
    const updates = this.derivePermissionUpdates(state, roleContexts)
    this.sendUpdates(updates)
  }

  // send an array of updates to the bulk route and handle outcome
  // does a refresh of the dataset
  sendUpdates = (records) => {
    const forumId = this.state.selectedForum
    const {dispatch} = this.props
    if (records.length) {
      this.setState({mode: 'waiting'})
      U.sendBulk(records, dispatch)
      .catch(error => alert("This cannot be good. I dont know what to do."))
      .then(result => {
        this.resetModeToNormal()
        dispatch({type: 'FULLUSERS_REFRESH', p: {forumId}})
      })
    }
  }

  handleActionButton = (button, users) => {
    const {key} = button
    switch (key) {
      case 'resetPassword': // activate a form
      case 'grantRevoke': // activate a form
        this.setState({
          mode: key,
          selectedUsers: users,
        })
      break
      case 'proxyAdd': // activate a form
      case 'proxyRemove': // activate a form
        const newState = {
          mode: key,
          recipientList: users.map(user => user.id),
          proxies: [],
        }
        this.setState(newState)
      break
      case 'arrived':
      case 'departed':
        this.updateforumStatus(users, key)
        break
      default:
        throw Error('bad key')
    }
  }

  updateforumStatus = (users, action) => {
    const updates = []
    users.forEach(user => {
      const {id, forumId} =  user
      const record = {id, forumId, type: 'member'}
      switch (action) {
        case 'arrived':
          record.forumStatus = 'present'
          break
        case 'departed':
          record.forumStatus = 'absent'
          break
        default:
          throw Error('bad action')
      }
      updates.push({type: 'update', verb: 'PATCH', record})
    })
    this.sendUpdates(updates)
  }

  upsert = payload => {
    const {dispatch} = this.props
    const forumId = this.state.selectedForum
    if (payload) { // dispatch!!!
      if (payload.type === 'member') {
        // member (but not users) need forumId
        payload.forumId = forumId
      }
      console.log('payload', payload)
      dispatch({type: 'FORUM_UPSERT', p: payload})
      dispatch({type: 'FULLUSERS_REFRESH', p: {forumId}})
    }
  }

  userTitle = userId => userId
    ? (this.props.users[userId] || {title: 'err:'+userId}).title
    : '-'

  // user is either and ID or an object
  userChip = user =>
    typeof user === 'object'
    ? <Chip key={user.id} label={user.title} />
    : user // assume we have an id string
    ?  <Chip key={user} label={this.userTitle(user)} />
    : '-'

  makeUserTable() {
    const userTitle = this.userTitle
    return {
      startingState: {
        page: 0,
        rowsPerPage: 20,
        order: 'asc',
        orderBy: 'title',
        selected: [],
      },
      meta: [
        { id: 'title', type: 'text', padding: false, label: 'User' },
        { id: 'forumStatus', type: 'text', padding: false,
          label: 'Forum Status',
          value: row => row.forumStatus ?
            <div>
            <div>
            {row.forumStatus}
            </div>
            <ActionButtons
              menu={[row.forumStatus === 'present' // choose button
                ? this.departedAction
                : this.arrivedAction]}
              handler={this.handleActionButton}
              items={[row]} />
            </div> : "n/a",
          sortValue: row => row.forumStatus
        },
        { id: 'activeProxy', label: 'Current vote holder',
          type: 'text',
          padding: true,
          value: row => userTitle(row.activeProxy)
        },
        { id: 'proxyTo', label: 'Has given proxy To',
          type: 'text',
          padding: true,
          value: row => (row.proxyTo || []).map(userTitle).join(', ')
        },
        { id: 'votingFor', label: 'Voting for',
          type: 'text',
          padding: true,
          value: row => (row.votingFor || []).map(userTitle).join(', ')
        },
        { id: 'forumRoles', label: 'Forum Roles',
          type: 'text',
          padding: true,
          value: row => row.forumRoles ? row.forumRoles.join(', ') : ''
        },
        { id: 'roles', label: 'Roles',
          type: 'text',
          padding: true,
          value: row => row.roles ? row.roles.join(', ') : ''
        },
        { id: 'id', type: 'text', padding: false, label: 'ID' },
        { id: 'status', type: 'text', padding: false, label: 'General Status' },
//        { id: 'love', label: 'Love',
//          type: 'chart',
//          padding: false,
//          sortValue: row => row.moodSummary.love,
//          value: row => {
//            const scale = scaleColor(row.moodSummary.love / getParticipants())
//            return <div
//              style={{
//                display: "flex",
//                justifyContent: "flex-end",
//                alignItems: "center", }}>
//              <div><Icon style={{color: scale}}>favorite</Icon></div>
//              <ActionButtons
//                menu={this.loveActionButtons}
//                handler={this.handleActionButton}
//                items={[row]}
//              />
//            </div>
//          }
//        },
      ],
    }
  }

  memberList = (members) => <div>{members.map(this.userTitle).join(', ')}</div>

  preamble(mode) {
    const {forums} = this.props
    const {selectedForum, selectedUsers, grantRevokeChoice} = this.state
    const cancelButton = () =>
      <Button variant="raised" color="secondary"
        onClick={this.resetModeToNormal}>
        Cancel
      </Button>
    switch (mode) {
      case 'waiting':
        // fixme
        return 'ok......'
      case 'normal':
        return <Grid container spacing={0} >
          <Grid item xs={12} sm={3} style={padding}>
          <FormControl >
            <InputLabel htmlFor="forumInput">Forum</InputLabel>
            <Select
              value={this.state.selectedForum}
              onChange={this.handleForumChange}
              input={<Input name="" id='forumInput' />}
              autoWidth
            >
            <MenuItem value=""><em>None</em></MenuItem>{
              Object.values(forums)
              .map(forum => <MenuItem
                  key={forum.id}
                  value={forum.id}>{forum.title}</MenuItem>)}
            </Select>
            <FormHelperText>Change the forum to see proxy details etc.</FormHelperText>
        </FormControl>
        </Grid>
        <Grid item xs={12} sm={9} style={padding}>
          Use this table to administer members. Each user has a "global" role
          and status. Users also have forum-specific proxies and roles. Choose
          a forum to enable you to update the forum-specific information.
        </Grid>
      </Grid>
      case 'proxyAdd':
      case 'proxyRemove': {
        const {proxies, recipientList} = this.state
        const hasProxies = !!proxies.length
        return <Grid container spacing={0} >
  <Grid item xs={12} md={6} style={padding}>
        <div>
          At {forums[selectedForum].title}
          , {mode === 'proxyAdd' ? 'add' : 'remove'} these proxies:
        </div>
        <div>{ hasProxies
          ? proxies.map(this.userChip)
          : "[Click on the members below to choose members]"
        } </div>
  </Grid>
  <Grid item xs={12} md={6} style={padding}>
        <div>{mode === 'proxyAdd' ? 'to' : 'from'} these members:</div>
        <div>{recipientList.map(this.userChip)}</div>
  </Grid>
  {mode === 'proxyAdd' &&
  <Grid item xs={12} sm={3} style={padding}>
            <Button variant="raised" color="primary"
              disabled={!hasProxies}
              onClick={this.handleEndOfProxyAllocation}>
              {this.ADDASPREFERRED}
            </Button>
  </Grid>
  }
  {mode === 'proxyAdd' &&
  <Grid item xs={12} sm={3} style={padding}>
            <Button variant="raised" color="primary"
              disabled={!hasProxies}
              onClick={this.handleEndOfProxyAllocation}>
              {this.ADDASBACKUP}
            </Button>
  </Grid>
  }
  {mode === 'proxyAdd' &&
  <Grid item xs={12} sm={3} style={padding}>
            <Button variant="raised" color="primary"
              disabled={!hasProxies}
              onClick={this.handleEndOfProxyAllocation}>
              {this.REPLACEEXISTING}
            </Button>
  </Grid>
  }
  {mode === 'proxyRemove' &&
  <Grid item xs={12} sm={3} style={padding}>
            <Button variant="raised" color="primary"
              disabled={!hasProxies}
              onClick={this.handleEndOfProxyAllocation}>
              {this.REMOVE}
            </Button>
  </Grid>
         }
  <Grid item xs={12} sm={3} style={padding}>
        {cancelButton()}
  </Grid>
</Grid>
      }
        // <Card><CardContent> </CardContent></Card>
      case 'resetPassword':
        return <Grid container spacing={0} >
  <Grid item xs={12} style={padding}>
            <T variant="caption">Reset these members' password:</T>
            <div>{ selectedUsers.map(this.userChip)}</div>
  </Grid>
  <Grid item xs={12} sm={6} style={padding}>
            <Button variant="raised" color="primary"
              onClick={this.handleResetPassword}>
              Reset password
            </Button>
  </Grid>
  <Grid item xs={12} sm={3} style={padding}>
        {cancelButton()}
  </Grid>
</Grid>
      case 'grantRevoke': {
            const selectList = (title, collection, statePrefix) =>
              <FormControl component="fieldset">
                <FormLabel component="legend">{title}</FormLabel>
                { collection.map(role =>
                  <FormGroup key={role}>
                    <FormControlLabel control={
                        <Checkbox
                          name={statePrefix + role}
                          checked={this.state[statePrefix + role]}
                          onChange={this.handleInputChange}
                        />
                      }
                      label={role}
                    />
                </FormGroup> )}
              </FormControl>
        return <div>

<Grid container spacing={0} >
  <Grid item xs={12} sm={12} md={3} style={padding}>
            <T variant="caption">For these members:</T>
            <div>{ selectedUsers.map(this.userChip)}</div>
  </Grid>
  <Grid item xs={12} sm={12} md={3} style={padding}>
          <div>
            <FormControl >
              <InputLabel htmlFor="grantRevokeInput">
                I want to:</InputLabel>
              <Select
                name="grantRevokeChoice"
                value={grantRevokeChoice}
                onChange={this.handleInputChange}
                input={<Input name="" id='grantRevoke' />}
                autoWidth
              >
                <MenuItem value="revoke">Revoke</MenuItem>
                <MenuItem value="grant">Grant</MenuItem>
              </Select>
              <FormHelperText>
                Choose between adding or removing roles.
              </FormHelperText>
            </FormControl>
            </div>
  </Grid>
  <Grid item xs={12} sm={6} md={3} style={padding}>
            {selectList(
              <T variant="caption">Roles to {grantRevokeChoice}:</T>,
                U.globalRoles, "role_")}
  </Grid>
  <Grid item xs={12} sm={6} md={3} style={padding}>
            <div>
            {forums[selectedForum] && selectList(
              <T variant="caption">
                Roles to {grantRevokeChoice} for {forums[selectedForum].title}
              </T>,
              U.localRoles, "localRole_")}
            </div>
  </Grid>
  <Grid item xs={3} style={padding}>
              <Button variant="raised" color="primary"
                disabled={!this.state.selectedRoles}
                onClick={this.handleGrantRevoke}>
                {U.capitalise(grantRevokeChoice)}
              </Button>
  </Grid>
  <Grid item xs={3} style={padding}>
              {cancelButton()}
  </Grid>
</Grid>
            </div>
      }
      default:
        throw Error('bad mode: ' + mode)
    }
  }

  render() {
    const {
      mode, // normal|proxyAdd|proxyRemove|resetPassword|grantRevoke
    } = this.state
    const actions = this.actions()
    const showingTable = !['grantRevoke', 'resetPassword'].includes(mode)
    const {users, screen} = this.props
    const userArray = Object.values(users)
    return <div>{this.preamble(mode)}
    {!showingTable ||
        <Table
          heading={"Users"}
          meta={this.userTable.meta}
          data={userArray}
          menu={actions}
          buttonHandler={this.handleActionButton}
          focusOnItem={this.focusOnItem}
          startingState={this.userTable.startingState}
          selectionMode={mode === 'normal' ? 'internal' : 'external'}
        />
    }
    <Debug heading="users" val={userArray || "loading"} />
    <Debug heading="screen" val={screen || "loading"} />
    <Debug heading="state" val={this.state || "loading"} />
    </div>
  }
}

const mapStateToProps = state => {
  return {
    screen: state.screen,
    users: state.fullUsers,
    forums: state.forums,
  }
}
 
const User = connect(mapStateToProps, null, null, {pure: false})(UserRaw)

export default User
