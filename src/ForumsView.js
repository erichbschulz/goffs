import React, { Component } from 'react'
import { connect } from 'react-redux'
import U from './Utils.js'
import FieldEdit from './FieldEditView.js'
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import ActionButtons from './ActionButtonView.js'
import {availableActionButtons} from './Meta.js'
import {Form} from './Widgets.js'
import {evenPadding, padding} from './Theme.js'

class ForumsRaw extends Component {

  constructor(props) {
    super(props)
    this.baseline = {
      mode: 'normal',
    }
    this.state = this.baseline
    const {permissions} = props.session
    this.menu = availableActionButtons('forums', permissions)
    this.navMenu = availableActionButtons('forumNav', permissions)
    this.statusMeta = U.metaByField.forum.status
    this.titleMeta = U.metaByField.forum.title
    this.startMeta = U.metaByField.forum.start
    this.endMeta = U.metaByField.forum.end
    this.endMeta = U.metaByField.forum.end
    this.globalMenu =
        <ActionButtons
          menu={this.menu}
          handler={this.handleActionButton}
          items={[]} />
  }

  editHandler = (meta, forum, newValue) => {
    const {id: field} = meta
    const {id, type} = forum
    const record = {id, type, [field]: newValue}
    const updates = [{type: 'update', verb: 'PATCH', record}]
    // this is a rubish way to do this - ideally send updates would
    // respond with the updated object
    this.sendUpdates(updates)
    .then(() => this.props.dispatch({type: 'FORUMS_GET'}))
  }

  // send an array of updates to the bulk route and handle outcome
  sendUpdates = (records) => {
    this.setState({mode: 'waiting'})
    return U.sendBulk(records, this.props.dispatch)
    .catch(error => alert("This cannot be good. I dont know what to do."))
    .then(result => this.setState({mode: 'normal'}))
  }

  resetModeToNormal = () => this.setState(this.baseline)

  setForum = (forumId, mode) => {
    const {dispatch} = this.props
    dispatch({type: 'CURRENTFORUM_SET', p: forumId})
    this.navigateTo(U.forumUrl(forumId) + (mode ? '/' + mode : ''), forumId)
  }

  navigateTo = (url, name) => {
    const {history} = this.props
    U.navigateTo(url, name, history)
  }

  handleActionButton = (button, items) => {
    const {key} = button
    if (key === 'addForum') {
      const forum = U.buildObj({type: 'forum'}, {})
      this.setState({mode: 'edit', forum})
    }
    else {
      const forumId = items[0].id
      switch (key) {
      case 'agenda':
        this.setForum(forumId)
      break
      case 'votes':
      case 'comments':
        this.setForum(forumId, 'votes')
      break
      case 'track':
        this.navigateTo(U.itemUrl(forumId, 'active'), forumId)
      break
      default:
        throw Error('bad type: ' + key)
      }
    }
  }

  editForm() {
    const fields = [{
      name: 'title',
      label: "Forum title",
      placeholder: "Enter brief title here",
      type: "text",
      value: ''
    }]
    const buttons = [{text: "Create forum"}]
    const onSubmit = (values, button) => {
      const {dispatch} = this.props
      const forum = Object.assign(values, this.state.forum)
      dispatch({type: 'FORUM_CREATEANDFOCUS', p: forum})
      this.setState({mode: 'normal'})
      return false
    }
    return <Form
      fields={fields}
      buttons={buttons}
      onCancel={this.resetModeToNormal}
      onSubmit={onSubmit}
      />
  }

  render() {
    const { mode } = this.state
    const {forums = {}, session} = this.props
    const {permissions} = session
    const sorter = (a,b) => a.start > b.start
    const sortedForums = Object.values(forums).sort(sorter)
    return mode === 'edit'
      ? this.editForm()
      : <div style={padding}>
          <div style={{display: "flex", alignItems: "center", }}>
            <Typography variant={'title'} >
              Available Forums
            </Typography>
            { this.globalMenu }
          </div>
      {sortedForums.length
      ? <div style={evenPadding}>
      {sortedForums.map(forum =>
        <Card key={forum.id} >
          <CardContent>
          {permissions['update forum']
            ?  <Grid container spacing={0} >
                <Grid item xs={5} sm={2}>
                  <Button variant="flat" color="primary"
                    onClick={() => this.setForum(forum.id)}>
                   go
                  </Button>
                </Grid>
                <Grid item xs={1} sm={1} style={padding}>
                  <ActionButtons
                    menu={this.navMenu}
                    handler={this.handleActionButton}
                    items={[forum]} />
                </Grid>
                <Grid item xs={6} sm={2} style={padding}>
                  <FieldEdit
                    meta={this.statusMeta}
                    readOnly={!permissions['update forum']}
                    onChangeHandler={newValue =>
                      this.editHandler({id:'status'}, forum, newValue)}
                    value={forum.status} />
                </Grid>
                <Grid item xs={12} sm={7}>
                  <FieldEdit
                    readOnly={!permissions['update forum']}
                    meta={this.titleMeta}
                    onChangeHandler={newValue =>
                    this.editHandler({id:'title'}, forum, newValue)}
                    value={forum.title} />
                  <FieldEdit
                    readOnly={!permissions['update forum']}
                    meta={this.startMeta}
                    onChangeHandler={newValue =>
                    this.editHandler({id:'start'}, forum, newValue)}
                    value={forum.start}
                    />
                  <FieldEdit
                    readOnly={!permissions['update forum']}
                    meta={this.endMeta}
                    onChangeHandler={newValue =>
                    this.editHandler({id:'end'}, forum, newValue)}
                    value={forum.end}
                    />
                </Grid>
              </Grid>
            : <Button variant="flat" color="primary" size="large"
              onClick={() => this.setForum(forum.id)}>
            {forum.title}
            </Button>
          }
        </CardContent>
      </Card>)}
      </div>
    : <div>
        <Typography variant={'title'} >
          No forum allocated meetings (yet)
        </Typography>
      </div>}
    </div>
  }

}

const mapStateToProps = state => {
  return {
    forums: state.forums,
    session: state.session,
  }
}
 
const Forums = connect(mapStateToProps)(ForumsRaw)

export default Forums
