import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {padding} from './Theme.js'
import Utils from './Utils.js'
import {default as T} from 'material-ui/Typography'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import TextField from 'material-ui/TextField'
import {authorise, changePassword, register} from './Server.js';
import { Redirect } from 'react-router-dom'
import { LinearProgress } from 'material-ui/Progress';

class AuthRaw extends Component {

  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
      mode: "ready",
      userId: "", pin: "",
      oldpin: "", newpin: "",
      title: "", email: "",
//      title: "testy test", email: "test@example.com",
    }
    this.startingState = this.state
  }

  handleInputChange = (event) => {
    this.setState(Utils.onChangeEventToStateUpdate(event))
  }

  action = () => {
    const {pathname} = this.props.location
    switch (pathname) {
      case '/auth':
      case '/changepassword':
      case '/register':
        return pathname.substr(1) // strip leading slash
      default:
        throw Error('bad pathname')
    }
  }

  handleSubmit = (event) => {
    const {dispatch, session} = this.props
    const {users} = session
    const action = this.action()
    const {
      pin, userId, //auth
      oldpin, newpin, //changepassword
      email, title, //register
    } = this.state
    var payload, get
    switch (action) {
    case 'auth':
      payload = {id: userId, pin: pin}
      get = authorise
      break
    case 'changepassword': {
      const user = Utils.firstValue(users)
      payload = {id: user.id, oldpin, newpin}
      get = changePassword
      }
      break
    case 'register': {
      const user = {type: 'user', title, email, pin}
      payload = Utils.buildObj(user)
      get = register
      }
      break
    default:
        throw Error('bad action')
    }
    this.setState({mode: 'waiting'})
    get(payload)
      .then((result) => {
        const {
          success,
          error={},
          data={},
        } = result
        const {auth, actions = []} = data
        console.log('data', data)
        console.log('actions', actions)
        const dispatchResult = actions.map(action => {return dispatch(action)})
        console.log('dispatchResult', dispatchResult)
        if (success && auth) {
          this.setState({mode: 'success'})
        }
        else if (!auth) {
          this.setState({mode: 'denied', errorMessage: error.message})
        }
        else {
          this.setState({mode: 'ready'})
        }
      })
    event.preventDefault()
  }

  render() {
    const {session} = this.props
    const {users} = session
    const action = this.action()
    const {
      mode,
      pin, userId, //auth
      oldpin, newpin, //changepassword
      email, title, //register
      errorMessage
    } = this.state
    const userCount = Object.keys(users).length
    const errors = {}
    var valid
    switch (action) {
      case 'auth':
        valid =  true
        break
      case 'changepassword':
        valid = userCount === 1
        break
      case 'register':
        // FIXME dry out and move to meta data
        errors.pin = Utils.passwordTest(pin)
              ? ''
              : <span>{Utils.passwordRequirements}</span>
        errors.email = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,10}/.test(email)
              ? ''
              : 'Please insert valid email'
        errors.title = /^[A-Za-z]+ [A-Za-z]+[ A-Za-z]*$/.test(title)
              ? ''
              : 'Name should be letters only and at least two words'
        valid = !(errors.pin || errors.email || errors.title)
        break
      default:
        throw Error('bad pathname')
    }
    const user = Utils.firstValue(users)
    return <div><div> {/* --- {mode}-{action} */}
    {mode === 'ready' || mode === 'denied' ////////////////////////////// mode
    ? //// form for each action //////////////////////////////////////////////
    <Grid container spacing={0} alignItems="center" justify="center">
      <Grid item xs={12} style={padding}>{
        // form preamble ----------------------------------------------------
        action === 'auth'
        ? <div>
          <T variant={'title'} >{userCount
            ? "Add another user to this session"
            : "Welcome! Log in"}
          </T>
          <T component={'p'} >
            Entering your ID and password will authenticate you on this device.
            Once you are authenticated whoever is controlling this device may
            vote on your behalf. Your
            concurrence with current voting is controlled by the clicking the
            bar at the top of the screen. Users with a + sign beside their name
            concur and those with a minus abstain with the views recorded.
          </T>
          </div>
        : action === 'changepassword'
        ? <div>
            <T variant={'title'} >Change your password</T>
            {userCount === 1
              ? <div>for {user.title} (#{user.id})</div>
              : <div><T>
                  You need to have a single user logged-in before you can change your
                  password.
                  {userCount === 0
                  ? "Please contact an administrator if you have forgotten your password."
                  : "Please log-out and restart your session with a single user."}</T>
                </div>}
          </div>
        : // register
          <div>
          <T variant={'title'} >Register as a new user</T>
          <T component={'p'} >
            Please complete the form below to register as a new user. If you
            have forgotten your password please contact an administrator to
            have your password reset.
          </T>
          </div>
      }
      <T variant="body2" color='error' align='center'>
        {errorMessage }
      </T>
      </Grid>
      <Grid item xs={12} style={padding}>
        <form onSubmit={this.handleSubmit}>{
        // form Fields ----------------------------------------------------
          action === 'auth'
        ? <Grid item xs={12} style={padding}>
          <Grid container spacing={0} alignItems="center" justify="center">
          <Grid item xs={12} md={6} style={padding}>
          <TextField autoFocus
            label="Your ID number"
            error={mode === 'denied'}
            name="userId" value={userId}
            onChange={this.handleInputChange}
            helperText={mode === 'denied'
              ? <span>Please try again</span> : ''}
            type="text" />
          </Grid>
          <Grid item xs={12} md={6} style={padding}>
          <TextField
            label="Password"
            error={mode === 'denied'}
            name="pin" value={pin}
            onChange={this.handleInputChange}
            type="password" />
          </Grid>
          </Grid></Grid>
        : action === 'changepassword' // --------------------------------------
        ? <Grid item xs={12} style={padding}>
          <Grid container spacing={0} alignItems="center" justify="center">
          <Grid item xs={12} md={6} style={padding}>
          <TextField autoFocus
            disabled={userCount !== 1}
            label="Current password"
            error={mode === 'denied'}
            name="oldpin" value={oldpin}
            onChange={this.handleInputChange}
            helperText={mode === 'denied'
              ? <span>Please try again</span> : ''}
            type="password" />
          </Grid>
          <Grid item xs={12} md={6} style={padding}>
          <TextField
            label="New password"
            disabled={userCount !== 1}
            error={mode === 'denied'}
            name="newpin" value={newpin}
            onChange={this.handleInputChange}
            type="password" />
          </Grid>
          </Grid></Grid>
        : // action === 'register' // --------------------------------------
        <Grid item xs={12} style={padding}>
          <Grid container spacing={0} alignItems="center" justify="center">
          <Grid item xs={12} md={4} style={padding}>
            <TextField autoFocus
              label="Name"
              error={!!(title && errors.title)}
              name="title" value={title}
              onChange={this.handleInputChange}
              helperText={errors.title}
              type="text" />
          </Grid>
          <Grid item xs={12} md={4} style={padding}>
            <TextField autoFocus
              label="Email"
              error={!!(email && errors.email)}
              name="email" value={email}
              onChange={this.handleInputChange}
              helperText={errors.email}
              type="email" />
          </Grid>
          <Grid item xs={12} md={4} style={padding}>
          <TextField
            label="Password"
            error={!!(pin && errors.pin)}
            name="pin" value={pin}
            onChange={this.handleInputChange}
            helperText={errors.pin}
            type="password" />
          </Grid>
          </Grid></Grid>
        }
          <Grid container spacing={0} >
            <Grid item xs={12} md={6} style={padding}>
              <Button
                type="submit" variant="raised" color="primary" size="large"
                disabled={!valid}
                >
                {action === 'auth'
                  ? 'Log-in'
                  : action === 'changepassword'
                  ? 'Change'
                  : 'register' }
              </Button>
            </Grid>
            <Grid item xs={12} md={6} style={padding}>
              <Button variant="raised" color="secondary" size="large"
              onClick={() => this.setState({mode: 'done'})}>
                Cancel
              </Button>
            </Grid>
          </Grid>
        </form>
      </Grid>
      </Grid>
    : mode === 'waiting' //////////////////////////////////////////////// mode
    ? <div><T variant={'title'} >Checking details...</T><LinearProgress /></div>
    : mode === 'success' //////////////////////////////////////////////// mode
    ? <div>{action === 'auth' // ---------------------------------------------
      ? <Grid container spacing={0} alignItems="center" justify="center">
        <Grid item xs={12} style={padding}>
           <T variant={'title'} >Successful log in</T>
           You may have several trusted people sharing the same device. This
           is only appropriate if all the members are physically present and
           participating in the discussion. If a member is not present then
           they should use the proxy mechanism.
        </Grid>
        <Grid item xs={12} md={6} style={padding}>
          <Button variant="raised" color="primary" size="large"
          onClick={() => this.setState({mode: 'done'})}
          >
            Lets go!!
          </Button>
        </Grid>
        <Grid item xs={12} md={6} style={padding}>
          <Button variant="raised" color="secondary" size="large"
          onClick={() => this.setState(this.startingState)}
          >
            Add another user to this device
          </Button>
        </Grid>
      </Grid>
      : action === 'changepassword' // --------------------------------------
      ? <Grid container spacing={0} alignItems="center" justify="center">
          <Grid item xs={12} style={padding}>
            <T variant={'title'} >Successful password change</T>
          </Grid>
          <Grid item xs={12} style={padding}>
            <Button variant="raised" color="primary" size="large"
              onClick={() => this.setState({mode: 'done'})}
          > OK </Button>
          </Grid>
        </Grid>
      : // register ----------------------------------------------------------
        <Grid container spacing={0} alignItems="center" justify="center">
        <Grid item xs={12} style={padding}>
           <T variant={'title'} >Successful registration</T>
           Please write down your password!
        </Grid>
        <Grid item xs={12} md={6} style={padding}>
          <Button variant="raised" color="primary" size="large"
          onClick={() => this.setState({mode: 'done'})}
          >
            Lets go!!
          </Button>
        </Grid>
        <Grid item xs={12} md={6} style={padding}>
          <Button variant="raised" color="secondary" size="large"
          onClick={() => this.setState(this.startingState)}
          >
            Add another user to this device
          </Button>
        </Grid>
      </Grid>
     // eo mode = success
    }</div>
    : mode === 'done' ///////////////////////////////////////////////////// mode
    ? <Redirect to='/' />
    : <div>bad mode: {mode}</div>
    }
  </div></div>
  }
}

AuthRaw.propTypes = {
  members: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
}
AuthRaw.defaultProps = {
//  mode: 'proxy',
}


// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
    // props here become props in the component
  return {
    members: state.members,
    session: state.session,
  }
}
 
const Auth = connect(mapStateToProps)(AuthRaw)

export default Auth
