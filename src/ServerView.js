import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
//import Utils from './Utils.js'
// import Members from './MemberView.js'
//import Typography from 'material-ui/Typography'
//import Button from 'material-ui/Button'
//import TextField from 'material-ui/TextField'
import './CommentView.css'
//import {authorise} from './Server.js';
import {default as T} from 'material-ui/Typography'

class ServerRaw extends Component {

  render() {
//    const {
//      members,
//      session,
//      } = this.props
    return <div>
<T variant='subheading'>Server dev system</T>
        <T variant='body1' gutterBottom>
        need to reset server
        </T>

    </div>
  }
}

ServerRaw.propTypes = {
  members: PropTypes.object.isRequired,
  session: PropTypes.object.isRequired,
}
ServerRaw.defaultProps = {
//  mode: 'proxy',
}

// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
    // props here become props in the component
  return {
    members: state.members,
    session: state.session,
  }
}
 
const Server = connect(mapStateToProps)(ServerRaw)

export default Server
