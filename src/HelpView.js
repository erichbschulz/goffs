import React, { Component } from 'react'
import Card, { CardContent } from 'material-ui/Card'
import Grid from 'material-ui/Grid'
import {padding} from './Theme.js'
import Toolbox from './Toolbox.js'
import {Markdown} from './Widgets.js'

class Help extends Component {

  constructor(props) {
    super(props)
    const {protocol, host} = window.location
    const baseUrl = protocol + '//' + host
    console.log('baseUrl', baseUrl)
    this.exampleMarkdown = `
Create markdown hyperlinks by wrapping the
link text in square brackets and the
URI in round brackets:
[markdown](//marked.js.org/demo/).

You can <u>underline</u>, ~~strike~~,
and **bold** text by enclosing it in the
appropriate tags.
`
    this.help = `

# Philospophy and goals

This app aims to support decision making in large groups by enabling
participants in fora to readily express their feelings and opinions and
making those views more explicit.
The overall goal is to make meetings more effective and more engaging
for participants.

This app is not a replacement for facilitation and good meeting practices but
aims to support both.

The motivation for all this is a hope we can make better use of the tremendous talent we bring together at big fora. The big problem with big meetings is the mathematical result of too many people for the available communication bandwidth. That is we all have lots to say, but only one person can speak at once. There is also a natural tendency for the shy to be drowned out by the confident. By enabling easier expression of views and allowing simultaneous on-line comments we can rapidly track the collective feelings, and allow more people to have a voice. Then our single audio channel should be have much less work do and the meeting can get more done and more reliably reflect our collective will.

# Instructions

## Before you begin

Be aware that this app is a work in progress. Feel free to have a go at using
the system and please report back with your experience and suggestions for
priorities.

That said, the system now has all of the essential features to manage a
meeting, propose motions and make comments.

Note that the system is currently quite laggy. I am working on a solution for that.

## Getting started

There are a set of test users installed in the test system. All users have the
password <code>changeme</code> to start with. User <code>2342</code> (Abby) is
set up with adminstrator priviliges so you can test these functions. User
<code>240854</code> (Heather) is set up with a regular user access.


## Basic operation

Get started by logging in with one of the above test users [here](/auth).

If you hover the mouse over most buttons a tool tip will explain what the
button does. Many fields may be edited in place. These editable fields are
shaded light green.

At the bottom of this page you can see
a list of outstanding tasks and desirable features that are in the pipeline
and below that is
a list of the current features.

Note the updates are a little laggy. This is partly because the system is
keeping all the other browser up-to-date and I haven't gotten around to
optimising the response times yet.

### Express an opinion

Members of a forum may express both binding and
non-binding opinions on motions and other elements of the meeting.

Express *non-binding* feelings using the thumbs up and down icons. These
feelings provide both the forum facilitators and item proponents with
guidance on the overall feeling of the meeting.

Express *binding* support with the tick and cross buttons. The little pie
chart becomes more green or red depending on the level of support.

### Edit the record

Users with appropriate roles are able to edit the agenda and motions. Clicking
on text with a green background will allow you to edit the text.

### Share your device

The system allows multiple users to connect to the same device and each user
may express their own opinions, or may vote as a block. The list of current
users appears on the title bar along with indicators for number of proxies the
user is currently holding and a plus or minus sign to indicate if the user is
currently voting. Simply click on the user name to defer voting.

### Follow the agenda

After you have selected your forum, click on the appropriate menu button to
open a page that will update as the active agenda item changes.

### Format your motions

Motion details use [markdown](//marked.js.org/demo/) formating to enable you to
provide basic formating and hyperlinking to related documents.

As you type in the motion details using markdown text a preview appears below the motion illustrating how your markdown appears after formatting.

For example:

    `

    this.help1 = `

### Manage participation

The [users](/users) page enables meeting administrators to manage users' roles,
    record attendance and track proxies.

You can easilty grant and revoke roles, which may vary depending on the forum.

Tracking proxies is also easy and as you update the attendance or absense of
each participant the system will recalculate the current proxy status for you.

### Link directly to fora and agenda items

Each forum and agenda item has a permanent URI that you can use to cross link
discussions. Internal IDs are quite long, but by navigating to the page in
question you can simply cut and paste the URI.

The forum links that you can use in emails or other documents are in the form:

<pre>  ${baseUrl}/forum/<b>FORUM_ID</b></pre>

The agenda item links are in the form:

<pre>  ${baseUrl}/item/<b>FORUM_ID/ITEM_ID</b></pre>

To cross link motions, using markdown you can provide a relaive link in the
standard markdown format with the link text in [square] bracket followed
immediately by the (URI in round brackets). For example:

<pre>  Please see [last month's meeting](/forum/<b>FORUM_ID</b>).</pre>

Or:

<pre>  This was recently considered [here](/item/<b>FORUM_ID/ITEM_ID</b>).</pre>

# Technical aspects

This code is designed to run on standalone servers that are independant of the
main organisation server cluster. This means that each state and developer may,
if they wanted, fire up their own server. The current demonstration server runs
on a vanilla Digital Ocean ubuntu server with <code>node</code> and <code>redis</code> installed.
Local development is simple in a docker container.

As a standard unix box there are opportunities to use text logs and simple
utilities like <code>screen</code>, <code>tail</code> and <code>grep</code> to enable some quick and simple
audit and logging functionality.

Currently all code is written in <code>ES6</code> using <code>reactjs</code> and <code>express</code>. (A full
set of tools is listed in the <code>TOOLS</code> button at the bottom of this page.)

The inititial client server interaction was RESTfulish but the app now uses
websockets as well to avoid laggy polling.

## Information model

This is a little technical background on the underlying information model used
by the application.

### Fora

Each forum represents a single meeting and has an associated set of agenda
<em>items</em> and members. Each fora is stored in JSON files in a single
directory.

### Items

Items allow managment of the <em>agenda</em> within a forum. Each forum has
multiple items, with each item having a set of motions.

Indicate preferences for time duration with the up and down arrows.

### Motions

Motions are the hard, binding decisions made by meetings. Indicate support
for a motion by using the tick and cross icons. Motions are bound to a single
item, but each item may have many motions.

Small pie charts indicate hard support or opposition to a motion in
red and green.

### Comments

Allow dialog between participants of a fora. Comments are tied to either an
item, a motion, or even another comment.

# Todo list:

https://contact-qld.ubox.greens.org.au/agc/report/related/h/387667/16

  * feature: email id and password on registration
  * feature: have secret code to register
  * feature: allow joining fora by access code
  * bug: time requested is broken (doesn't save)
  * feature: email password reset and details
  * optimise: prevent double call for forum
  * optimise: available movers running multiple times
  * ui: hide denied routes from menu
  * feature: modify bulk import to allow patching forum (new items, motions, comments)
  * enable forum member to edit details of own motion
  * title on proxy numbers
  * move REPLY button to right of comments
  * sort meetings by start date
  * allow secret meetings
  * summary on motion grid
  * view: top comments
  * server: detailed logs at: fid/uid/updates, fid/id/type/updates
  * feature: agenda view - color code passed motions, future motions
  * logging: classify and filter events
  * fix bug with bulk motion import and moved by, ellapsed time
  * betta type validation meta data (extact logic from authorisation, register route)

# Current Features

  * self service account creation (Name, password, email)
  * create forum
  * create agenda items, allocate time, move up and down agenda
  * create motions, and set mover and seconder
  * edit motions details in [markdown](//marked.js.org/demo/)
  * link directly to item, motion, or comment from markdown
  * reorder agenda items
  * display agenda with time ellapsed, allowance and support
  * display members with proxies
  * comment on items, motions and comments
  * share a single device (laptop or tablet) with multiple members
  * flexible layouts
  * visualise agenda in different formats, sort agenda by column
  * display engagement
  * list members support an support
  * observe support and engagement
  * add users and roles in bulk
  * change own password
  * administrators can reset passwords of multiple users
  * allocate and remove proxies in bulk
  * session tracks the users collected roles and forumRoles and provides a list of permissions.
  * easily track current agenda item
  * on server all updates are written to log files for each user and forumId enabling use of standard bash tools like tail to audit processes
`
  }

  render() {
    return (<div>
        <Card><CardContent>
          <Markdown value={this.help} />
          <Grid container spacing={0} >
            <Grid item xs={12} sm={6} style={padding}>
              This markdown:
              <pre>{this.exampleMarkdown}</pre>
            </Grid>
            <Grid item xs={12} sm={6} style={padding}>
              Becomes:
              <Markdown value={this.exampleMarkdown} />
            </Grid>
          </Grid>

          <Markdown value={this.help1} />
        <Toolbox / >
        </CardContent></Card>
    </div>
    )
  }
}

export default Help
