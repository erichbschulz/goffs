import React, { Component } from 'react'
import {connect} from 'react-redux'
//import { BrowserRouter as Router, Switch, Route, Redirect, withRouter }
//  from 'react-router-dom'
import Forum from './ForumView.js'
import Forums from './ForumsView.js'
import Item from './ItemView.js'
import MembersFull from './MembersFullView.js'
import Server from './ServerView.js'
import './App.css'
import ReactGridLayout from 'react-grid-layout'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import U from './Utils.js'

class MainRaw extends Component {

  render() {
    return <MyFirstGrid
            session={this.props.session}
            screen={this.props.screen}
          />
  }
}

// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
  return { // props here become props in the component
    session: state.session,
    screen: state.screen,
  }
}

const Main = connect(mapStateToProps)(MainRaw)

class MyFirstGrid extends Component {

  constructor(props) {
    super(props)
    this.state = this.styleMaker()
  }

  styleMaker = () => {
    const doc = (typeof document === "object") ? document : {}
    if (doc.body && doc.body.clientWidth) {
      const body = doc.body
      return {
        getWidth: () => body.clientWidth
      }
    }
    else {
      console.warn('unable to locate document.body.clientWidth')
      return {
        getWidth: () => 800
      }
    }
  }

  resize = () => this.forceUpdate()

  componentDidMount() {
    window.addEventListener('resize', this.resize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize)
  }

  render() {
    const {
      users
    } = this.props.session
    const {
      gridLock,
      currentForumId,
    } = this.props.screen
    const roles = U.getUsersRoles(users)
    // get layout
    const width =  this.state.getWidth() -10
    const cols = width > 1200 ? 24 : 12
    // shift elements over on big screen
    const col2 = cols === 24 ? 12 : 0
    var layout = [
      {i: 'forum',
          x: 0, y: 0, w: 12, h: 24, minW: 3 },
      {i: 'item',
          x: 0+col2, y: 0, w: 12, h: 24, minW: 3},
      {i: 'members',
          x: 6+col2, y: 0, w: 6, h: 10, minW: 1},
    ];
    if (roles.admin) {
      layout.unshift({i: 'server', x: 0, y: 0, w: 12, h: 10, minW: 1})
    }
    return (
      <div>
        <div id="main-grid">
          <ReactGridLayout
           className="appLayout"
           layout={layout}
           cols={cols}
           rowHeight={20}
           width={width}
           isDraggable={!gridLock}
           isResizable={!gridLock}
           >
          <div key="forum">
            <div className="main-grid-item-container">
            {currentForumId ?  <Forum /> : <Forums />}
          </div></div>
          {roles.admin
            ?
            <div key="server">
              <div className="main-grid-item-container">
                <Server />
              </div>
            </div>
            : <div></div>
          }
          <div key="item">
            <div className="main-grid-item-container">
              <Item />
            </div>
          </div>
          <div key="members">
            <div className="main-grid-item-container">
              <MembersFull />
            </div>
          </div>
        </ReactGridLayout>
      </div>
    </div>
    )
  }
}
export default Main
