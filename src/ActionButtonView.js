import React, { Component } from 'react'
import Button from 'material-ui/Button'
import Tooltip from 'material-ui/Tooltip'
import { connect } from 'react-redux'

class ActionButtonsRaw extends Component {

  handleClick(button, items, e) {
    e.stopPropagation();
    this.props.handler(button, items)
  }

  // expects:
  // items: array of elements applicable to action
  // buttons: [{key, icon, (hide), <etc>}]
  // handler: (button, items)
  render() {
    const size="24px"
    const {menu, items, hide} = this.props
    const buttons = menu
    .filter(button => hide ? !hide(button) : true)
    .map(button => {
      const {key, title, icon, color='primary'} = button
      const buttonJsx =
      <Button
        variant="fab"
        color={color}
        style={{
          fontSize:"0.8rem",
          width: size,
          height: size,
          minHeight: "1rem",
          margin:"4px"}}
        mini
        onClick={(e) => this.handleClick(button, items, e)}
        key={key}
        aria-label={key}
        >
        {icon}
      </Button>
      return title
      ? <Tooltip key={key}
        placement="bottom-end" title={title}>
          {buttonJsx}
        </Tooltip>
      : buttonJsx
    })
    return <div>{buttons}</div>
  }
}

const mapStateToProps = state => {
  return {} // {session: state.session}
}

const ActionButtons = connect(mapStateToProps)(ActionButtonsRaw)

export default ActionButtons
