import {
  combineReducers,
  createStore,
  applyMiddleware
} from 'redux'
import Utils from './Utils.js'
import testState from './TestState.js'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './Saga.js'

/*
 * Things you should never do inside a reducer:
 * Mutate its arguments;
 * Perform side effects like API calls and routing transitions;
 * Call non-pure functions, e.g. Date.now() or Math.random().
 */

// these constants link the action creators to reducers
const a = Utils.actionTypes
export const actionTypes = a

// potentially type checking goes in here
// a = action constants
// DEPRECATED
export const actionCreators = {
  toggleComments: (p) => ({type: a.SCREEN.TOGGLECOMMENTS, p}),
  toggleGridlock: (p) => ({type: a.SCREEN.TOGGLEGRIDLOCK, p}),
  addItem: (p) => ({type: a.FORUM.ADDITEM, p}),
  setCurrentItem: (p) => ({type: a.SCREEN.SETCURRENTITEM, p}),
}

// read the state
export const selectors = {
  sessionUsers: state => state.session.users,
  screenUsers: state => state.screen.users,
  session: state => state.session,
  screen: state => state.screen,
  forumUpdateId: state => {
    return state.screen
  }
  ,
}

/*
 * Generic upserter
 * Will only handle matching types, or votes
 */
function upsert(state, obj, stateType) {
  const {type, id, targetType} = obj
  if (stateType === type) {
    console.log('upserting obj', obj)
    return {...state, [id]: obj}
  }
  else if (type === 'vote' && stateType === targetType) {
    return vote(state, obj)
  }
  else {
    return state
  }
}

/*
 * Generic vote recorder
 * Must only handle matching types
 */
function vote(state, vote) {
  const {userId, action, targetType, targetId} =  vote
  const newObj = Utils.RecordAndSummarise({
    obj: state[targetId],
    voters: [userId],
    action,
    meta: Utils.getMeta(targetType)})
  console.log('voting newObj', newObj)
  return {...state, [targetId]: newObj}
}

/*
SESSION_RESET_SUCCESS
SESSION_GET_SUCCESS
-> complete sync
 - loop over session.users - if not exists in screen => SCREEN_USER_ADD_NOW
  - loop over screen.users - if not exists in session => SCREEN_USER_REMOVE_NOW

  USER_ADD_NOW => SCREEN_USER_ADD_NOW
  USER_REMOVE_NOW => SCREEN_USER_REMOVE_NOW
*/

// sub reducer
function session(state = {}, action) {
  const {type, p} = action
  if (!{TICK: 1, SAGA_GET_UPDATES: 1}[type]) {
    console.log('action', action)
  }
  switch (type) {
    case "SESSION_RESET_SUCCESS":
    case "SESSION_GET_SUCCESS":
      return p
    case 'USER_ADD_NOW': { // also triggers SCREEN_USER_ADD_NOW in saga
      const newUser = {...p, localStatus: true}
      const newUsers = {...state.users, [p.id]: newUser}
      return {...state, users: newUsers}
    }
    case 'USER_REMOVE_NOW': {
      const newUsers = Utils.omit(state.users, p.id)
      return {...state, users: newUsers}
    }
    default:
      return state
  }
}

// sub reducer
function screen(state = {
  showMenu: false,
  gridLock: true,
  showComments: true,
  currentForumId: undefined,
  currentItemId: undefined,
  users: {}
}, action) {
  const {type, p} = action
  switch (type) {
    case 'USERSTATUS_TOGGLE_NOW': {
      const toggle = state => state === 'active' ? 'connected' : 'active'
      const userId = p
      const user = state.users[userId]
      const newUser = {...user, localStatus: toggle(user.localStatus)}
      const newUsers = {...state.users, [userId]: newUser}
      return {...state, users: newUsers}
    }
    case 'SCREEN_USER_ADD_NOW': {
      const id = p
      const newUser = {id, type: 'screen_user', localStatus: 'active'}
      const newUsers = {...state.users, [id]: newUser}
      return {...state, users: newUsers}
    }
    case 'SCREEN_PATCH_NOW': {
      if (p.updateId && state.updateId) { // patching the updateID
        const oldId = state.updateId
        const newId = p.updateId
        const diff = newId-oldId
        if (diff !== 1) { // should always be a step increment
          console.warn('Problem updating from-to:', oldId, newId)
        }
      }
      return Object.assign({}, state, p)
    }
    case 'SCREEN_USER_REMOVE_NOW': {
      const newUsers = Utils.omit(state.users, p)
      return {...state, users: newUsers}
    }
    case a.SCREEN.SETCURRENTITEM: {
      return {...state, currentItemId: p.itemId}
    }
    case 'CURRENTFORUM_SET_NOW': {
      return {...state, currentForumId: p}
    }
    case 'MENU_SHOW': {
      return {...state, showMenu: true}
    }
    case 'MENU_HIDE': {
      return {...state, showMenu: false}
    }
    case a.SCREEN.TOGGLECOMMENTS: {
      return {...state, showComments: !state.showComments}
    }
    case a.SCREEN.TOGGLEGRIDLOCK: {
      return {...state, gridLock: !state.gridLock}
    }
    default:
      return state
  }
}

// sub reducer
function clock(state = {}, action) {
  const {type, p} = action
  switch (type) {
    case 'TICK':
      return {...state, now: p}
    default:
      return state
  }
}

// sub reducer
function forums(state = {}, action) {
  const me = 'forum'
  const {type, p} = action
  switch (type) {
    case 'FORUMS_GET_SUCCESS':
      return p
    case 'UPSERT_NOW':
      return upsert(state, p, me)
    case a.FORUM.ADDITEM: {
      console.warn('in deprecated a.FORUM.ADDITEM')
      const items = state.items.concat([p])
      return {...state, items}
    }
    default:
      return state
  }
}

// sub reducer
function items(state = {}, action) {
  const me = 'item'
  const {type, p} = action
  switch (type) {
    case 'ITEMS_UPDATE_NOW':
      return p
    case 'UPSERT_NOW':
      return upsert(state, p, me)
    default:
      return state
  }
}

// sub reducer
function motions(state = {}, action) {
  const me = 'motion'
  const {type, p} = action
  switch (type) {
    case 'MOTIONS_UPDATE_NOW':
      return p
    case 'UPSERT_NOW':
      return upsert(state, p, me)
    case a.ITEM.SET: {
      return state
    }
    default:
      return state
  }
}

// sub reducer
function comments(state = {}, action) {
  const me = 'comment'
  const {type, p} = action
  switch (type) {
    case 'COMMENTS_UPDATE_NOW':
      return p
    case 'UPSERT_NOW': {
      return upsert(state, p, me)
    }
    case 'COMMENT_UPSERT': { // deprecated!
      console.warn('in deprecated COMMENT_UPSERT')
      return {...state, [p.id]: p}
    }
    default:
      return state
  }
}

// sub reducer
function members(state = {}, action) {
  const me = 'member'
  const {type, p} = action
  switch (type) {
    case 'MEMBERS_UPDATE_NOW':
      return p
    case 'UPSERT_NOW': {
      return upsert(state, p, me)
    }
    case a.MEMBER.ADD: {
      const newMember = {...p, present: false}
      return {...state, [p.id]: newMember}
    }
    default:
      return state
  }
}

// sub reducer
function votes(state = {}, action) {
  const {type, p} = action
  switch (type) {
    case 'MEMBERS_UPDATE_NOW':
      return p
    case a.MEMBER.ADD: {
      const newMember = {...p, present: false}
      return {...state, [p.id]: newMember}
    }
    default:
      return state
  }
}

// sub reducer
function fullUsers(state = {}, action) {
  const {type, p} = action
  switch (type) {
//    case 'FULLUSERS_GET_REQUEST':
      // this is required so the table loads on changes as the
      // table checks the array length
      // return []
    case 'FULLUSERS_GET_SUCCESS':
      return p
    default:
      return state
  }
}

const reducer = combineReducers({
  session, // global session state
  screen, // characteristics of this window
  clock,
  forums,
  items,
  motions,
  comments,
  members,
  votes,
  fullUsers, // special item only for admins in UserView
})

const sagaMiddleware = createSagaMiddleware()

const hotLoadTestState = false
const startingState = hotLoadTestState
  ? testState()
  : {session: {users: {}}}

const store = createStore(reducer, startingState,
    applyMiddleware(sagaMiddleware)
  )

sagaMiddleware.run(rootSaga)

export default store
