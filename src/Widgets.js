import React, { Component } from 'react'
import PropTypes from 'prop-types'
import marked from 'marked'
import Grid from 'material-ui/Grid'
import Typography from 'material-ui/Typography'
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import Snackbar from 'material-ui/Snackbar'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import PersonIcon from 'material-ui-icons/Person';
import Icon from 'material-ui/Icon'
import Menu, { MenuItem } from 'material-ui/Menu'
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import Utils from './Utils.js'
import Pie from './PieView.js'
import Tooltip from 'material-ui/Tooltip'
import {pieColors, padding, evenPadding, editable, editing } from './Theme.js'
import {DateTime} from 'luxon'

class Form extends Component {
  constructor(props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
    const state = { }
    props.fields.forEach(field => {
      const {name, value} = field
      if (typeof value === 'undefined') {
        throw Error('missing value for ' + name)
      }
      state[name] = value
    })
    this.state = state
  }

  handleInputChange = (event) => {
    this.setState(Utils.onChangeEventToStateUpdate(event))
  }

  handleEnumChange = (name, value) => {
    const newState = {[name]: value}
    this.setState(newState)
  }

  // wrapper around props.onSubmit to call preventDefault
  onSubmit = (event) => {
    event.preventDefault()
    this.props.onSubmit(this.state)
  }

  render() {
    const {fields, buttons, onCancel, onSubmit} = this.props
    return <form onSubmit={this.onSubmit}>
    {fields.map(field => {
      // FIXME - use FieldEdit
      const {name, values, label,
        placeholder, type, disabled=false} = field
      const value = this.state[name]
      switch (type) {
        case 'text':
          return <TextField autoFocus fullWidth
            disabled={disabled}
            onChange={this.handleInputChange}
            key={name} value={value} label={label}
            name={name} placeholder={placeholder}
            type={type}/>
        case 'enum':
        case 'array':
          return <div>
      <Grid container alignItems="center" spacing={16} >
            <Grid item xs={6} md={3}>
              <Typography variant="caption" align="right">
                {label}
              </Typography>
            </Grid>
            <Grid item xs={6} md={3}>
              <SelectMenu
                options={values}
                value={value}
                type={type}
                disabled={disabled}
                onChange={newValue => this.handleEnumChange(name, newValue)} />
            </Grid>
          </Grid>
          </div>

        default:
          throw Error('bad type')}
    })}
    {buttons.map(button => {
      const {text} = button
      return <Button key={text} variant="flat" color="primary"
      onClick={() => onSubmit(this.state, text)}> {text} </Button>
    })}
    <Button variant="flat" color="secondary" size="small"
    onClick={onCancel}>
      Cancel
    </Button>
    </form>
  }
}
Form.propTypes = {
  fields: PropTypes.array.isRequired,
  buttons: PropTypes.array.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  disabled: PropTypes.bool
}
Form.defaultProps = {
  heading: 'Form',
}

class TextInput extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: props.value,
      original: props.value,
      mode: 'wait',
    }
    this.onKeyDown = this.onKeyDown.bind(this)
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    const prevProp = prevProps.value
    const newValue = this.props.value
    const oldValue = prevState.value
    const oldOriginal = prevState.original
    // neeed to detect a new value (newValue !== oldValue)
    // or a return of a save (prevProp === newValue)
    if ((newValue !== oldValue || prevProp === newValue)
        && newValue !== oldOriginal) {
      this.setState({
        value: newValue,
        original: newValue,
        mode: 'wait',
      })
    }
  }

  onKeyDown = event => {
    const {key} = event
    const {meta} = this.props
    switch (key) {
      case 'Escape':
        this.setState({
          value: this.state.original,
          mode: 'wait',
          })
        this.setState({value: this.state.original})
        event.preventDefault()
        break
      case 'Enter':
        if (meta.type !== 'markdown') {
          this.save()
        }
        break
      default:
    }
  }
  onChange = event => {
    const {value} = event.target
    this.setState({value})
  }
  onClick = event => {
    if (this.state.mode === 'wait') {
      this.setState({mode: 'edit'})
    }
  }
  onBlur = event => {
    this.save()
  }
  save = () => {
    const {value, original} = this.state
    const {type} = this.props.meta
    if (value !== original) {
      this.setState({mode: 'wait'})
      const valueOut = type === 'datetime'
        ? DateTime.fromISO(value)
        : value
      this.props.onChange(valueOut)
    }
    else {
      this.setState({mode: 'wait'})
    }
  }

  render() {
    const {value, mode} = this.state
    const {meta} = this.props
    const {name, label, placeholder, type} = meta
    if (type === 'markdown') {
      return mode === 'edit'
      ? <div style={padding}>
        <TextField
          InputProps={{style: editing}}
          multiline={true}
          rows={10}
          fullWidth autoFocus
          label={label}
          name={name}
          placeholder={placeholder}
          type={'text'}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onKeyDown={this.onKeyDown}
          value={value}
          />
        <Markdown value={value} />
      </div>
      : <div style={evenPadding}>
        <div style={editable}
          onClick={this.onClick}><Markdown value={value || "edit"} />
        </div>
        </div>
    }
    else {
      const typeMap = {
        datetime: "datetime-local",
        number: 'number',
        string: 'string',
      }[type] || 'string'
      return <div style={evenPadding}>
       { mode === 'edit'
      ?  <TextField
          InputProps={{style: editing}}
          fullWidth autoFocus
          label={label}
          name={name}
          placeholder={placeholder}
          type={typeMap}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onKeyDown={this.onKeyDown}
          value={type === 'datetime'
           ? DateTime.fromISO(value).toString().substr(0, 16)
           : value}
          />
      :
          <div style={editable} onClick={this.onClick}>
          {type === 'datetime'
           ? <NiceDate date={DateTime.fromISO(value)} />
           : value}
          </div>}
       </div>
    }
  }
}
TextInput.propTypes = {
  meta: PropTypes.object.isRequired,
  value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
}

// FIXME array only works with a single element (all I need for now)
// first element in options defines type of element (so avatar for member)
class SelectMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      anchorEl: null,
      value: props.value,
    }
  }
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }
  handleClose = () => {
    this.setState({ anchorEl: null })
  }
  handleChoice = (item) => {
    this.handleClose()
    const result = item === '--'
    ? (this.props.type === 'array' ? [] : null)
    : (this.props.type === 'array' ? [item] : item)
    this.props.onChange(result)
  }
  // TODO pull this ought to be its on class
  avatarChip = label => {
    const avatar = <Avatar><PersonIcon /></Avatar>
    return <Chip style={{margin: 2}}
      onClick={this.handleClick}
      avatar={avatar} label={label} />
  }

  render() {
    const {anchorEl} = this.state
    const {options, type, value, disabled = false} = this.props
    const optionValues = typeof options === 'function' ? options() : options
    let optionTypes = typeof optionValues[0]
    let displayValue = type === 'array' ? value[0] : value
    if (optionTypes !== 'string') {
      optionTypes = optionValues[0].type || 'unknown'
      displayValue = displayValue
        ? ((optionValues.find(option => option.id === displayValue))
          || {title: '#' + displayValue }).title
        : '--'
    }
    return (
      <div>
        <Button size="small" color="primary"
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
          disabled={disabled}
        >

        {optionTypes === 'member'
          ? this.avatarChip(displayValue)
          : displayValue
        }
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {optionValues.map(item => {
            return optionTypes === 'string'
            ?  <MenuItem onClick={() => this.handleChoice(item)} key={item} >
              {item}
            </MenuItem>
            : <MenuItem onClick={() => this.handleChoice(item.id)} key={item.id} >
              {item.title}
            </MenuItem>
          }
          )}
        </Menu>
      </div>
    )
  }
}
SelectMenu.propTypes = {
  options: PropTypes.oneOfType([PropTypes.func, PropTypes.array]).isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  type: PropTypes.string, // enum|array
}
SelectMenu.defaultProps = {
  disabled: false,
  type: 'enum',
}

class NiceDate extends Component {
  format = Object.assign({weekday: 'long'}, DateTime.DATETIME_MED)
  render() {
    const {date} = this.props
    const diff = date.diffNow().milliseconds
    const nice = Utils.humaniseMilliseconds(diff)
    const tip = nice
      + ' (' + date.toLocaleString(DateTime.DATETIME_FULL) + ')'
    return <Tooltip placement="bottom-end" title={tip}>
       <div>
         {date.toLocaleString(this.format)}
       </div>
     </Tooltip>
  }
}
NiceDate.propTypes = {
  date: PropTypes.object.isRequired,
}

class Markdown extends Component {
  render() {
    const markdown = {__html: marked(this.props.value)}
    return <div dangerouslySetInnerHTML={markdown} />
  }
}
Markdown.propTypes = {
  value: PropTypes.string.isRequired,
}

class Debug extends Component {
  details = () => {
    const {heading, val} = this.props
    console.log('debuging', heading, val)
    return <Typography component='pre'>
         {Utils.stringify(val)}
      </Typography>
  }
  render() {
    const {heading} = this.props
    return <ExpandLite heading={heading} callBack={this.details} />
  }
}
Debug.propTypes = {
  heading: PropTypes.string,
  val: PropTypes.any.isRequired,
}
Debug.defaultProps = {
  heading: 'Debug',
}


class LoveIndicatorIcon extends Component {
  render() {
    const {scale} = this.props
    return <div><Tooltip placement="bottom-end"
      title="Darker green indicates love. Darker grey indicates fear.">
      <Icon style={{color: scale}}>favorite</Icon>
      </Tooltip></div>
  }
}
LoveIndicatorIcon.propTypes = {
  scale: PropTypes.string.isRequired,
}

class Field extends Component {
  render() {
    const {heading, val} = this.props
    return <div><span><em>{heading}: </em>{val || "-"}</span></div>
  }
}
Field.propTypes = {
  heading: PropTypes.string.isRequired,
  val: PropTypes.string,
}

// Expanding panel that only calculates values when expanded
class ExpandLite extends Component {
  state = {
    expanded: false,
  }
  handleChange = () => {
    this.setState({expanded: !this.state.expanded})
  }
  details = () => {
    if (this.state.expanded) {
      console.log('rending', this.props.heading)
      return this.props.callBack()
    }
    else {
      return ''
    }
  }
  render() {
    return <ExpansionPanel
      expanded={this.state.expanded} onChange={this.handleChange}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>{this.props.heading}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        {this.details()}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  }
}
ExpandLite.propTypes = {
  heading: PropTypes.string.isRequired,
  callBack: PropTypes.func.isRequired,
}

class MoodPie extends React.Component {
  render() {
    const {mood = {}, total, size} = this.props
    const pie = [
        { value: mood[1] || 0,
          key: 1,
          color: pieColors.green,
        },
        { value: total - (mood[-1] || 0) - (mood[1] || 0),
          key: 2,
          color: pieColors.grey,
        },
        { value: mood[-1] || 0,
          key: 3,
          color: pieColors.red,
        },
      ]
    return <Tooltip placement="bottom-end"
        title="Green on the right side indicates support. Red to the left indicates opposition."
      >
    <div> <Pie data={pie} size={size} /> </div>
      </Tooltip>
  }
}
MoodPie.propTypes = {
  mood: PropTypes.object.isRequired,
  total: PropTypes.number.isRequired,
  size: PropTypes.string
}
MoodPie.defaultProps = {
  size: "2em"
}

// wip
class Snack extends React.Component {
  state = {
    open: false,
  }

  handleClick = () => {this.setState({open: true })}

  handleClose = (event, reason) => {
    if (reason !== 'clickaway') this.setState({ open: false })
  }

  message = () => {
    if (this.state.open) {
      const msg = "Note archived"
      return msg
    }
  }

  actions = () => {
    if (this.state.open) {
      const actions = [
        <Button key="undo"
          onClick={this.handleClose}
          color="inherit" >
          Close
        </Button>,
        <IconButton key="close" aria-label="Close" color="inherit"
          onClick={this.handleClose} >
          <CloseIcon />
        </IconButton>,
      ]
      return actions
    }
    return []
  }

  render() {
    return (
      <div>
        <Button onClick={this.handleClick}>Open simple snackbar</Button>
        <Snackbar
          anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
          SnackbarContentProps={{'aria-describedby': 'message-id'}}
          message={<span id="message-id">{this.message()}</span>}
          action={this.actions()}
        />
      </div>
    )
  }
}

Snack.propTypes = {
}

export {
  Form,
  TextInput,
  SelectMenu,
  Markdown,
  ExpandLite,
  Debug,
  LoveIndicatorIcon,
  Field,
  MoodPie,
  Snack,
}
