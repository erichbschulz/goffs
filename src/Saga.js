import { delay } from 'redux-saga'
import {
    call,
    put,
    takeEvery,
//    takeLatest,
    throttle,
    all,
    select,
    fork,
    } from 'redux-saga/effects'
import {
  selectors as s,
  actionTypes as at} from './Store'
import * as Server from './Server.js';
import Utils from './Utils.js'
import Config from './Config.js'

// const {pollInterval} = Config // no longer required?

let TaskIdStore = 0
const TaskId = () => TaskIdStore++

// currently reconnects on failed send
// ideally should respond to close event and restart there, with queing
const socket = (() => {
  const send = msg => {
    const message = JSON.stringify(msg)
    switch (ws.readyState) {
      case 1:
        return ws.send(message)
      case 3: // oops we've disconnected
        console.log('-s- restarting! status', status)
        return start().then(() => ws.send(message))
      default:
        console.log('status', status)
        console.log('ws', ws)
        throw Error('unexpected state: ' + ws.readyState)
    }
  }
  const subscribe = (forumId, updateId) => {
      console.log('subscribing to forumId', forumId)
      send({type: 'subscribe', forumId, updateId})
      Object.assign(state, {forumId, updateId})
  }
  let deferred // container for promise to hand off messages to saga
  var ws
  var status = 'startup'
  var state = {
    forumId: null,
    updateId: null
  }
  const start = () => new Promise((resolve, reject) => {
    status = 'starting'
    const Address = Config.ws
    console.log('Opening root socket to', Address)
    ws = new WebSocket(Address);
    status = 'waiting to open'
    ws.onopen = event => {
      status = 'open'
      ws.send(JSON.stringify({type: 'log', message: "starting up"}))
      if (state.forumId) {
        const {forumId, updateId} = state
        subscribe(forumId, updateId)
      }
      resolve()
    }
    ws.onmessage = event => {
      if (deferred) {
        console.log('messaged time', event.timeStamp)
        const packet = Utils.parseMessage(event.data)
        deferred.resolve(packet)
        deferred = null
      }
      else {
        console.error('ignoring messages', event)
      }
    }
    ws.onclose = event => { // listen to onmessage event
      status = 'closed'
      console.warn('socket closed!!', event)
    }
  })

  return {
    init: () => {
      return start()
    },
    nextMessage: () => {
      if (!deferred) {
        deferred = {}
        deferred.promise = new Promise(resolve => deferred.resolve = resolve)
      }
      return deferred.promise
    },
    send,
    log: message => send({type: 'log', message}),
    shout: message => send({type: 'shout', message}),
    subscribe,
  }
})()

/**
* Effects:
*   take: wait for specific actions in the store
*     yield take('CHECKOUT_REQUEST')
*   call: make asynchronous calls
*     const products = yield call(Api.fetch, '/products')
*   put: dispatch actions to the store
*     yield put(actions.requestPosts())  << actioncreator format
*     yield put({ type: 'PRODUCTS_RECEIVED', products }) << POJO format
*   select: get stuff from selectors
*     const getCart = state => state.cart
*     call with:
*     const cart = yield select(getCart)
*   fork:
*     function* checkout() { }
*     yield fork(checkout)
*
* Running multpiple ??
*  const actionReslt = yield all(data.actions.map(a => put(a)))
*
* In case the saga eats your errors use:
*   try { } catch (e) { console.log('e', e) }
*/

function* resetSession() {
  yield call(doAction, {
    at: 'SESSION_RESET',
    action: Server.deleteSession
  })
}

function* getSession() {
  yield call(doAction, {
    at: 'SESSION_GET',
    action: Server.getSession
  })
}

function* refreshFullUsers(action) {
  // since we just take latest the initial delay enables cancelling
  const {forumId} = action.p
  console.log('going to get forumId', forumId)
  yield put({type: 'FULLUSERS_GET', p: {forumId}})
}

function* getFullUsers(action) {
  const {p = {}} = action
  const {forumId} = p
  yield call(doAction, {
    at: 'FULLUSERS_GET',
    action: Server.getUsers,
    params: forumId
  })
}

// fixme consistent name!
function* init() {
  const now = Utils.now()
  socket.init()
  yield call(doAction, {
    at: 'SESSION_GET',
    action: Server.getSession
  })
  yield put({type: 'TICK', p: now})
  yield fork(watchSocket, socket)
}
function* watchSocket(socket) {
  let packet = yield call(socket.nextMessage)
  while (true) {
    const screen = yield select(s.screen)
    const {currentForumId, updateId} = screen
    console.log('>> got a packet at screen.updateId', updateId)
    const {type, message, updates} = packet
    switch (type) {
      case 'shout':
        console.log('>>shouted message', message)
        break
      case 'updates':
        yield applyUpdates(currentForumId, updates)
        break
      case 'log':
        console.log('server message', message)
        break
      case 'error':
        console.error('bad packet', packet)
        break
      default:
        console.error('unknown type', type)
    }
    packet = yield call(socket.nextMessage)
  }
}

function* getForums() {
  yield call(doAction, {
    at: 'FORUMS_GET',
    action: Server.getForum
  })
}

/*
 * Call the server to perform an upsert
 */
function* upsertForum(action) {
  yield call(doAction, {
    at: 'FORUM_UPSERT',
    action: Server.upsert,
    params: {record: action.p}
  })
}

/*
 * WIP
 * Insert and navigate to a forum
 */
function* createAndFocusOnForum(action) {
  const {p} = action
  const forumId = p.id
  yield put({type: 'FORUM_UPSERT', p})
  yield call(getForums)
  yield put({type: 'CURRENTFORUM_SET', p: forumId})
  const users = yield select(s.screenUsers)
  const userId = Object.keys(users)[0]
  const records = [{type: "update",  verb: "PATCH",
    record: {id: userId, type: "member", forumId, forumRoles: ['secretary']}}]
  yield call(Server.bulk, {records})
}

/*
 * Call the server to perform a patch
 * The record is a collection votes without an id at this point
 * The server will make this into a series of upserts and return
 */
function* patchVote(action) {
  yield call(doAction, {
    at: 'VOTE_PATCH',
    action: Server.vote,
    params: {record: action.p}
  })
}

/*
 * Handle a request from the server
 */
function* processUpdate(action) {
  const {p} = action
  const {verb, id, record} = p
  console.log('--------------------verb, id, record', verb, id, record)
  yield put({type: verb + '_NOW', p: record})
  yield put({type: 'SCREEN_PATCH_NOW', p: {updateId: id}})
}

// responds to global changes to the session obj and updates screen.users
function* syncScreenUsersToSession(action) {
  const {type} = action
  switch (type) {
    case 'USER_ADD_NOW':
      yield put({type: 'SCREEN_USER_ADD_NOW', p: action.p.id})
      yield call(doAction, {at: 'SESSION_GET', action: Server.getSession})
    break
    case 'USER_REMOVE_NOW':
      yield put({type: 'SCREEN_USER_REMOVE_NOW', p: action.p.id})
    break
    case 'SESSION_RESET_SUCCESS':
    case 'SESSION_GET_SUCCESS': {
      const sessionUsers = yield select(s.sessionUsers)
      const screenUsers = yield select(s.screenUsers)
      const changes = Utils.objKeyDiff(screenUsers, sessionUsers)
      for (const userId in changes) {
        const type = 'SCREEN_USER_' + changes[userId]+ '_NOW'
        yield put({type, p: userId})
      }
    }
    break
    default:
      console.error('whhhhhhhhhaaaaaaaaaaaaaaaaaaaaaaaaa fixme')
      throw Error('bad type', type)
  }
}

/*
 * Main handler for loading in a forum object into the saga
 */
function* setCurrentForum(action) {
  const {p: forumId} = action
  var updateId = 0
  if (forumId) {
    const task = {
      at: 'CURRENTFORUM_GET',
      action: Server.getForum,
      params: forumId
    }
    const result = yield call(doAction, task)
    if (result.success) {
      yield put({type: 'CURRENTFORUM_SET_NOW', p: forumId})
      const completeForum = result.data
      const props = ['item', 'motion', 'comment', 'member']
      for (const prop of props) {
        // capitalise and add 's' to property and dispatch update
        const type = prop.toUpperCase() + 'S_UPDATE_NOW'
        yield put({type, p: completeForum[prop]})
      }
      updateId = Math.trunc(completeForum.forum.updateId)
      const p = {
          participants: Utils.participantCount(completeForum.member),
          updateId
        }
      yield put({type: 'SCREEN_PATCH_NOW', p})
    }
  }
  else { //left forum, reset state back to baseline
    yield put({type: 'CURRENTFORUM_SET_NOW', p: forumId})
    yield put({type: 'SCREEN_PATCH_NOW',
        p: {participants: 0, updateId}})
    yield put({type: 'MEMBERS_UPDATE_NOW', p: {}})
  }
  socket.subscribe(forumId, updateId)
}

// wrap a task to emit _REQUEST, _SUCCESS & _FAILURE events
// params:
// - at = action type prefix
// - action = function to call
// - params = parameters to call
// returns the result of the call with data property
function* doAction (task) {
  const {at, action, params = null} = task
  const taskId = TaskId()
  yield put({type: at + '_REQUEST', taskId})
  const result = yield call(action, params)
  const {
    success,
    data={},
  } = result
  if (success) {
    yield put({type: at + '_SUCCESS', p: data, taskId})
  }
  else {
    yield put({type: at + '_FAILURE', taskId})
  }
  return result
}


function* forumChanges() {
  yield takeEvery(at.SCREEN.SETCURRENTFORUM, forumChange)
}

function* forumChange(action) {
  const {forumId} = action.p
  const res = yield Server.getForum({id: forumId})
  console.log('forum changes result', res)
}

let update_cycle = 0

function* applyUpdates(currentForumId, updates) {
  const n = updates.length
  console.log('>> n updates', n)
  for (let i = 0; i < n; ++i) {
    const update = updates[i]
    console.log('>> applying update', update)
    if (update.forumId === currentForumId) {
      yield put({type: 'SAGA_PROCESS_UPDATE', p: update})
    }
    else {
      console.warn('mismatched forums: ', update.forumId, currentForumId)
    }
  }
}

// redundant function
//function* getUpdates() {
//  const screen = yield select(s.screen)
//  const {currentForumId, updateId} = screen
//  const result = yield call(Server.getForumUpdates, currentForumId, updateId)
//  if (result.data) {
//    console.log('>> get updates result', result) // refetch the screen in case it has changed
//    const screen = yield select(s.screen)
//    const updates = result.data
//    yield applyUpdates(screen.currentForumId, updates)
//  }
//  yield delay(pollInterval * 1000)
//  yield put({ type: 'SAGA_GET_UPDATES' })
//}

// Time keeper
// Shout at 1 hour
function* tick() {
  yield delay(1000)
  const now = Utils.now()
  yield put({type: 'TICK', p: now})
  update_cycle = update_cycle + 1
  if (!((update_cycle + 5) % 3600)) {
    const users = yield select(s.sessionUsers)
    const userNames = Object.values(users).map(u => u.title).join(', ')
    const message = 'shout at '+ update_cycle + 'secs from ' + userNames
    socket.shout(message)
  }
}

export default function* rootSaga() {
  yield all([
    init(),
    takeEvery('FORUM_UPSERT', upsertForum),
    takeEvery('FORUM_CREATEANDFOCUS', createAndFocusOnForum),
    takeEvery('VOTE_PATCH', patchVote),
    takeEvery('SAGA_PROCESS_UPDATE', processUpdate),
    takeEvery('SESSION_RESET', resetSession),
    takeEvery(['CURRENTFORUM_GET_SUCCESS'], getSession),// reload permission on new forum
    takeEvery(['FORUMS_GET', 'SESSION_RESET_SUCCESS', 'SESSION_GET_SUCCESS'],
      getForums),
    takeEvery(['SESSION_RESET_SUCCESS', 'SESSION_GET_SUCCESS'],
      syncScreenUsersToSession),
    takeEvery('USER_ADD_NOW', syncScreenUsersToSession),
    throttle(2000, 'CURRENTFORUM_SET', setCurrentForum),
    throttle(4000, 'FULLUSERS_GET', getFullUsers),
    throttle(1000, 'FULLUSERS_REFRESH', refreshFullUsers),
    forumChanges(),
    //    getUpdates(), // redundant now sockets in use
    //    takeEvery('SAGA_GET_UPDATES', getUpdates),
    takeEvery('TICK', tick),
  ])
}


//function* yieldAndReturn() {
//  yield "Y";
//  return "R";
//  yield "unreachable";
//}
//var gen = yieldAndReturn()
//console.log(gen.next()); // { value: "Y", done: false }
//console.log(gen.next()); // { value: "R", done: true }
//console.log(gen.next()); // { value: undefined, done: true }
