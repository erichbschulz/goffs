import React, { Component } from 'react'
import {DateTime} from 'luxon'
import humanizeDuration from 'humanize-duration'
// import U from './Utils.js'
import PropTypes from 'prop-types'
//import Tooltip from 'material-ui/Tooltip'
import U from './Utils.js'
//import {Debug} from './Widgets.js'
//import {Link} from 'react-router-dom'
import Table, {
  TableBody,
  TableCell,
//  TableFooter,
  TableHead,
//  TablePagination,
  TableRow,
//  TableSortLabel,
} from 'material-ui/Table'

class MasterComment extends Component {

  url(comment, motions, comments) {
    // look up the parent Id - this is a bit more convuluted because itemId is
    // not stored in comment - maybe it ought to be?
    const getItemId = (obj, motions, comments) => {
      const {parentType, parentId} = obj
      switch (parentType) {
        case 'item':
          return parentId
        case 'motion':
          return motions[parentId].itemId
        case 'comment':
          return getItemId(comments[parentId], motions, comments)
        default:
          throw Error('unexpected type: ' + parentType)
      }
    }
    const itemId = getItemId(comment, motions, comments)
    const url = U.commentUrl(comment.forumId, itemId, comment.id)
    return url
  }

  from = timestamp => {
    const diff = DateTime.local()
      .diff(DateTime.fromISO(timestamp), 'milliseconds').milliseconds
    const duration = humanizeDuration(diff, {largest: 1, round: true})
    const result = diff > 0
      ? duration + ' ago'
      : 'in ' + duration
    return result
  }

  render() {
    const {motions, comments} = this.props
    const sortedComments =
      Object.values(comments).sort((a, b) => a.order > b.order)
    // cols comment | on | by
    return <div>
    <Table>
    <TableHead>
      <TableRow>
        <TableCell padding={'none'}>
            Comment
        </TableCell>
        <TableCell padding={'none'}>
            Status
        </TableCell>
        <TableCell padding={'none'}>
            On
        </TableCell>
        <TableCell padding={'none'}>
            By
        </TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {sortedComments.map(comment =>
        <TableRow key={comment.id}>
          <TableCell>
            <a href={this.url(comment, motions, comments)}>
              {comment.title}
            </a>
          </TableCell>
          <TableCell>
              {comment.status}
          </TableCell><TableCell>
              {this.from(comment.created)}
          </TableCell><TableCell>
              {this.from(comment.updated)}
          </TableCell>
        </TableRow>,
      )}
    </TableBody>
    </Table>
    </div>
//    <Debug heading='Debug items' val={items}/>
//    <Debug heading='Debug motions' val={motions}/>
//    <Debug heading='Debug comments' val={comments}/>
  }
}

MasterComment.propTypes = {
  items: PropTypes.object.isRequired,
  motions: PropTypes.object.isRequired,
  comments: PropTypes.object.isRequired,
}

export default MasterComment
