// This file runs in both the ES6-react world and the node world.
// The server repo version is the master.

module.exports = (config) => {
  const {
    uuid,
    DateTime, // 'luxon'
    timeZone, // 'UTC+10'
    mutate, // if true many methods will mutate the imput object
  } = config

  // export object
  var x = {}

  // deprectated
  x.actionTypes = {
  SESSION: {
    INIT: 'session/init',
    SETCURRENTITEM: 'session/setcurrentitem',
  },
  SCREEN: {
    SHOWMENU: 'screen/showmenu',
    HIDEMENU: 'screen/hidemenu',
    TOGGLECOMMENTS: 'screen/togglecomments',
    TOGGLEGRIDLOCK: 'screen/togglegridlock',
    SETCURRENTITEM: 'screen/setcurrentitem',
    SETCURRENTFORUM: 'screen/setcurrentforum',
  },
  MEMBER: {
    ADD: 'member/add',
  },
  ITEM: {
    SET: 'item/set',
  },
  COMMENT: {
    ADD: 'comment/add',
    SET: 'comment/set',
    ASSERT: 'comment/assert',
  },
  USER: {
    TOGGLELOCALSTATUS: 'user/togglelocalstatus',
  },
  FORUM: {
    ADDITEM: 'item/create',
    ASSERTITEM: 'item/assert', // asserts a position (helpful, unhelpful, agree, disagree)
    ASSERTMOTION: 'item/assert/motion', // asserts a position (helpful, unhelpful, agree, disagree)
  },
}

  // Tests password
  x.passwordTest = password => /^\w{4,14}$/.test(password)
  x.passwordRequirements = "Passwords must be between 4 and 14 characters long and contain only letters, numbers and the underscore character."
  //  console.log('x.passwordTest xxxx', x.passwordTest('xxxx'))
  //  console.log('x.passwordTest asd"sdf', x.passwordTest('asd"sdf'))
  //  console.log('x.passwordTest asd_12', x.passwordTest('asd_12'))
  //  console.log('x.passwordTest 54XX', x.passwordTest('54XX'))

  // promises //////////////////////////////////////////////////////////////////

  // return a promise that runs over a collection, executing the method on each
  // params:
  // - method - function to call
  // - callection - array of items to execute
  // - report - optional array to record outcomes
  x.runSequentially = (method, collection, report = []) => {
    const runMethod = (record, i) => {
      report[i] = 'trying'
      return method(record)
    }
    // set up this list of updates so they run in a sequence
    return collection.reduce((p, record, i) => p.then(() => {
      return runMethod(record, i)
        .then(result => {
          report[i] = 'woot'
        })
      }), Promise.resolve())
  }

  // message handling //////////////////////////////////////////////////////////

  // parse a string and expect {type: <string>, etc}
  // returns {type: 'error', error: <string>} on failure
  x.parseMessage = messageString => {
    let message
    try {
      message = JSON.parse(messageString)
    } catch(e) {
      message = {type: 'error', error: 'bad JSON: ' + e.message}
    }
    if (typeof message !== 'object') {
      message = {type: 'error', error: 'expecting JSON object'}
    }
    else if (!message.type) {
      message = {type: 'error', error: 'missing message type property'}
    }
    else if (typeof message.type !== 'string') {
      message = {type: 'error', error: 'message type is not a string'}
    }
    return message
  }

  // language //////////////////////////////////////////////////////////////////

  x.capitalise = s => s && s[0].toUpperCase() + s.slice(1)
  x.pluralise = (n, singular = '', multiple = 's') => n === 1 ? singular : multiple
  // cope with the weirdness of "1 loves, 2 love" - wtf English!
  x.inversePluralise = n => n === 1 ? 's' : ''
  x.toList = (array, sep = ', ', last = 'and') => {
    const len = array.length
      return array.reduce((s, a, i) => a + (i+1===len ? last : sep) + s)
  }
  x.camelToSentence = (s) => {
    const res = s
      .replace(/([A-Z]+)/g, " $1")
      .replace(/([A-Z][a-z])/g, " $1")
      .replace(/\s\s+/g, " ").trim()
    return res.charAt(0).toUpperCase() + res.slice(1);
  }

//  ['USBPort', 'helloThere ', 'HelloThere ', 'ILoveTheUSA', 'iLoveTheUSA']
//  .forEach(s => console.log(s, x.camelToSentence(s)))


  // date & time ///////////////////////////////////////////////////////////////

  // return a string
  x.now = () => DateTime.local().toString()
  x.local = () => DateTime.local().setZone(timeZone).toString()
  console.log('x.now', x.now())
  console.log('x.local', x.local())

  // param time integer
  // param daysForward integer
  // param time integer
  x.timeSoon = (time, daysForward = 0, rollForwardTime = 12) =>
    DateTime.local().setZone(timeZone)
      .plus({hours: Math.max(time, rollForwardTime)}).startOf('day')
      .plus({days: daysForward})
      .plus({hours: time}).toString()

  //  console.log('x.timeSoon(7,0,18)', x.timeSoon(7,0,18))
  //  console.log('x.timeSoon(7)', x.timeSoon(7))
  //  console.log('x.timeSoon(12,7)', x.timeSoon(12,7))

  // Calculate total time that item has been active
  // param history array of phase boundary changes [{mode, start}, ...]
  // param now data
  // returns integer milliseconds where history has been active
  x.elapsed = (history, now) => {
    let pointer = 0
    let result = 0
    const len = history ? history.length : 0
    function next(mode) { // scan the history forward to pick next change
      while (pointer < len) {
        const {status, start} = history[pointer]
        pointer++
        if ((mode === 'start') === (status  === 'active')) {
          return start // this is the start of the current phase (ie maybe end)
        }
      }
      return now // fallback to now if no matching boundary found
    }
    while (pointer < len) { // accumulate the periods
      const start = new Date(next('start'))
      const end = new Date(next('end'))
      result += end - start
    }
    return result
  }

//  const test = {
//    history: [
//    { status: 'active',   start: '2018-07-01T12:30:00.100Z' },
//    { status: 'complete', start: '2018-07-01T12:33:00.100Z' },
//    { status: 'active',   start: '2018-07-01T12:35:00.100Z' },
//    { status: 'complete', start: '2018-07-01T12:42:00.100Z' },
//    ],
//    now: '2018-07-01T12:52:00.100Z'}
//  console.log('x.elapsed() minutes', x.elapsed(test.history,test.now)/60000)

  x.remaining = (item, now) => {
    function remaining(allocatedMinutes, elapsedMilliseconds) {
      return Math.max(0, (allocatedMinutes * 60000) - elapsedMilliseconds)
    }
    switch (item.status) {
      case 'complete':
        return 0
      case 'active':
        // recalculate
        return remaining(item.timeAllocated, x.elapsed(item.history, now))
      default:
        return remaining(item.timeAllocated, item.timeElapsed)
    }
  }

  x.totalRemaining = (items, now) => {
    var result = 0
    for (const itemId in items) {
      const item = items[itemId]
      result += x.remaining(item, now)
    }
    return result
  }

  // collections ///////////////////////////////////////////////////////////////

  x.omit = (object, key) => {
    const {[key]: _, ...otherKeys} = object;
    return otherKeys;
  }

  // Look up the first key in a hash
  // (bit dodgy since properties not in fixed order)
  x.firstKey = obj => {
    if (obj) {
      for (var k in obj) return k
    }
  }

  // Look up the first property in a hash
  // (bit dodgy since properties not in fixed order)
  x.firstValue = obj => {
    if (obj) {
      return obj[x.firstKey(obj)]
    }
  }
//  console.log('x.firstValue(null)', x.firstValue(null))
//  console.log('x.firstValue(true)', x.firstValue(true))
//  console.log('x.firstValue({})', x.firstValue({}))
//  console.log('x.firstValue({x:1,y:2})', x.firstValue({x:1,y:2}))

  // check an object to see if it is empty or not
  // returns Boolean
  x.hasProperties = obj => !!x.firstKey(obj)
  //console.log('x.hasProperties - null', x.hasProperties(null))
  //console.log('x.hasProperties - true', x.hasProperties(true))
  //console.log('x.hasProperties - {}', x.hasProperties({}))
  //console.log('x.hasProperties - {x:false}', x.hasProperties({x:false}))
  //console.log('x.hasProperties-{x:undefined}', x.hasProperties({x:undefined}))
  //console.log('x.hasProperties - {x:null}', x.hasProperties({x:null}))

  // simple diff on the keys of two objects
  // creates a new array of 'add' and 'remove' values
  //    const testA = {fish: 10, head: 5, tail: 7}
  //    const testB = {arm: 10, leg: 5, tail: 5}
  //    console.log('a,b', x.objKeyDiff(testA, testB))
  //    console.log('b,a', x.objKeyDiff(testB, testA))
  //  // a,b { fish: 'REMOVE', head: 'REMOVE', arm: 'ADD', leg: 'ADD' }
  //  // b,a { arm: 'REMOVE', leg: 'REMOVE', fish: 'ADD', head: 'ADD' }
  x.objKeyDiff = (start, end, params = {add: 'ADD', remove: 'REMOVE'}) => {
    const diff = (a, b, val) => {
      for (const prop in a) {
        if (!b.hasOwnProperty(prop)) res[prop] = val
      }
    }
    let res = {}
    diff(start, end, params.remove)
    diff(end, start, params.add)
    return res
  }

  // convert a string into a 2D array based on fieldSep and recordSep
  x.stringToArray = (string, params = {}) => {
    const {fieldSep, recordSep} = params
      const recs = string.trim().split(recordSep).map(row => row.split(fieldSep))
      return recs
  }

  x.unique = array => [...new Set(array)]

  // superficial comparision between two arrays
  x.arraysEqual = (a, b) => a.length === b.length && a.every((val, i) => b[i]===val)

  x.hashesEqualOneWay = (a, b) => {
    for (const p in a) {
      if (a[p] !== b[p]) { return false }
    }
    return true
  }

  // convert an array to object has on id property of elements
  // can run array through optional `val` transformation
  x.arrayToHash = (array, params = {}) => {
    const {
      id = 'id',
      throwOnDuplicate = false,
      val = x => x, // transformation function
    } = params
    const result = {}
    let key
    for (var i=0, n=array.length; i<n; i++) {
      key = array[i][id]
      if (throwOnDuplicate && result.hasOwnProperty(key)) {
        throw Error('duplicate property key field' + id + ':' + key)
      }
      result[key] = val(array[i])
    }
    return result
  }
  //  const items = [{id:3,a:3},{id:5,a:5},{id:1,a:7}]
  //  console.log('x.arrayToHash(items)', x.arrayToHash(items))
  //  console.log('x.arrayToHash(items, {val: x=>x.a})',
  //      x.arrayToHash(items, {val: x=>x.a}))

  // scans a hash for the first/previous/next/last item
  // in hash obj, using the property
  x.findRelativeInHash = (currentId, obj, mode, property) => {
    const co = (obj[currentId] || {})[property]
    var winner = co ? currentId : x.firstKey(obj)
    for (let key in obj) {
      const wo = obj[winner][property]
      const to = obj[key][property] // test item
      switch (mode) {
        case 'first':
          if (to < wo) winner = key
          break
        case 'previous':
          if ((to > wo || wo === co) && to < co) winner = key
          break
        case 'next':
          if ((to < wo || wo === co) && to > co) winner = key
          break
        case 'last':
          if (to > wo) winner = key
          break
        default:
          throw Error('bad mode: ' + mode)
      }
    }
    return winner
  }

  /////////////////////////////////////////////////////////////////////////////
  // pemissions ///////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  x.roles = {
    superadmin: {
      type: 'global',
      description: 'can do anything',
      permissions: [
      'do anything',
      ],
      includes: ['admin']
    },
    admin: {
      type: 'global',
      description: 'general admin',
      permissions: [
      'grant admin', 'grant convenor',
      ],
      includes: ['convenor']
    },
    convenor: {
      type: 'global',
      description: 'creates meetings',
      permissions: [
      'create forum',
      'update forum', // eg title, start, end
      'grant user',
      'grant secretary to own fora',
      'bulk import',
      'administer user',
      ],
      includes: ['user']
    },
    user: {
      type: 'global',
      description: 'basic system use',
      permissions: [
      'use system',
      ],
      includes: ['anon']
    },
    anon: {
      type: 'global',
      description: 'anonymous access',
      permissions: [
      'see help',
      'login',
      ],
    },
    observer: {
      type: 'local',
      description: 'can see the forum',
      permissions: [
      'see item',
      'see motion',
      'see comment',
      ],
    },
    guest: {
      type: 'local',
      description: 'can see make comments',
      permissions: [
      'create comment',
      ],
      includes: ['observer']
    },
    member: {
      type: 'local',
      description: '',
      permissions: [
      'propose item',
      'propose motion',
      'rate forum thing',
      ],
      includes: ['guest']
    },
    secretary: {
      type: 'local',
      description: '',
      permissions: [
      'create item',
      'update item',
      'update motion',
      'update motion mover to any member',
      'hide comment',
      'reorder item',
      'reorder motion',
      'grant secretary',
      'grant moderator',
      'grant member',
      'grant observer',
      'grant guest',
      ],
      includes: ['member']
    },
    moderator: {
      type: 'local',
      description: '',
      permissions: [
      'hide comment',
      'hide item',
      'hide motion',
      'record member presence',
      ],
    },
  }

  x.allRoles = Object.keys(x.roles)

  const getAllPermissions = (role_name, role_type, depth=3) => {
    //const pre = "----  ".repeat(depth)
    if (depth > 10) {
      throw Error("recursive role inclusions affecting " + role_name)
    }
    const role = x.roles[role_name]
    const {permissions, allPermissions, includes = [], type} = role
    if (role_type !== type) {
      throw Error("missmatched type affecting " + role_name)
    }
    if (allPermissions) {
      return allPermissions
    }
    else {
      let all = [].concat(permissions)
      for (let i=0, n=includes.length; i<n; i++) {
        all = all.concat(getAllPermissions(includes[i], type, depth+1))
      }
      all = x.unique(all).sort()
      role.allPermissions = all
      return all
    }
  }

  // gather allPermissions, and make the index:
  x.roleIndex = x.allRoles.reduce((index, role_name) => {
    // this step is essential and manipulates roles to add the allPermissions
    const p = getAllPermissions(role_name, x.roles[role_name].type)
    p.forEach(permission => {
      if (!index[permission]) index[permission] = []
      index[permission].push(role_name)
    })
    return index
  }, {})
  // check we have a grant for each role
  x.allRoles.forEach(role_name => {
    const role = "grant " + role_name
    if (role_name !== 'superadmin'
      && role_name !== 'anon' && !x.roleIndex[role]) {
      console.error('>>>> no permission exists to grant', role_name)
    }
  })
  //  console.log('x.roleIndex', x.roleIndex)
  x.has = (permissions, permission) => {
    if (!x.roleIndex[permission]) {
      throw Error('unknown permission ' + permission)
    }
    return Boolean(permissions[permission])
  }

  x.globalRoles = x.allRoles.filter(role => x.roles[role].type === 'global')
  x.localRoles = x.allRoles.filter(role => x.roles[role].type === 'local')

  const meta = {
    user: [{
        title: 'roles',
        type: 'set', // comma delim
        defaultValue: ['user'],
        values: x.globalRoles,
        required: true,
      },
      {
        title: 'password',
        type: 'string',
        required: false,
      },
      {
        title: 'status',
        type: 'enum',
        values: ['active', 'disabled'],
        defaultValue: 'active',
        required: false,
      },
      {
        title: 'email',
        type: 'email',
        required: false,
      },
    ],
    forum: [
      {
        title: 'description',
        type: 'string',
        required: false,
      },
      {
        title: 'start',
        type: 'datetime',
        required: true,
        defaultValue: obj => x.timeSoon(18,1,12),
      },
      {
        title: 'end',
        type: 'datetime',
        required: true,
        defaultValue: obj => obj.start
          ? DateTime.fromISO(obj.start).plus({hours: 2}).toString()
          : x.timeSoon(20,1,12),
      },
      {
        title: 'status',
        type: 'enum',
        values: ['active', 'cancelled'],
        defaultValue: 'active',
        required: false,
      },
    ],
    item: [
      {
        title: 'forumId',
        type: 'forumId',
        required: true,
      },
      {
        title: 'description',
        type: 'string',
        required: false,
      },
      {
        title: 'order',
        type: 'number',
        required: true,
        defaultValue: 100,
      },
      {
        title: 'timeAllocated',
        type: 'number',
        required: true,
        defaultValue: 10,
      },
      {
        title: 'timeElapsed',
        type: 'number',
        required: true,
        defaultValue: 0,
      },
      {
        title: 'status',
        type: 'enum',
        values: ['pending', 'active', 'complete'],
        defaultValue: 'pending',
        required: true,
      },
      {
        title: 'motion',
        type: 'array',
        defaultValue: [],
        required: true,
      },
    ],
    motion: [
      {
        title: 'description',
        type: 'markdown',
        required: false,
      },
      {
        title: 'movedBy',
        type: 'array',
        defaultValue: [],
        required: true,
      },
      {
        title: 'secondedBy',
        type: 'array',
        defaultValue: [],
        required: true,
      },
      {
        title: 'status',
        type: 'enum',
        values: ['pending', 'passed', 'defeated', 'withdrawn', 'tabled'],
        defaultValue: 'pending',
        required: true,
      },
      {
        title: 'forumId',
        type: 'forumId',
        required: true,
      },
      {
        title: 'itemId',
        type: 'itemId',
        required: true,
      },
    ],
    comment: [
      {
        title: 'targetId',
        type: 'id',
        required: true,
      },
      {
        title: 'targetType',
        type: 'enum',
        values: ['item', 'motion', 'comment'],
        required: true,
      },
      {
        title: 'status',
        type: 'enum',
        values: ['active', 'hidden'],
        defaultValue: 'active',
        required: true,
      },
      {
        title: 'forumId',
        type: 'forumId',
        required: true,
      },
    ],
    vote: [
      {
        title: 'targetId',
        type: 'id',
        required: true,
      },
      {
        title: 'targetType',
        type: 'enum',
        values: ['item', 'motion', 'comment'],
        required: true,
      },
      {
        title: 'action',
        type: 'hash',
        defaultValue: {},
        required: true,
      },
      {
        title: 'forumId',
        type: 'forumId',
        required: true,
      },
    ],
    member: [
      {
        title: 'forumRoles',
        type: 'set', // comma delim
        defaultValue: ['member'],
        values: x.localRoles,
        required: true,
      },
      {
        title: 'proxyTo',
        type: 'set',
        required: false,
        defaultValue: [],
      },
      {
        title: 'forumStatus',
        type: 'enum',
        values: ['present', 'absent', 'apology'],
        defaultValue: 'absent',
        required: true,
      },
      {
        title: 'forumId',
        type: 'forumId',
        required: true,
      },
      {
        title: 'votingFor',
        type: 'set',
        required: false,
        defaultValue: [],
      },
    ],
  }

  // clean up a string (lowercase and strip trailing s)
  // to match a type
  x.cleanType = (string) => {
    return string.trim().toLowerCase().replace(/s$/, "")
  }

  // look up the metadata
  x.meta = (type) => {
    const coreFields =  meta[type]
    if (!coreFields) throw Error ('bad type: ' + type)
    const pre = [
      { title: 'id',
        type: 'id',
        required: true,
      }]
    // these two types do not have a title
    if (!['member', 'vote'].includes(type)) {
      pre.push({
        title: 'title',
        type: 'string',
        required: true,
      })
    }
    const post = [
      { title: 'type',
        type: 'enum',
        values: [type],
        defaultValue: type,
        required: true,
      }]
    for (let i=0, n=coreFields.length; i<n; i++) {
      const f = coreFields[i]
      if (!f.label) {
        f.label = x.camelToSentence(f.title)
      }
    }
    return coreFields
      ? pre.concat(coreFields, post)
      : false
  }

  // look up the metadata
  x.fieldImportOrder = (type) => {
    const meta = x.meta(type)
    return meta.map(field => field.title)
  }

  // create an indexed copy of the meta field collection
  x.metaByField = (() => {
    const result = {}
    // not meta is raw `meta`, `x.meta()` is final version
    for (let key in meta) {
      result[key] = x.arrayToHash(x.meta(key), {
        throwOnDuplicate: true,
        id: 'title'})
    }
    return result
  })()
  // console.log('x.metaByField', metaByField.user.title)
  x.trim = (string) => string.trim()

  x.fieldImportParse = (value, type, field) => {
    // get the field meta
    const fmeta = x.metaByField[type][field]
    switch (fmeta.type) {
      case 'set': {
        return value.trim().split(',').map(x.trim)
      }
      default:
        return value
    }
  }
  // console.log('x.fieldImportParse', x.fieldImportParse(' admin, boss ', 'user', 'roles'))

  // look up the metadata
  x.defaultValues = (type) => {
    const meta = x.meta(type)
    const result = {}
    meta.forEach(fieldMeta => {
      const {defaultValue, title} = fieldMeta
      switch (typeof defaultValue) {
        case 'undefined':
         // skip value if none defined
        break
        case 'function':
          result[title] = defaultValue(result)
        break
        default:
          result[title] = defaultValue
      }
    })
    console.log('---defaults', type,  result)
    return result
  }

  // validate a single object
  // returns false if none, or an array of strings if there are any
  x.validationErrors = (obj) => {
    if (!obj.type) return ['missing type']
    const meta = x.meta(obj.type)
    if (!meta) return ['unknown type']
    const errors = []
    meta.forEach(field => {
      const {title, type, values, required} = field
      if (obj.hasOwnProperty(title)) {
        const value = obj[title]
        switch (type) {
          case 'set':
            if (Array.isArray(value)) {
              if (values) {
                for (let i=0, n=value.length; i<n; i++) {
                  if (!values.includes(value[i])) {
                    errors.push('bad value "' + value[i]
                      + '" for field: ' + title + '. ' +
                    'Expected one of: ' + values.join('|'))
                  }
                }
              }
            }
            else {
              errors.push('non-array value for field: ' + title)
            }
            break
          case 'enum':
            if (!values.includes(value)) {
              errors.push('bad value for field: ' + title + '. ' +
                'Expected one of: ' + values.join('|'))
            }
            break
          case 'number':
            if (Number.isNaN(value)) {
              errors.push('non-numeric value for field: ' + title)
            }
            break
          case 'datetime': {
              const parsed = DateTime.fromISO(value)
              if (!parsed.isValid) {
                errors.push(parsed.invalidReason + ' for field: ' + title)
              }
            }
            break
          default:
        }
      }
      else if (required) {
        errors.push('missing field: ' + title)
      }
    })
    // check each field in obj has a meta value
    for (const fieldName in obj) {
      const {title} = meta
      if (meta.find(meta => title === fieldName) > -1) {
        errors.push('unknown field' + title)
      }
    }
    return errors
  }


  // take the basic properties of an object and decorate with defaults etc
  x.buildObj = (obj, context={}) => {
    const selectedContext = {}
    const {type} = obj
    const {forumId, itemId, lastType, lastId} = context
    if (!['user', 'forum'].includes(type)) {
      selectedContext.forumId = forumId
    }
    switch (type) {
      case 'item':
        obj = x.summarise(obj) // add tally properties
        break
      case 'motion':
        obj = x.summarise(obj) // add tally properties
        selectedContext.itemId = itemId
        break
      case 'comment':
        obj = x.summarise(obj) // add tally properties
        selectedContext.parentType = lastType
        selectedContext.parentId = lastId
        break
      default:
    }
    return Object.assign(
      selectedContext,
      x.defaultValues(type),
      x.addId(type, obj))
  }

  // miscelaneous utils
  // decorate an object with a type and UUID based on its title
  x.addId = (type, obj) => {
    if (!obj.id) obj.id = uuid(obj.title)
    obj.type = type
    return obj
  }

  // create a union of elements from selected arrays in a hash
  // eg agregating all the permissions for a set of roles
  x.branchUnion = (hash, properties) => {
    let union = []
    for (let i=0, n=properties.length; i<n; i++) {
      union = union.concat(hash[properties[i]])
    }
    return x.unique(union).sort()
  }

  //  const hash = {alpha: ['a', 'b'], beta: ['b', 'c'], delta: ['e', 'a']}
  //  console.log('x.branchUnion(hash,["alpha","beta"])',
  //      x.branchUnion(hash,["alpha","beta"]))

  // create a union of elements from an array of objects with an array property
  x.propertyUnion = (array, property) => {
    let union = []
    for (let i=0, n=array.length; i<n; i++) {
      union = union.concat((array[i] || {})[property])
    }
    return x.unique(union).sort()
  }
  // const array=[{pets: ['enzo', 'ruby']}, {pets: ['sam', 'enzo']}]
  // console.log('x.propertyUnion(array,"pets")', x.propertyUnion(array,"pets"))

  // decants an array of objects into a hash
  // param array - source collection (array) of objects
  // param hash - target collection (object) of objects
  x.decant = (array, hash) => {
    array.forEach(obj => {
      hash[obj.id] = obj
    })
    return hash
  }

  // indexes an array of strings into a hash
  // param array - source collection (array) of string
  x.index = (array, hash = {}) => {
    array.forEach(string => {
      hash[string] = true
    })
    return hash
  }

  // adds a child collection after decorating children with 'parentId' property
  // mutates both obj and children
  x.addChildren = (obj, childrenAttributeName, children) => {
    const parentObj = {parentId: obj.id}
    obj[childrenAttributeName] = children.map(
        child => Object.assign(child, parentObj))
      return obj
  }

  // join elemenets of array b into elements of array a
  x.joinById = (a, b) => {
    return a.map(ea =>
        Object.assign(ea, b.find(eb => {return eb.id === ea.id}))
        )
  }

  // test if an ID is ok
  x.validId = id => /^[\w\d-.:]*$/.test(id)
  //console.log('x.validId xx', x.validId('xx'))
  //console.log('x.validId xx-ab-1243', x.validId('xx-ab-1243'))
  //console.log('x.validId 234,235', x.validId('234,235'))
  //console.log('x.validId ../private', x.validId('../private'))
  //console.log('x.validId 1234', x.validId(1234))

  // let values = [2, 56, 3, 41, 0, 4, 100, 23]
  // median = 13,5
  x.median = values => {
    values.sort((a, b) => a - b)
      const i1 = Math.floor((values.length - 1) / 2)
      const i2 = Math.ceil((values.length - 1) / 2)
      return (values[i1] + values[i2]) / 2
  }

  // removes a single item at a given index
  // array.indexOf(id)
  x.removeArrayItem = (array, index) => {
    if (index === -1) { // add item
      return array
    } else if (index === 0) {
      return array.slice(1)
    } else if (index === array.length - 1) {
      return array.slice(0, -1)
    } else if (index > 0) {
      return array.slice(0, index).concat(array.slice(index + 1))
    }
  }
  //console.log('x.removeArrayItem([3,4,6,7,8], 0)', x.removeArrayItem([3,4,6,7,8], 0))
  //console.log('x.removeArrayItem([3,4,6,7,8], 1)', x.removeArrayItem([3,4,6,7,8], 1))
  //console.log('x.removeArrayItem([3,4,6,7,8], 2)', x.removeArrayItem([3,4,6,7,8], 2))
  //console.log('x.removeArrayItem([3,4,6,7,8], 4)', x.removeArrayItem([3,4,6,7,8], 4))
  //console.log('x.removeArrayItem([3,4,6,7,8], 7)', x.removeArrayItem([3,4,6,7,8], 7))

  x.removeArrayItemByValue = (array = [], value) => {
    const index = array.indexOf(value)
    return x.removeArrayItem(array, index)
  }
  //  const z = ["a","b","c"]
  //  console.log('z', z)
  //  console.log('removeArrayItemByValue(z, "a")', x.removeArrayItemByValue(z, "a"))
  //  console.log('removeArrayItemByValue(z, "c")', x.removeArrayItemByValue(z, "c"))
  //  console.log('removeArrayItemByValue(z, "d")', x.removeArrayItemByValue(z, "d"))



  // decorate each obj with a parent (eg motionId) property
  // add a list of the obj keys
  // todo - make the id optional
  x.add = (parentObj, childObj, childStore) => {
    const parentType = parentObj.type
      const childType = childObj.type
      if (!(parentType && childType)) {
        throw Error('bad ojbects')
      }
    // grab or create the child ID
    x.addId(childType, childObj)
      // link child to parent
      const childId = childObj.id
      childObj[parentType + 'Id'] = parentObj.id
      // link parent to child by array of IDs
      if (!parentObj[childType]) parentObj[childType] = []
        parentObj[childType].push(childId)
          // add child to global store
          childStore[childId] = childObj
          return childId
  }

// find the union of roles held by a collection of users
// returns an object with truthy for values
x.getUsersRoles = (users) => {
  const res = Object.values(users).reduce((allRoles, user) => {
    const {roles = []} = user
    roles.forEach(role => allRoles[role] = 1)
    return allRoles
  }, {})
  return res
}

// todo split this into two - one just making tallys
// take obj[attributeCollection] and sums the values
// clones the new object, adding and attributeCollection + mood property
x.summarise = (obj) => {
  const summarisable = {item: true, motion: true, comment: true}
  const {type} = obj
  if (!summarisable[type]) {
    return obj
  }
  // iniatialise accumlators
  var summary = {}
  var tally = {}
  const meta = x.getMeta(type)
  const attributeCollection = 'mood'
  for (const prop in meta) {
    summary[prop] = meta[prop].mode === 'median' ? [] : 0
    tally[prop] = {}
  }
  // loop over votes and accumulate vaules per mode
  const votes = obj[attributeCollection]
  for (const index in votes) {
    for (const prop in meta) {
      const vote = votes[index][prop]
      tally[prop][vote] = (tally[prop][vote] || 0) + 1
      switch (meta[prop].mode) {
        case 'sum':
          summary[prop] += (vote || 0)
          break
        case 'median':
          if (typeof vote !== 'undefined') summary[prop].push(vote)
          break
        default:
          throw new Error('bad mode')
      }
    }
  }
  for (const prop in meta) {
    if (meta[prop].mode === 'median') {
      // calculate median or use default value
      let value = x.median(summary[prop])
      summary[prop] = isNaN(value) ? meta[prop].defaultValue(obj) : value
    }
  }
  const res = {
    [attributeCollection + 'Summary']: summary,
    [attributeCollection + 'Tally']: tally
  }
  const newObj = mutate
    ? Object.assign(obj, res )
    : Object.assign({}, obj, res )
  return newObj
}

  // mutates a member collection to add add votingFor and presentProxy properties
  x.allocateProxies = memberObj => {
    const members = Object.values(memberObj)
      // add votingFor fields
      members.map(member => Object.assign(member, {votingFor : []}))
      members.map(member => {
        let presentProxy = false
        // am I voting on behalf of myself?
        if (member.present) {
          presentProxy = member
        }
        else { // are any of my proxy's present
          if (member.proxyTo) {
            let latest = false // holds latest member object tested
        // find first available proxy
        const found = member.proxyTo.find(proxyId => {
          latest = memberObj[proxyId]
          return latest && latest.present
        })
      if (found) presentProxy = latest
          }
        }
      if (presentProxy) {
        presentProxy.votingFor.push(member.id)
        member.activeProxy = presentProxy.id
      }
      else {
        member.activeProxy = null
      }
      return member
      })
    return memberObj
  }

  x.getMeta = (type) => {
    switch (type) {
      case 'item': // agenda items
        return {
          love: {mode: 'sum'},
          timeAllowance: {
            mode: 'median',
            defaultValue: (obj) => {
              return isNaN(obj.timeAllocated) ? 10 : obj.timeAllocated}
          }}
      case 'motion':
        return {
          support: {mode: 'sum'},
          love: {mode: 'sum'},
        }
      case 'comment':
        return {
          love: {mode: 'sum'},
        }
      default:
        throw Error('bad type: ' + type)
    }
  }

  x.sortItems = (items) => {
    const itemSort = (a, b) => {
      if (a.order && b.order) return a - b
      return a.title > b.title
    }
    let counter = 1
    items.sort(itemSort).map(item => Object.assign(item, {order: counter++}))
    return items
  }

  const findOther = (items, property, current, direction) => {
    const compare = (a,b) => direction === 'later' ? a > b : b > a
    return Object.values(items)
      .filter(item => compare(item[property], current))
      .sort((a,b) => compare(a[property], b[property]))[0]
  }

  //  items = [{a:3},{a:5},{a:7},{a:2},{a:4},{a:9},{a:3},{a:6}]
  //console.log('findOther(items,"a",6,"earlier")',
  //   findOther(items,"a",6,"earlier"))
  //console.log('findOther(items,"a",6,"later")',
  //   findOther(items,"a",6,"later"))
  //console.log('findOther(items,"a",9,"later")',
  //   findOther(items,"a",9,"later"))

  // produce a series of patch records:
  // returns an array or null if
  x.itemReorderPatches = (direction, item, items) => {
    const {id, type, forumId, order} = item
    // get the other item
    const other = findOther(items, 'order', order, direction)
    if (other) {
      const patches = [ // swap the order properties:
        {type: 'update', verb: 'PATCH',
          record: {id, type, forumId, order: other.order}},
        {type: 'update', verb: 'PATCH',
          record: {
            id: other.id,
            type: other.type,
            forumId: other.forumId,
            order}}
      ]
      // do live update of original objects
//      item.order = other.order
//      other.order = order
      return patches
    }
  }

  x.testData = () => {

    // create an object from an array keyed on an id column, with type property
    // mutates original array
    //   adds id property if none present
    //   adds type property if none present
    const makeHashWithId = (type, array) => {
      var index = {}
      array.forEach(obj => {
        obj.type = type
  //      x.addId(type, obj)
        index[obj.id] = x.buildObj(obj)
      })
      return index
    }

    const title = "Friends AGM"
    const forum = x.addId('forum', {
      title,
      start: x.timeSoon(18,1,12),
      end: x.timeSoon(20,1,12),
    })
    const forums = makeHashWithId('forum',[
        forum,
        {
          title: 'Spicy working group',
          start: x.timeSoon(18,7,12),
          end: x.timeSoon(20,7,12),
        },
        ])
    let items ={},
      motions ={},
      comments ={}

  // present is currently a true member of this forum
  const people = x.allocateProxies(makeHashWithId('member', [
    {id: '2342', title: 'Abby',
    forumStatus: 'present', proxyTo: ['122445', '240851'],
        roles: ['admin'], forumRoles: ['secretary']},
    {id: '62345', title: 'Bill',
    forumStatus: 'present', proxyTo: ['2342', '122445', '240851']},
    {id: '97652', title: 'Charlie',
    forumStatus: 'present', proxyTo: ['2342', '122445', '240851']},
    {id: '122445', title: 'Doug',
    forumStatus: 'absent', proxyTo: ['2342', '240851']},
    {id: '240853', title: 'Gina',
    forumStatus: 'present', proxyTo: ['2342', '122445', '240851']},
    {id: '240854', title: 'Heather',
    forumStatus: 'present', proxyTo: ['240851']},
    {id: '240851', title: 'Fill',
    forumStatus: 'present',},
    {id: '244085', title: 'Enzo',
    forumStatus: 'absent', proxyTo: ['2342', '122445', '240851']},
  ]))

  const peopleToUsers = (people) => {
    return Object.values(people).map(person => {
      const {id, title, status = 'active', roles = ['user'] } = person
      return {id, title, status, roles}
    })
  }
  const peopleToMembers = (people) => {
    return Object.values(people).map(person => {
      const {
        id,
        title,
        forumStatus = 'present',
        forumRoles = ['member'],
        proxyTo = [],
        votingFor = [],
      } = person
      return {id, title, forumStatus, forumRoles, proxyTo, votingFor}
    })
  }

  const users = peopleToUsers(people)
  const members = peopleToMembers(people)

      // makeHashWithId('user',[
      //        {id: '2342', localStatus: 'active', },
      //        {id: '97652', localStatus: 'active',},
      //        {id: '122445', localStatus: 'connected', localOwner: true },
      //        ])

      const treasurersReport =
      x.addId('item', {
        title: 'Treasurer\'s report',
      details: 'Update on financial position after recent election',
      timeAllocated: 10,
      timeElapsed: 4,
      mood: {
        '240851': {support: 1, love: 1, timeAllowance: 10},
      '240853': {support: 1, love: 1, timeAllowance: 10},
      '240854': {support: 1, love: 1, timeAllowance: 50},
      }})

    // returns ID of motion
    const addMotion = (item, motion) => {
      motion.type = 'motion'
      return x.add(
        item,
        x.buildObj(motion),
        motions)
    }

    // add a comment to and object and adds in various properties
    // returns the ID
    const addComment = (parentObj, comment) => {
      comment.type = 'comment'
      comment.parentId = parentObj.id
      comment.parentType = parentObj.type
      return x.add(parentObj, comment, comments)
    }

    const MotionToAccepttreasurersReport = x.addId('motion', {
      title: 'Accept report',
      details: 'That the treasurer\'s report be accepted',
      movedBy: ['62345'], secondedBy: ['244085']})
    addMotion(treasurersReport, MotionToAccepttreasurersReport)

    addComment(treasurersReport, {
      title: 'This is my best work ever. I hope you like it',
    authors: ['62345']})

    addComment(MotionToAccepttreasurersReport, {
      title: 'I think I deserve a pay raise.',
    authors: ['62345']})

    const SecretariesReport =
    x.addId('item', {title: 'Secretary\'s report',
      timeAllocated: 5,
    timeElapsed: 0,
    })

    addMotion(SecretariesReport,
        { title: 'Accept report',
          details: 'That the secretaries\'s report be accepted',
    mood: {
      '240851': {support: 1, love: 1},
      '240853': {support: 1, love: 1},
      '240854': {support: -1, love: 1},
    },
    movedBy: ['62345'], secondedBy: ['244085']})

    addMotion(SecretariesReport,
        { title: 'Appoint assistant',
          details: 'That Sam be appointed as assitant secretary',
    movedBy: [], secondedBy: []})
    const itemsArray = [SecretariesReport, treasurersReport,
      x.addId('item', { title: 'Convenors\'s report',
        timeAllocated: 5,
      timeElapsed: 0,
      }),
      x.addId('item', { title: 'Directors\' report',
        timeAllocated: 5,
      timeElapsed: 0,
      }),
      ]
    items = makeHashWithId('item',
        x.sortItems(itemsArray.map(item => x.summarise(item))))

    return {
      session: {
        device: "Dougs's tablet",
        lastUpdate: null,
        currentForumId: forum.id,
        currentItemId: undefined,
        users: [],
      },
      forums,
      items,
      motions,
      comments,
      members,
      users,
    }
  }


  // transform our generic test data blob into a client side specific form
  x.testState = () => {
    var data = x.testData()
    delete data.users
    return data
  }

  x.testShared = () => {
    console.log('*****Shared module')
    console.log('Local time is:', timeZone)
    console.log('Utils will ' + (mutate ? '' : 'not ') + 'mutate inputs')
  }

  return x
}

