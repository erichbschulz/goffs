import React, { Component } from 'react'
//import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import U from './Utils.js'
import {default as T} from 'material-ui/Typography'
import Card, { CardContent } from 'material-ui/Card'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import {bulk} from './Server.js'
import { Redirect } from 'react-router-dom'
import { LinearProgress } from 'material-ui/Progress'
import Select from 'material-ui/Select'
import /*Input,*/ { InputLabel } from 'material-ui/Input'
import { MenuItem } from 'material-ui/Menu'
import { FormControl/*, FormHelperText*/ } from 'material-ui/Form'
import { ExpandLite  } from './Widgets'

class BulkImportRaw extends Component {

/*
 * in complex mode:
 * * loop over elements
 * * if element type is not in ['forum', 'user'] and no forum id => invalid
 * * validate type
 * * validate each item
 */


  test = `
## user
1001|Kym|
1002|John|convenor
1003|Indigo|admin
1004|Lou|admin
1005|Milly|
1006|Ned|
1007|Oscar|

## forum
|Fish AGM||20180907|20180908

## item
|Treasurer's report

## motion
|That the treasures report be accepted||fred|abby

## item
|secrtaries's report

## motion
|the treasures report be accepted||fred|abby

## member
1003|secretary|1002|present
1001||1004,1003
1002|||present
1004
1005
`
  test1 = `
## user
1001|Kym|
1002|John|convenor
1003|Indigo|admin
## forum
|Fish AGM||20180907|20180908

## member
1003|secretary
1001
1002
`

  constructor(props) {
    super(props)
    this.handleBulkImport = this.handleBulkImport.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.state = {
      type: 'user',
      data: this.test, // "",
      records: [],
      errorCount: 0,
      mode:"ready"}
    this.startingState = {...this.state}
    this.state.format = 'long'
    Object.assign(this.state, this.parse(this.state.data))
  }

  formats = {
    tab: {
      name: 'Tab seperated',
      help: 'Use tabs for seperating each field. Put each record on a new line.',
      fieldSep: /\s*\t\s*/,
      recordSep: /\s*\n\s*/,
    },
    pipe: {
      name: 'Pipe seperated',
      help: 'Use the "|" character for seperating each field. Put each record on a new line.',
      fieldSep: /\s*\|\s*/,
      recordSep: /\s*\n\s*/,
    },
    long: {
      name: 'Long form',
      help: 'Put each each field on a new line. Use a line containing at least two hypens to seperate records',
      fieldSep: /\s*\n\s*/,
      recordSep: /\s*\n\s*---*\s*\n\s*/,
    }
  }


  handleInputChange = (event) => {
    // handle form changes
    const newState = U.onChangeEventToStateUpdate(event)
    this.setState(newState)
    // parse get report and records
    const parseResult = this.parse(this.state.data)
    this.setState(parseResult)
  }

  updateContext = (obj, context) => {
    switch (obj.type) {
      case 'user':
        return
      case 'forum':
        context.forumId = obj.id
        break
      case 'item':
        context.itemId = obj.id
        break
      default:
    }
    // do this for all except user:
    context.lastType = obj.type
    context.lastId = obj.id
  }

  // returns {report, records}
  parse = data => {
    const {format} = this.state
    const formatMeta = this.formats[format]
    const report = []
    const reportInfo = text => {report.push({type: 'info', text})}
    const reportError = text => {report.push({type: 'error', text})}
    // set up baseline context
    const {currentForumId: forumId, currentItemId: itemId} = this.props.screen
    var records = [], errorCount = 0
    const context = {forumId, itemId}
    if (format === 'long') {
      const chapters = data.split(/^\s*##\s*/gm)
      chapters.forEach(chapter => {if (chapter) {
        const lines = chapter.split(/\s*\n\s*/)
        const [type, ...recordStrings] = lines
        const fieldImportOrder = U.fieldImportOrder(type)
        reportInfo('expected field order: ' + fieldImportOrder.join('|'))
        recordStrings.forEach(record => {if (record) {
          const obj = {type}
          // split by |
          const fields = record.split(/\s*\|\s*/)
          // loop over each field
          fields.forEach((value, index) => {
            const field = fieldImportOrder[index]
            if (value) {
              console.log('value, type, field', value, type, field)
              obj[field] = U.fieldImportParse(value, type, field)
            }
          })
          console.log('obj', obj)
          const obj1 = U.buildObj(obj, context)
          console.log('obj', obj1)
          this.updateContext(obj1, context)
          const errors = U.validationErrors(obj1)
          reportInfo('record: ' + JSON.stringify(obj1))
          reportInfo('context: ' + JSON.stringify(context))
          if (errors.length) {
            errorCount += errors.length
            reportInfo('record: ' + record)
            reportInfo('expected field order: ' + fieldImportOrder.join('|'))
            errors.forEach(reportError)
          }
          else {
            records.push(obj1)
          }
        }})
      }})
    }
    else {
      const {type} = this.state
      console.log('type', type)
      console.log('format', formatMeta)
      records = U.stringToArray(data, formatMeta)
    }
    return {report, records, errorCount}
  }

  handleBulkImport = (event) => {
    const {dispatch} = this.props
    const {records} = this.state
    const payload = {records}
    this.setState({mode: 'waiting'})
    bulk(payload)
      .then((result) => {
        const {success, data={}} = result
        const {actions = []} = data
        const dispatchResult = actions.map(action => {return dispatch(action)})
        console.log('dispatchResult', dispatchResult)
        if (success) {
          this.setState({mode: 'success'})
        }
        else {
          this.setState({mode: 'ready'})
        }
      })
  }

  render() {
//    const {
//      members,
//      session,
//      } = this.props
    const {state, formats, types} = this
    const {mode, type, format, data, records = [], report, errorCount} = state
    return <div>
      {mode === 'done'
        ? <Redirect to='/' />
        : mode === 'waiting'
        ? <div><T variant={'title'} >Checking details...</T><LinearProgress /></div>
        : mode === 'success'
        ? <div>
             <T variant={'title'} >Success!</T>
            <Button variant="raised" color="primary" size="large"
            onClick={() => this.setState({mode: 'done'})}
            >
              Finished
            </Button>
            <Button variant="raised" color="secondary" size="large"
            onClick={() => this.setState(this.startingState)}
            >
              Import more things
            </Button>

        </div>
        : <div><Card>
        <CardContent>
          <T variant={'title'} >
            Bulk import
          </T>
          <T component={'p'} >
          This form allows you to quickly add:
          users,
          forums,
          items,
          motions and
          members. Each "forum" (aka "meeting") has a collection of items (ie the agenda), and each "item" has a collection of "motions". Once "users" are in the database they maybe added as "members" of multiple "forums".

            You will typically wish to prepare your bulk import in the editor of your choice then cut and paste the text into the form.
            By default each new thing is on its own line with fields seperated by the "|" character
          </T>
          <form onSubmit={this.handleSubmit}>
{errorCount
              ? <T color='error'>{errorCount}&nbsp;
                  {U.pluralise(errorCount, 'error', 'errors')
                  } - see the Parsing report below for details
                </T>
              : '' }
            <FormControl>
              <InputLabel htmlFor="InputFormat">Input format</InputLabel>
              <Select
                disabled
                name="format"
                value={format}
                onChange={this.handleInputChange}
                inputProps={{id: 'InputFormat'}}
              >
              {Object.keys(formats).map(format =>
                <
                  MenuItem
                  key={format}
                  value={format}>{formats[format].name}</MenuItem>)
                }
              </Select>
            </FormControl>

            {format === 'long'
              ?  ""
              : <FormControl>
                  <InputLabel htmlFor="InputType">Input format</InputLabel>
                  <Select
                    name="type"
                    value={type}
                    onChange={this.handleInputChange}
                    inputProps={{id: 'InputType'}}
                  >
                  {Object.keys(types).map(type =>
                    <
                      MenuItem
                      key={type}
                      value={type}>{type}</MenuItem>)
                    }
                  </Select>
                </FormControl>
            }

            <TextField autoFocus
              label="Data"
              multiline fullWidth rows="10"
              error={mode === 'denied'}
              name="data" value={data}
              onChange={this.handleInputChange}
              helperText={mode === 'denied'
                ? <span>Please try again</span> : ''}
              type="text" />
            <Button variant="raised" color="primary" size="large"
            disabled={errorCount > 0}
            onClick={this.handleBulkImport}>
              Submit
            </Button>
            <Button variant="raised" color="secondary" size="large"
            onClick={() => this.setState({mode: 'done'})}>
              Cancel
            </Button>
          </form>
        </CardContent>
      </Card>
      <Card>
        <CardContent>

          <ExpandLite heading="Parsing report"
            callBack={() => <div style={{display: 'block'}}>
              {report.map((row, i) =>
                <T variant="body1" gutterBottom align="left"
                component="p"
                key={i} color={row.type === 'error' ? 'error' : 'default'}>
                  {row.text}
                </T>)
              }</div>
          }/>

          <ExpandLite heading="Records" callBack={() => <table>
            <tbody>
            {records.map((row, i) => {
              const {id, title, type, status, ...rest} = row
              return <tr key={id}>
                <td>{id}</td>
                <td>{type}</td>
                <td>{title}</td>
                <td>{status}</td>
                <td>{JSON.stringify(rest)}</td>
              </tr>}
            )}
            </tbody>
          </table>}
/>
        </CardContent>
      </Card></div>
}

    </div>
  }
}

BulkImportRaw.propTypes = {
}
BulkImportRaw.defaultProps = {
//  mode: 'proxy',
}


// Returns a plain object, which is merged into component’s props
// If not subscribing to store updates, pass null instead of function
// Can take an optional second param [ownProps]
const mapStateToProps = state => {
    // props here become props in the component
  return {
    screen: state.screen,
  }
}
 
const BulkImport = connect(mapStateToProps)(BulkImportRaw)

export default BulkImport
