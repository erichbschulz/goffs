// server and environment specific settings
const host = window.location.hostname
const Config = {
  host,
  pollInterval: 10, // seconds
  server: host !== 'localhost'
    ? `https://${host}:3003/`
    : `http://${host}:4001/`,
  ws: host !== 'localhost'
    ? `wss://${host}:3003/`
    : `ws://${host}:4001/`,
}

console.log('Config', Config)

export default Config
