import axios from 'axios'
import Config from './Config.js'
/**
* Module to handle HTTP calls to server
*
* React components call methods of Server.js with params object
*
* Server.js responds with either:
*
*   {success: false, error: {message, code}}
*   or
*   {success: true, data: {...}}
*
* Failure to authorise with return error situation, except on login attmept
*/

const serverAddress = Config.server
console.log('serverAddress', serverAddress)
const config = {withCredentials: true}

export const authorise = (p) => {
  const {id, pin} = p
  const payload = {id, pin}
  return POST('auth', payload)
}

export const register = (p) => {
  const {id, type, title, email, pin} = p
  const payload = {id, type, title, email, pin}
  return POST('register', payload)
}

// reset own password (anybody)
export const changePassword = (p) => {
  const {id, oldpin, newpin} = p
  const payload = {id, oldpin, newpin}
  return POST('changepassword', payload)
}

// reset other users' password (admins only)
export const resetPassword = (p) => {
  const {userIds, forumId} = p
  const payload = {userIds, forumId}
  return POST('resetpassword', payload)
}

export const bulk = (p) => {
  const {records} = p
  const payload = {records}
  return POST('bulk', payload)
}

export const upsert = (p) => {
  const {record} = p
  const payload = {record}
  return POST('upsert', payload)
}

export const patch = (p) => {
  const {record} = p
  const payload = {record}
  return POST('patch', payload)
}

export const vote = (p) => {
  const {record} = p
  const payload = {record}
  return POST('vote', payload)
}

export const getSession = () => GET('session')
export const deleteSession = () => DELETE('session')
export const getForum = (id) => GET('forum', {id})
export const getUsers = (id) => GET('forum/users', {id})
export const getForumUpdates = (id, from) => {
  if (id && from) {
    return GET('forum/updates', {id, from})
  }
  else {
    return Promise.resolve([])
  }
}

///////////////////////// wrappers:

function GET(route, params) {
  return axios
    .get(serverAddress + route, Object.assign({}, {params}, config))
    .then(succeed)
    .catch(fail)
}

function POST(route, payload) {
  return axios
    .post(serverAddress + route, payload, config)
    .then(succeed)
    .catch(fail)
}

function DELETE(route, params) {
  return axios
    .delete(serverAddress + route, Object.assign({}, {params}, config))
    .then(succeed)
    .catch(fail)
}

// error handler
function fail(errorObj) {
  const {
    response = {},
  } = errorObj
  const {
    status: code = 'none',
    data = {}
  } = response
  const {error} = data
  const messageText = /expired session/.test(error)
  ? "Sorry your session has expired. Please log-in again."
  : error ? error
  : code === 403
  ? "mmm... server says you're not allowed"
  : 'Something went wrong. Soz (' + code + ').'
  alert(messageText)
  console.error('full errorObj ', errorObj)
  return {success: false, error: {message: messageText, code}}
}

// success handler
// packages up result in standard package
function succeed(res) {
  return {success: true, data: res.data}
}

