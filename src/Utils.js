import uuidv4 from 'uuid/v4'
import Shared from './Shared.js'
import {DateTime} from 'luxon'
import humanizeDuration from 'humanize-duration'
import {bulk} from './Server.js'

// polyfill
if (!Array.prototype.includes) {
  console.log('shimming array.includes')
  // eslint-disable-next-line
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }
      var o = Object(this);
      var len = o.length >>> 0;
      if (len === 0) {
        return false;
      }
      var n = fromIndex | 0;
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }
      while (k < len) {
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }
      return false;
    }
  });
}
// polyfill
if (!Object.values) {
  console.log('shimming object.values')
  Object.values = function(obj) {
    return Object.keys(obj).map(key => obj[key]);
  };
};

// access the shared methods and attach to the exports object
const exports = (() => {
  const x = {
    humaniseMilliseconds: ms => (ms > 0 ? 'in ' : '')
      + humanizeDuration(ms, {
        largest: 2,
        round: true,
        units: ['w', 'd', 'h', 'm']})
      + (ms < 0 ? ' ago' : '')
  }
  const uuid = (seed) => (uuidv4(seed, uuidv4.DNS))
  const shared = Shared({uuid,
    DateTime,
    timeZone: DateTime.local().zoneName,
    mutate: false})
  Object.keys(shared).forEach(method => x[method] = shared[method])
  return x
})()
exports.testShared()

// client only widgets

// make an object from a form onChange event
const onChangeEventToStateUpdate = (event) => {
  const {name, value, type, checked} = event.target
  const finalValue = type === 'checkbox' ? checked : value
  return {[name]: finalValue}
}

// JSON.stringify replacer avoid circles
// thanks to https://stackoverflow.com/questions/11616630/json-stringify-avoid-typeerror-converting-circular-structure-to-json?rq=1
const stringify = (obj, replacer, indent=2) => {
  var po = [] // printed objects
  var pok = [] // printed object keys
  const printOnceReplacer = (key, value) => {
    if (po.length > 2000) { return 'object too long'}
    var printedObjIndex = false
    po.forEach((obj, index) => {
      if(obj===value) {printedObjIndex = index}
    })
    if (key === '') { //root element
      po.push(obj)
      pok.push("root")
      return value
    }
    else if (printedObjIndex+"" !== "false" && typeof(value)==="object"){
      if (pok[printedObjIndex] === "root"){
        return "(pointer to root)"
      } else if (value && value.id && value.title) {
        return '{#' + value.id + ' ' + value.title + '}'
      } else {
        const type = (!!value && !!value.constructor)
            ? value.constructor.name.toLowerCase()
            : typeof(value)
        return "(see " + type +  " with key " + pok[printedObjIndex] + ")"
      }
    } else {
      po.push(value)
      pok.push(key || "(empty key)")
      return replacer ? replacer(key, value) : value
    }
  }
  return JSON.stringify(obj, printOnceReplacer, indent)
}

////////////////////////////////////////////////////////////////////////////////

// update a sinlge obj and return a copy
// does not mutate original collection
const updateSingle = (collection, id, callback) => {
  return {...collection, [id]: callback(collection[id])}
}

//create a collection of {userId, viaUserId}
// based on the session user collection and localStatus property
const currentVoters = (users, members) => {
  const activeUsers =
    Object.values(users)
    .filter(user => user.localStatus === 'active')
    .reduce((active, user) => {
      //if no member record then not voting (eg admin)
      const member = members[user.id] || {}
      const {votingFor = []} = member
      votingFor.forEach(userId => active.push({userId, viaUserId: user.id}))
      return active
      }, [])
  return activeUsers
}

//create a collection of {userId, viaUserId}
// based on the session user collection and localStatus property
const currentUsers = (users) => {
  const activeUsers =
    Object.keys(users)
    .filter(userId => users[userId].localStatus === 'active')
  return activeUsers
}

// todo split this into two - one just making tallys
// take {obj, voters, payload.action}
// returns a new obj with the mood updated
const recordVotes = params => {
  console.log('>>params', params)
  const {obj, voters, action, meta} = params
  // merge these actions to create collection of updated moods by voters
  let moodUpdates = {}
  voters.forEach(userId => {
    console.log('userId', userId)
    const existingMood = ((obj.mood || {})[userId]) || {}
    let relativeActions = {}
    for (const field in action) {
      const fieldMeta = meta[field]
      if (fieldMeta.mode === 'median') {
        relativeActions[field] = relativeChange(
            action[field],
            existingMood[field],
            fieldMeta.defaultValue, obj)
      }
      else {
        relativeActions[field] = action[field]
      }
    }
    moodUpdates[userId] = Object.assign({}, existingMood, relativeActions)
  })
  // add in new moods to existing mood
  const mood = Object.assign({}, obj.mood, moodUpdates)
  // copy obj with new mood
  const finalobj = Object.assign({}, obj, {mood})
  return finalobj
}


// record a vote and resummarise and tally
// params: obj, voters, action, meta
const RecordAndSummarise = (params) => {
  const attributeCollection = 'mood'
  return exports.summarise(recordVotes(params),
      attributeCollection, params.meta)
}

// cluster a mood (indexed on memberid) to make a list of moods.
const cluster = (mood) => {
  let result = {}
  Object.keys(mood).forEach(id => { // id = 1234
    Object.keys(mood[id]).forEach(attribute => { // attribute = support
      if (!result[attribute]) result[attribute] = {}
      const value = mood[id][attribute] //
      if (!result[attribute][value]) result[attribute][value] = []
      result[attribute][value].push(id)
    })
  })
  return result
}

const timesUp = {1: 2, 2: 3, 3: 4, 4: 5, 5: 7, 7: 8, 8: 10, 10: 15, 15: 20, 20: 25, 25: 30,
  30: 40, 40: 50, 50: 60, 60: 70, 70: 80, 80: 90, 90: 120, 120: 150, 150: 180,
  180: 240, 240: 300, 300: 360, 360: 420, 420: 480}
const timesDown = ((o) => { // invert times up array
  let i = {}
  for (const k in o) i[o[k]] = Math.floor(k) // cast as int
  return i})(timesUp)

// add more or less to a number in a logarithmic scale
// uses timesUp and timesDown
const relativeChange = (action, current, defaultValue, obj) => {
  const start = isNaN(current) ? defaultValue(obj) : current
  var result
  switch (action) {
    case 'more':
      result = timesUp[start] || Math.round(start * 1.3) + 1
      break
    case 'less':
      result = timesDown[start] || Math.max(Math.round(start / 1.3) - 1, 0)
      break
    default:
      throw new Error('bad action')
  }
  return result
}

// handler for bulk changes
// returns a promise
const sendBulk = (records, dispatch) => {
  const payload = {records}
  return bulk(payload)
  .then((result) => {
    const {success, data={}} = result
    const {actions = []} = data
    const dispatchResult = actions.map(action => {return dispatch(action)})
    console.log('dispatchResult', dispatchResult)
    if (!success) {
      throw Error('update failed')
    }
    else {
      return dispatchResult
    }
  })
}

// calculate the reference number of participants for a forum
const participantCount = (members = {}) =>
  Object.values(members).filter(member => member.activeProxy).length || 1

// determine if new permissions (supports componentDidUpdate())
// return Boolean
const newPerms = (prevProps, currentPermissions) => {
  const prevPermissions = prevProps.session.permissions
  // this length comparision is a horrid dirty hack,
  // but should work 99.9999% of the time:
  return Object.keys(currentPermissions).length !==
         Object.keys(prevPermissions).length
}

// set ./meta.js for the converse of these functions
const forumUrl = (forumId) => `/forum/${forumId}`
const itemUrl = (forumId, itemId) => `/item/${forumId}/${itemId}`
const motionUrl
    = (forumId, itemId, motionId) => `/item/${forumId}/${itemId}#${motionId}`
const commentUrl
    = (forumId, itemId, commentId) => `/item/${forumId}/${itemId}#${commentId}`

// attempt to use router history object to navigate insitu
// but if no history simply open new window
const navigateTo = (url, name, history) => {
  if (history && history.push)
    history.push(url)
  else {
    const win = window.open(url, name)
    win.focus()
  }
}

export default Object.assign(exports, {
  // generic:
  onChangeEventToStateUpdate,
  stringify, // JSON replacer
  updateSingle,
  // application specific:
  currentVoters,
  currentUsers,
  recordVotes,
  RecordAndSummarise,
  cluster, // reorganise a mood list to make arrays of ids
  participantCount,
  sendBulk,
  newPerms,
  forumUrl,
  itemUrl,
  motionUrl,
  commentUrl,
  navigateTo
})
