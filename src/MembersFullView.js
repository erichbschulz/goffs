import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Members from './MemberView'
import { ExpandLite } from './Widgets'
import { connect } from 'react-redux'
import {getUsers} from './Server.js'

// full detailed breakdown
// wip
class MembersFullRaw extends Component {

  constructor(props) {

    super(props)
    this.loadUsers = this.loadUsers.bind(this)
    this.state = {
      fetching: false,
    }
  }

  loadUsers = (forumId) => {
    if (forumId && this.state.fetching !== forumId) {
      console.log('forumId', forumId)
      console.log('this.state.fetching', this.state.fetching)
      // set state then use callback
//      this.setState({fetching: forumId}, this.state.something(forumId))
      this.setState({fetching: forumId}, () => {
        getUsers(forumId).then(users => {
          console.log('users', users)
          this.setState({users, forumId})
      })})
    }
  }

  render() {
    const {//members,
      heading,
      members,
      screen} = this.props
      // fixme!!
    const memberList = Object.keys(members)
    // fixme - move out of render
    const details = () => {
      const forumId = screen.currentForumId
      this.loadUsers(forumId)
      return (
          <div>
      <Members memberList={memberList} members={members} />
          </div>
      )
    }
    return (
      <ExpandLite heading={heading} callBack={details} />
      )
  }
}

MembersFullRaw.propTypes = {
  // members: PropTypes.array.isRequired,
  members: PropTypes.object.isRequired,
}
MembersFullRaw.defaultProps = {
  heading: 'Members',
}

const mapStateToProps = state => {
  return {
    members: state.members,
    screen: state.screen,
  }
}
 
const MembersFull = connect(mapStateToProps)(MembersFullRaw)

export default MembersFull
