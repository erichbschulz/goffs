import Utils from './Utils.js'

// todo move this starting state to store
export default () => {
  let state = Utils.testState()
  const {
    currentForum,
    currentItem,
  } = state.session

  if (!currentForum) {
    Object.assign(state, {items: {}, motions: {}, comments: {}})
  }

  state.screen = {
    participants: Utils.participantCount(state.members),
    gridLock: true,
    showComments: true,
    // copy screen over from session
    currentForum,
    currentItem,
  }
    console.log('state', state)
  return state
}

